<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualTaxRate extends Model
{
    protected $table = 'pms_annual_tax_rates';
    protected $fillable = [

    	'employee_id',
    	'employee_number',
		'basic_amount',
		'contribution_amount',
		'pera_amount',
		'rata_amount',
		'longevity_amount',
		'hp_amount',
		'tax_amount',
		'for_month',
		'for_year',
		'created_by',
		'updated_by'

    ];
}
