<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceInfo extends Model
{
    protected $table = 'pms_attendance_info';
    protected $fillable = [

		'employee_id',
		'employee_number',
		'transaction_id',
		'actual_workdays',
		'adjust_workdays',
		'total_workdays',
		'actual_absence' ,
		'adjust_absenc',
		'total_absence' ,
		'actual_tardines',
		'adjust_tardines',
		'total_tardines',
		'actual_undertime',
		'adjust_undertime',
		'total_undertime',
		'employee_status',
		'month',
		'year'

    ];
}
