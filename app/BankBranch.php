<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankBranch extends Model
{
    protected $table = 'pms_bankbranches';
    protected $fillable = [
    	'code',
    	'name',
    	'address',
    	'remarks'
    ];
}
