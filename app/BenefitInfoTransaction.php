<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitInfoTransaction extends Model
{
    protected $table = 'pms_benefitsinfo_transactions';
    protected $fillable = [
        'employee_id',
        'employee_number',
    	'benefit_id',
        'benefitinfo_id',
        'amount',
        'no_of_months_entitled',
        'sala_absent',
        'sala_absent_amount',
        'sala_undertime',
        'sala_undertime_amount',
        'days_present',
        'hp_rate',
        'year',
        'month',
        'status',
        'created_by',
        'updated_by'
    ];


    public function benefitinfo(){
        return $this->belongsTo('App\BenefitInfo','benefitinfo_id')->with('benefits');
    }

    public function employeeInfo(){
    	return $this->belongsTo('App\EmployeeInfo','employeeinfo_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function  employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id');
    }

    public function  salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id');
    }

    public function benefits(){
        return $this->belongsTo('App\Benefit','benefit_id');
    }
}
