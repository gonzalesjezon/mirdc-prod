<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductionInfoTransaction extends Model
{
    protected $table 	= 'pms_deductioninfo_transactions';
    protected $fillable = [
		'employee_id',
        'employee_number',
		'transaction_id',
		'deduction_id',
        'division_id',
		'deduction_info_id',
		'amount',
		'status',
        'year',
        'month',
        'deduction_rate',
        'effective_date',
		'created_by',
		'updated_by',
    ];

    public function deductioninfo(){
    	return $this->belongsTo('App\DeductionInfo','deduction_info_id')->with('deductions');
    }
    public function deductions(){
    	return $this->belongsTo('App\Deduction','deduction_id');
    }
    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }
}
