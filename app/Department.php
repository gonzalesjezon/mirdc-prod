<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

	protected $primaryKey = 'RefId';
    protected $table = 'department';
    protected $fillable = [
        'Name',
        'Code',
        'created_by',
        'updated_by'
    ];
}
