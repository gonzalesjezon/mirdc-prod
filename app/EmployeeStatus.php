<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
	protected $primaryKey = 'RefId';
    protected $table = 'empstatus';
    protected $fillable = [
    	'Code',
    	'Name',
    	'category',
    	'created_by',
    	'updated_by'
    ];

}
