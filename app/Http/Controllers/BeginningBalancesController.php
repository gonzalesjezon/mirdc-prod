<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\BeginningBalance;
use App\PreviousEmployer;
class BeginningBalancesController extends Controller
{
    function __construct(){
    	$this->title = 'EMPLOYEE COMPENSATION BEGINNING BALANCES';
    	$this->module = 'beginningbalances';
        $this->module_prefix = 'payrolls/admin';
    	$this->controller = $this;
    }

    public function index(){


    	$response = array(
	    					'module'        	=> $this->module,
	    					'controller'   	 	=> $this->controller,
	                        'module_prefix' 	=> $this->module_prefix,
	    					'title'		    	=> $this->title,
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->searchName($q);

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $query = Employee::where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });

        $response = $query->orderBy('lastname','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$this->validate(request(),[
    		'as_of_date' 				=> 'required',
    		'premium_amount' 			=> 'required',
    		'tax_witheld' 				=> 'required',
    		'basic_pay' 				=> 'required',
    		'overtime_pay' 				=> 'required',
    		'thirteen_month_pay' 		=> 'required',
    		'deminimis' 				=> 'required',
    		'other_salaries' 			=> 'required',
    		'taxable_basic_pay' 		=> 'required',
    		'taxable_overtime_pay' 		=> 'required',
    		'taxable_other_salaries' 	=> 'required',
    		'taxable_thirteen_month_pay' => 'required',
    	]);

    	$begbalance = new BeginningBalance;

    	if(isset($request->beginningbalances_id)){
    		$begbalance = BeginningBalance::find($request->beginningbalances_id);

    		$begbalance->employee_id 					= str_replace(',', '', $request->employee_id);
	    	$begbalance->premium_amount 				= str_replace(',', '', $request->premium_amount);
	    	$begbalance->tax_witheld 					= str_replace(',', '', $request->tax_witheld);
	    	$begbalance->basic_pay 						= str_replace(',', '', $request->basic_pay);
	    	$begbalance->overtime_pay 					= str_replace(',', '', $request->overtime_pay);
	    	$begbalance->thirteen_month_pay 			= str_replace(',', '', $request->thirteen_month_pay);
	    	$begbalance->deminimis 						= str_replace(',', '', $request->deminimis);
	    	$begbalance->other_salaries 				= str_replace(',', '', $request->other_salaries);
	    	$begbalance->taxable_basic_pay 				= str_replace(',', '', $request->taxable_basic_pay);
	    	$begbalance->taxable_overtime_pay 			= str_replace(',', '', $request->taxable_overtime_pay);
	    	$begbalance->taxable_thirteen_month_pay 	= str_replace(',', '', $request->taxable_thirteen_month_pay);
	    	$begbalance->taxable_other_salaries 		= str_replace(',', '', $request->taxable_other_salaries);
	    	$begbalance->as_of_date 					= str_replace(',', '', $request->as_of_date);

    		$begbalance->save();

    		$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

    	}else{

	    	$begbalance->employee_id 					= str_replace(',', '', $request->employee_id);
	    	$begbalance->premium_amount 				= str_replace(',', '', $request->premium_amount);
	    	$begbalance->tax_witheld 					= str_replace(',', '', $request->tax_witheld);
	    	$begbalance->basic_pay 						= str_replace(',', '', $request->basic_pay);
	    	$begbalance->overtime_pay 					= str_replace(',', '', $request->overtime_pay);
	    	$begbalance->thirteen_month_pay 			= str_replace(',', '', $request->thirteen_month_pay);
	    	$begbalance->deminimis 						= str_replace(',', '', $request->deminimis);
	    	$begbalance->other_salaries 				= str_replace(',', '', $request->other_salaries);
	    	$begbalance->taxable_basic_pay 				= str_replace(',', '', $request->taxable_basic_pay);
	    	$begbalance->taxable_overtime_pay 			= str_replace(',', '', $request->taxable_overtime_pay);
	    	$begbalance->taxable_thirteen_month_pay 	= str_replace(',', '', $request->taxable_thirteen_month_pay);
	    	$begbalance->taxable_other_salaries 		= str_replace(',', '', $request->taxable_other_salaries);
	    	$begbalance->as_of_date 					= str_replace(',', '', $request->as_of_date);

    		$begbalance->save();

    		$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}




    	return $response;
    }

    public function getBeginningBalances(){
    	$data = Input::all();

    	$query = BeginningBalance::where('employee_id',$data['id'])
						    	->first();

		return json_encode($query);
    }
}
