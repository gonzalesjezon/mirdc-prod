<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
class COSGeneralPayrollReportsController extends Controller
{
    function __construct(){
		$this->title = 'CONTRACTUAL GENERAL PAYROLL REPORT';
    	$this->module = 'cosgeneralpayrollreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year   	   = $data['year'];
        $month  	   = $data['month'];

        $transaction =  new NonPlantillaTransaction;
        $status 	 = 	new EmployeeStatus;

        $query = $transaction
        ->with('nonplantilla','employees','departments')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $payroll = [];
        foreach ($query as $key => $value) {
        	$payroll[@$value->departments->Name][$key] = $value;
        }

        return json_encode($payroll);

    }
}
