<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\Benefit;
use App\LoanInfoTransaction;
use App\DeductionInfoTransaction;
class CashAdvanceReportsController extends Controller
{
    function __construct(){
    	$this->title = 'CASH ADVANCE REPORT';
    	$this->module = 'cashadvancereports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $transaction 	 		= new Transaction;
        $loantransaction 		= new LoanInfoTransaction;
        $deductiontranssaction 	= new DeductionInfoTransaction;
        $benefit 	 	 	 	= new Benefit;

        $year = $q['year'];
        $month = $q['month'];

        $pera = $benefit
        ->where('code','PERA')
        ->first();

        $rataone = $benefit
        ->where('code','RATA1')
        ->first();

        $ratatwo = $benefit
        ->where('code','RATA2')
        ->first();

        $hp = $benefit
        ->where('code','HP')
        ->first();

        $sala = $benefit
        ->where('code','SALA')
        ->first();

        $query = $transaction
        ->with([
            'employees',
            'positionitems',
            'positions',
            'employeeinfo',
            'longevity',
            'divisions' =>function($qry){
                $qry->orderBy('Name','asc');
            },
            'offices',
            'employeeinformation',
            'salaryinfo',
            'getpera'=>function($qry) use($pera,$year,$month){
                $qry = $qry->where('benefit_id',$pera->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'getrataone'=>function($qry) use($rataone,$year,$month){
                $qry = $qry->where('benefit_id',$rataone->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'getratatwo'=>function($qry) use($ratatwo,$year,$month){
                $qry = $qry->where('benefit_id',$ratatwo->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'gethp'=>function($qry) use($hp,$year,$month){
                $qry = $qry->where('benefit_id',$hp->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'getsala'=>function($qry) use($sala,$year,$month){
                $qry = $qry->where('benefit_id',$sala->id)
                ->where('year',$year)
                ->where('month',$month);
            },
            'gettax'=>function($qry) use($year,$month){
                $qry = $qry->where('for_year',$year)
                ->where('for_month',$month);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
            	if (isset($value->division_id)) {
                	$data[$value->divisions->Code][$key] = $value;
            	}
            }

        }else{
            $data = [];
        }

        $query2 = $loantransaction
        ->with('divisions','loans')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data2 = [];
        if(count($query2) > 0){
            foreach ($query2 as $key => $value) {
            	if (isset($value->division_id)) {
                	$data2[$value->divisions->Code][$value->loans->name][$key] = $value;
            	}
            }

        }else{
            $data2 = [];
        }

        $query3 = $deductiontranssaction
        ->with('divisions','deductions')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data3 = [];
        if(count($query3) > 0){
            foreach ($query3 as $key => $value) {
            	if (isset($value->division_id)) {
                	$data3[$value->divisions->Code][$value->deductions->name][$key] = $value;
            	}
            }

        }else{
            $data3 = [];
        }

        $query4 = $loantransaction
        ->with('divisions','loans')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data4 = [];
        if(count($query4) > 0){
            foreach ($query4 as $key => $value) {
            	if (isset($value->division_id)) {
                	$data4[$value->loans->name][$key] = $value;
            	}
            }

        }else{
            $data4 = [];
        }

        $query5 = $deductiontranssaction
        ->with('divisions','deductions')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data5 = [];
        if(count($query5) > 0){
            foreach ($query5 as $key => $value) {
            	if (isset($value->division_id)) {
                	$data5[$value->deductions->name][$key] = $value;
            	}
            }

        }else{
            $data5 = [];
        }

        return json_encode([
            'transactions' => $data,
            'loanlists' => $data2 ,
            'deductionlists' => $data3,
            'grouploans' => $data4  ,
            'groupdeductions' => $data5
        ]);
    }
}
