<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\SalaryInfo;
use App\Rate;
use DateTime;
class CashGiftAndYearEndTransactionsController extends Controller
{
     function __construct(){
        $this->title = 'YEAR END BONUS AND CASH GIFT';
        $this->controller = $this;
        $this->module = 'cgyetransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


        $response = array(
           'title'              => $this->title,
           'controller'         => $this->controller,
           'module'             => $this->module,
           'module_prefix'      => $this->module_prefix,

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q          = Input::get('q');
        $year       = Input::get('year');
        $month      = Input::get('month');
        $_year       = Input::get('_year');
        $_month      = Input::get('_month');
        $check_cgye  = Input::get('check_cgye');
        $checkcgye   = Input::get('checkcgye');
        $data = "";

        $data = $this->searchName($q,$check_cgye,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkcgye)){
            $data = $this->filter($year,$month,$checkcgye);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkcgye,$year,$month){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkcgye) {
            case 'wcgye':
               $employee_id = $transaction
               ->whereIn('employee_id',$employee_info_id)
               ->where('status','yearendbonus')
               ->where('year',$year)
               ->where('month',$month)
               ->select('employee_id')->get()->toArray();

               $query = $employee->whereIn('id',$employee_id);

                break;

            default:
                $employee_id = $transaction->whereIn('employee_id',$employee_info_id)
                ->where('year',$year)
                ->where('month',$month)
                ->where('status','yearendbonus')
                ->select('employee_id')
                ->get()
                ->toArray();

                $query = $employee->whereNotIn('id',$employee_id)
                                    ->where('with_setup',1)
                                    ->whereIn('id',$employee_info_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function filter($year,$month,$checkcgye){


        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
        $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkcgye) {
            case 'wcgye':

                $query =  $transaction->select('employee_id')->where('status','yearendbonus');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            default:

                 $query =  $transaction->select('employee_id')->where('status','yearendbonus');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        ->whereIn('id',$salary_info_employee_id)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->where('with_setup',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processCgye(Request $request){
        $data = Input::all();

        $employeeinformation = new EmployeeInformation;
        $salaryinfo          = new SalaryInfo;
        $cash_gift = 5000;

        foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $transaction     = new SpecialPayrollTransaction;
                $employeeinfo    = $employeeinformation->where('employee_id',$value)->first();
                $salary          = $salaryinfo->where('employee_id',$value)->orderBy('salary_effectivity_date','desc')->first();
                $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

                $basic_amount_one = ($salary->basic_amount_one) ? $salary->basic_amount_one : 0;
                $basic_amount_two = ($salary->basic_amount_two) ? $salary->basic_amount_one : 0;
                $basic_amount = (float)$basic_amount_one + (float)$basic_amount_two;

                $first_date = new DateTime($assumption_date);
                $nov = date('Y').'-10-31 00:00:00';

                $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
                $interval = $first_date->diff($second_date);

                // $x = $interval->format('%Y years %M months and %D days.');
                $number_of_month = (int)$interval->format('%M');
                $number_of_year = (int)$interval->format('%Y');

                if($number_of_year !== 0){

                    $transaction->employee_id         = $value;
                    $transaction->percentage          = 100;
                    $transaction->position_id         = @$employeeinfo->position_id;
                    $transaction->division_id         = @$employeeinfo->division_id;
                    $transaction->office_id           = @$employeeinfo->office_id;
                    $transaction->employee_number     = @$employeeinfo->employee_number;
                    $transaction->amount              = $basic_amount;
                    $transaction->tax_amount          = $data['tax_amount'];
                    $transaction->deduction_amount    = $data['deduction_amount'];
                    $transaction->cash_gift_amount    = $cash_gift;
                    $transaction->year                = $data['year'];
                    $transaction->month               = $data['month'];
                    $transaction->status              = 'yearendbonus';

                    $transaction->save();

                }else{

                    if($number_of_month >= 10){

                        $transaction->employee_id         = $value;
                        $transaction->percentage          = 100;
                        $transaction->position_id         = @$employeeinfo->position_id;
                        $transaction->division_id         = @$employeeinfo->division_id;
                        $transaction->office_id           = @$employeeinfo->office_id;
                        $transaction->employee_number     = @$employeeinfo->employee_number;
                        $transaction->amount              = $basic_amount;
                        $transaction->tax_amount          = $data['tax_amount'];
                        $transaction->deduction_amount    = $data['deduction_amount'];
                        $transaction->cash_gift_amount    = $cash_gift;
                        $transaction->year                = $data['year'];
                        $transaction->month               = $data['month'];
                        $transaction->status              = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 9 && $number_of_month < 10){

                        $new_cash_gift =  ((float)$cash_gift*.95);
                        $new_basic     =  ((float)$basic_amount*.95);

                        $transaction->percentage         = 95;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 8 && $number_of_month < 9){

                        $new_cash_gift =  ((float)$cash_gift*.90);
                        $new_basic     =  ((float)$basic_amount*.90);

                        $transaction->percentage         = 90;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 7 && $number_of_month < 8){

                        $new_cash_gift =  ((float)$cash_gift*.80);
                        $new_basic     =  ((float)$basic_amount*.80);

                        $transaction->percentage         = 80;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 6 && $number_of_month < 7){

                        $new_cash_gift =  ((float)$cash_gift*.70);
                        $new_basic     =  ((float)$basic_amount*.70);

                        $transaction->percentage         = 70;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 5 && $number_of_month < 6){

                        $new_cash_gift =  ((float)$cash_gift*.60);
                        $new_basic     =  ((float)$basic_amount*.60);

                        $transaction->percentage         = 60;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 4 && $number_of_month < 5){

                        $new_cash_gift =  ((float)$cash_gift*.50);
                        $new_basic     =  ((float)$basic_amount*.50);

                        $transaction->percentage         = 50;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->amount             = $new_basic;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 3 && $number_of_month < 4){

                        $new_cash_gift =  ((float)$cash_gift*.40);

                        $transaction->percentage         = 40;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 2 && $number_of_month < 3){

                        $new_cash_gift =  ((float)$cash_gift*.30);

                        $transaction->percentage         = 30;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }elseif($number_of_month >= 1 && $number_of_month < 2){

                        $new_cash_gift =  ((float)$cash_gift*.20);

                        $transaction->percentage         = 20;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }else{

                        $new_cash_gift =  ((float)$cash_gift*.10);

                        $transaction->percentage         = 10;
                        $transaction->position_id        = @$employeeinfo->position_id;
                        $transaction->division_id        = @$employeeinfo->division_id;
                        $transaction->office_id          = @$employeeinfo->office_id;
                        $transaction->employee_number    = @$employeeinfo->employee_number;
                        $transaction->employee_id        = $value;
                        $transaction->tax_amount         = $data['tax_amount'];
                        $transaction->deduction_amount   = $data['deduction_amount'];
                        $transaction->cash_gift_amount   = $new_cash_gift;
                        $transaction->year               = $data['year'];
                        $transaction->month              = $data['month'];
                        $transaction->status             = 'yearendbonus';

                        $transaction->save();

                    }
                }

            }
        }

        $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

        return $response;
    }

    public function showCgyeDatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getCgyeInfo(){
        $data = Input::all();

        $pei =  new SpecialPayrollTransaction;

        $query = $pei->with('positions')
                    ->where('year',$data['year'])
                    ->where('month',$data['month'])
                    ->where('status','yearendbonus')
                    ->where('employee_id',@$data['employee_id'])
                    ->first();

        return json_encode($query);
    }

    public function deleteCgye(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','yearendbonus')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
