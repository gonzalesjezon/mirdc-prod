<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\TaxTable;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function table_columns($table){

        $res = array();
        $data = DB::select('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "default" AND TABLE_NAME="'.$table.'"');
        foreach ($data as $key => $value) {
        	$notInArray = array('id','created_at','updated_at');
        	// echo
            if(!in_array($value->column_name,$notInArray)){
                array_push($res,$value->column_name);
            }
        }
        return $res;

    }

    public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count-1;
    }

    public function getWorkingDays($startDate,$endDate){
        $start = strtotime($startDate);
        $end = strtotime($endDate);

        if($start > $end){
            $working_days = 0;
        }else{
            $no_days = 0;
            $weekends = 0;
            while ($start <= $end) {
                $no_days++; // no of days in given interval
                $what_day = date('N',$start);
                if($what_day>6){ // 6 and 7 are weekend
                    $weekends++;
                }
                $start+=86400; // +1 day
            }
        }
        $working_days = $no_days - $weekends;

        return $working_days;

    }

    public function computeTax($gross_salary){

        $taxtable = new TaxTable;

        $prescribeTax        = 0;
        $prescribePercentage = 0;
        $clRange             = 0;
        $taxDue              = 0;
        $grossTaxable        = 0;
        $taxPercentage       = 0;

        $taxtable = $taxtable
        ->get()->toArray();

        $data['monthlyCL'] =  [
            0,
            $taxtable[1]['salary_bracket_level2'],
            $taxtable[1]['salary_bracket_level3'],
            $taxtable[1]['salary_bracket_level4'],
            $taxtable[1]['salary_bracket_level5'],
            $taxtable[1]['salary_bracket_level6']
        ];

        $data['percent'] = [0,.20,.25,.30,.32,.35];
        $data['below']  = [0,20833.33,33333.33,66666.66,166666.66,666666.66];
        $data['above'] = [20832,33332,66666,166666,666666,9999999] ;

        foreach ($data['monthlyCL'] as $key => $value) {

            if($gross_salary > $data['below'][$key] && $gross_salary < $data['above'][$key]){
                $prescribeTax = $value;
                $prescribePercentage = $data['percent'][$key];
                $clRange = $data['below'][$key];
            }
        }

        if($gross_salary < 20833){
            $clRange = 0;
            $prescribePercentage = 0;
        }

        $grossTaxable = $gross_salary - $clRange;
        $taxPercentage = $grossTaxable * $prescribePercentage;
        $taxDue = $taxPercentage + $prescribeTax;

        return $taxDue;
    }

    public function computePhic($basic_amount){

        $basicAmount = $basic_amount;
        $ee_share = 0;
        $er_share = 0;
        $return = [];

        if($basicAmount >= 41999){
            $ee_share = 550;
            $er_share = 550;
        }else{
            $phicAmount = $basicAmount * 0.0275;
            $phicAmount = $phicAmount; // round to two decimal
            $newAmount = $phicAmount / 2;
            $newAmount = $newAmount; // round to two decimal
            $checkAmount = $newAmount + $newAmount;

            if($checkAmount != $phicAmount){
                $er_share = $newAmount + 0.01;
                $ee_share = $newAmount;
            }else{
                $er_share = $newAmount;
                $ee_share = $newAmount;
            }
        }

        $return['ee_share'] = $ee_share;
        $return['er_share'] = $er_share;

        return $return;
    }

    public function computeGsis($basic_amount){

        $return = [];
        $ee_share = $basic_amount * .09;
        $er_share = $basic_amount * .12;

        $return['ee_share'] = $ee_share;
        $return['er_share'] = $er_share;

        return $return;
    }
}
