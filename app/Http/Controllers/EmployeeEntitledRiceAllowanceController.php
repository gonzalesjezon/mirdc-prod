<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class EmployeeEntitledRiceAllowanceController extends Controller
{
    function __construct(){
		$this->title = 'EMPLOYEE ENTITLED TO RICE ALLOWANCE';
    	$this->module = 'employeeentitledriceallowance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $transaction    =  new SpecialPayrollTransaction;

        $year = $data['year'];
        $month = $data['month'];

        $query = $transaction
        ->with('employees','benefitinfo')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','ra')
        ->get();



        return json_encode($query);
    }
}
