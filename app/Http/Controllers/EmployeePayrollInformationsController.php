<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\EmployeeInfo;
use App\SalaryGrade;
use App\PositionItem;
use App\JobGrade;
use App\NonPlantillaEmployeeInfo;
use Carbon\Carbon;
use App\Rate;

class EmployeePayrollInformationsController extends Controller
{

    function __construct(){
        $this->module_prefix = 'payrolls/admin';
        $this->module = 'employees_payroll_informations';
        $this->title = 'EMPLOYEE FILE';
        $this->controller  = $this;


    }

    public function index(){

        $gsis           = GsisPolicy::get();
        $pagibig        = PagibigPolicy::get();
        $philhealth     = PhilhealthPolicy::get();
        $bank           = Bank::get();
        $benefit        = Benefit::get();
        $taxperiod      = TaxTable::get();
        $wagerate       = WageRate::get();
        $loans          = Loan::get();
        $deductions     = Deduction::get();
        $sg_data        = SalaryGrade::get();
        $positionitem   = PositionItem::orderBy('name','asc')->get();
        $position       = Position::orderBy('Name','asc')->get();
        $jg_data        = JobGrade::get();
        $jo_tax_policy  = TaxPolicy::where('policy_name','like','%'.'%'.'%')->whereNotNull('job_grade_rate')->get();
        $tax_policy     = TaxPolicy::whereNull('job_grade_rate')->get();
        $hp_rate        = Rate::where('status','hp')->orderBy('rate','asc')->get();

    	$response = array(
                        'hp_rate'       => $hp_rate,
                        'jo_tax_policy' => $jo_tax_policy,
                        // 'jo_tax_policy_two' => $jo_tax_policy_two,
                        'jg_data'       => $jg_data,
                        'sg_data'       => $sg_data,
                        'positionitem'  => $positionitem,
                        'position'      => $position,
                        'deductions'    => $deductions,
                        'loans'         => $loans,
                        'wagerate'      => $wagerate,
                        'taxperiod'     => $taxperiod,
                        'bank'          => $bank,
                        'philhealth'    => $philhealth,
                        'pagibig'       => $pagibig,
                        'gsis'          => $gsis,
                        'benefit'       => $benefit,
                        'tax_policy'    => $tax_policy,
    					'title' 		=> $this->title,
    					'controller'	=> $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){

        $benefitinfo    = BenefitInfo::with('benefits')->get();

        $response       = array(
                            'benefitinfo'   => $benefitinfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitinfo_datatable',$response);
    }

    public function showLoaninfo(){

        $loaninfo = LoanInfo::with('loans')->get();
        $response = array(
                        'loaninfo'   => $loaninfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.loaninfo_datatable',$response);
    }


     public function showSalaryinfo(){

        $salaryinfo   = SalaryInfo::with('salarygrade','jobgrade')->get();
        $response     = array(
                        'salaryinfo'   => $salaryinfo,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );
        return view($this->module_prefix.'.'.$this->module.'.salaryinfo_datatable',$response);
    }

     public function showDeductioninfo(){

        $deductioninfo   = DeductionInfo::with('deductions')->get();
        $response        = array(
                            'deductioninfo'   => $deductioninfo,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );
        return view($this->module_prefix.'.'.$this->module.'.deductioninfo_datatable',$response);
    }


    public function store(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{


            $employeeInfo = new EmployeeInfo;

            $pagibig_contribution       =   (isset($request->pagibig_contribution) ? str_replace(',', '', $request->pagibig_contribution) : 0.00);
            $philhealth_contribution    = (isset($request->philhealth_contribution) ? str_replace(',', '', $request->philhealth_contribution) : 0.00);
            $gsis_contribution          = (isset($request->gsis_contribution) ? str_replace(',', '', $request->gsis_contribution) : 0.00);
            $tax_contribution           = (isset($request->tax_contribution) ? str_replace(',', '', $request->tax_contribution) : 0.00);
            $pagibig2                   = (isset($request->pagibig2) ? str_replace(',', '', $request->pagibig2) : 0.00);
            $tax_bracket_amount         = (isset($request->tax_bracket_amount) ? str_replace(',', '', $request->tax_bracket_amount) : 0.00);
            $pagibig_personal                = (isset($request->pagibig_personal) ? str_replace(',', '', $request->pagibig_personal) : 0.00);

            $tax_inexcess                    = (isset($request->tax_inexcess) ? str_replace(',', '', $request->tax_inexcess) : 0.00);
            $er_pagibig_share                = (isset($request->er_pagibig_share) ? str_replace(',', '', $request->er_pagibig_share) : 0.00);
            $er_gsis_share                   = (isset($request->er_gsis_share) ? str_replace(',', '', $request->er_gsis_share) : 0.00);
            $er_philhealth_share             = (isset($request->er_philhealth_share) ? str_replace(',', '', $request->er_philhealth_share) : 0.00);
            $overtime_balance_amount         = (isset($request->overtime_balance_amount) ? str_replace(',', '', $request->overtime_balance_amount) : 0.00);
            $daily_rate_amount         = (isset($request->daily_rate_amount) ? str_replace(',', '', $request->daily_rate_amount) : 0);

            $monthly_rate_amount         = (isset($request->monthly_rate_amount) ? str_replace(',', '', $request->monthly_rate_amount) : 0);
            $annual_rate_amount         = (isset($request->annual_rate_amount) ? str_replace(',', '', $request->annual_rate_amount) : 0);

            if(isset($request->employeeinfo_id)){
                $employeeInfo = EmployeeInfo::find($request->employeeinfo_id);

                $employeeInfo->bp_no                    = $request->bp_no;
                $employeeInfo->taxpolicy_id             = $request->taxpolicy_id;
                $employeeInfo->gsispolicy_id            = $request->gsispolicy_id;
                $employeeInfo->philhealthpolicy_id      = $request->philhealthpolicy_id;
                $employeeInfo->pagibigpolicy_id         = $request->pagibigpolicy_id;
                $employeeInfo->bank_id                  = $request->bank_id;
                $employeeInfo->wagestatus_id            = $request->wagestatus_id;
                $employeeInfo->providentfund_id         = $request->providentfund_id;
                $employeeInfo->atm_no                   = $request->atm_no;
                $employeeInfo->daily_rate_amount        = $daily_rate_amount;
                $employeeInfo->monthly_rate_amount      = $monthly_rate_amount;
                $employeeInfo->pagibig_contribution     = $pagibig_contribution;
                $employeeInfo->annual_rate_amount       = $annual_rate_amount;
                $employeeInfo->philhealth_contribution  = $philhealth_contribution;
                $employeeInfo->gsis_contribution        = $gsis_contribution;
                $employeeInfo->tax_contribution         = $tax_contribution;
                $employeeInfo->pagibig2                 = $pagibig2;
                $employeeInfo->no_ofdays_inayear        = $request->no_ofdays_inayear;
                $employeeInfo->no_ofdays_inamonth       = $request->no_ofdays_inamonth;
                $employeeInfo->total_hours_inaday       = $request->total_hours_inaday;
                $employeeInfo->tax_payperiod            = $request->tax_payperiod;
                $employeeInfo->tax_bracket              = $request->tax_bracket;
                $employeeInfo->tax_id_number            = $request->tax_id_number;
                $employeeInfo->tax_bracket_amount       = $tax_bracket_amount;
                $employeeInfo->tax_inexcess             = $tax_inexcess;
                $employeeInfo->pagibig_personal         = $pagibig_personal;
                $employeeInfo->er_pagibig_share         = $er_pagibig_share;
                $employeeInfo->er_gsis_share            = $er_gsis_share;
                $employeeInfo->er_philhealth_share      = $er_philhealth_share;
                $employeeInfo->employee_number          = $request->emp_no;
                $employeeInfo->overtime_balance_amount  = $overtime_balance_amount;

                $employeeInfo->save();

                if(isset($employeeInfo)){
                    $employees = new Employee;

                    $employees = $employees->find($request->employee_id);
                    $employees->with_setup = $request->with_setup;
                    $employees->save();
                }

                $response = json_encode(['status' => true, 'response' => 'Update Successfully!','employeeload'=>true]);

            }else{

                 $request->validate([
                    'pagibig_contribution'      => 'required',
                    'philhealth_contribution'   => 'required',
                    'gsis_contribution'         => 'required',
                 ]);

                $request->merge([
                    'pagibig_contribution'    => $pagibig_contribution,
                    'philhealth_contribution' => $philhealth_contribution,
                    'gsis_contribution'       => $gsis_contribution,
                    'tax_contribution'        => $tax_contribution,
                    'pagibig2'                => $pagibig2,
                    'tax_bracket_amount'      => $tax_bracket_amount,
                    'tax_inexcess'            => $tax_inexcess,
                    'pagibig_personal'        => $pagibig_personal,
                    'er_philhealth_share'     => $er_philhealth_share,
                    'er_gsis_share'           => $er_gsis_share,
                    'er_pagibig_share'        => $er_pagibig_share,
                    'overtime_balance_amount' => $overtime_balance_amount,
                    'daily_rate_amount'       => $daily_rate_amount,
                    'monthly_rate_amount'     => $monthly_rate_amount,
                    'annual_rate_amount'      => $annual_rate_amount,
                    'employee_number'         => $request->emp_no


                ]);

                $employeeInfo->fill($request->all())->save();

                if(isset($employeeInfo)){
                    $employees = new Employee;

                    $employees = $employees->find($request->employee_id);
                    $employees->with_setup = 1;
                    $employees->save();
                }

                $response = json_encode(['status' => true, 'response' => 'Save Successfully!','employeeload'=>true]);

            }

        }

        return $response;


    }

    public function show(){

        $q               = Input::get('q');
        $empstatus       = Input::get('empstatus');
        $employee_status = Input::get('employee_status');
        $category        = Input::get('category');
        $emp_type        = Input::get('emp_type');
        $searchby        = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q,$employee_status);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$empstatus){
        $cols = ['lastname','firstname'];

        $employee_status      = new EmployeeStatus;
        $employee             = new Employee;
        $employeeinformation  = new EmployeeInformation;
        $empstatus_id = [];
        $employee_id = [];


        $query = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employee_status
                ->where('category',1)
                ->select('RefId')
                ->get()
                ->toArray();

                $employee_id =  $employeeinformation->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);
            break;

            case 'nonplantilla':
                $empstatus_id = $employee_status
                ->where('category',0)
                ->select('RefId')
                ->get()
                ->toArray();

                $employee_id =  $employeeinformation->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);
            break;

            default:

                $employee_id =  $employeeinformation->select('employee_id')->get()->toArray();
                $query = $employee->whereIn('id',$employee_id);

            break;
        }

        $query = $query
            ->where('active',1)
            ->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        })->orderBy('lastname','asc')->get();

        return $query;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){

        $employee_status     = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;
        $employeeinfo        = new EmployeeInfo;

        $employeeinfo = $employeeinfo->select('employee_id')->get()->toArray();

        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = $employee_status->where('category',0)->select('RefId')->get()->toArray();
            break;
        }

        $employeesRefId = [];
        $employeesRefId = $employeeinformation->select('employee_id')
                            ->where(function($qry) use($category,$searchby){
                                switch ($searchby) {
                                    case 'company':
                                        $qry =  $qry->where('company_id',$category);
                                        break;
                                    case 'position':
                                        $qry =  $qry->where('position_id',$category);
                                        break;
                                    case 'division':
                                        $qry =  $qry->where('division_id',$category);
                                        break;
                                    case 'office':
                                        $qry =  $qry->where('office_id',$category);
                                        break;
                                    case 'department':
                                        $qry =  $qry->where('department_id',$category);
                                        break;
                                }

                            });
                if(count($empstatus_id) > 0){
                    $employeesRefId = $employeesRefId->whereIn('employee_status_id',$empstatus_id);
                }
                $employeesRefId = $employeesRefId->get()->toArray();

        $query = $employee
            ->whereIn('id',$employeesRefId)
            ->where('active',1)
            ->orderBy('lastname','asc')->get();

        return $query;


    }


    public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('Name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('Name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('Name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('Name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('Name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){
        $data = Input::all();

        $employee_id     = $data['id'];
        $employee_number = $data['employee_number'];

        $employeeinformation = new EmployeeInformation;
        $employeeinfo        = new EmployeeInfo;
        $loaninfo            = new LoanInfo;
        $deductioninfo       = new DeductionInfo;
        $benefitinfo         = new BenefitInfo;
        $salaryinfo          = new SalaryInfo;
        $nonplantilla        = new NonPlantillaEmployeeInfo;
        $taxable             = new TaxTable;

        $data['employeeinfo'] = $employeeinformation
        ->with([
            'employees',
            'divisions',
            'positions',
            'employeestatus',
            'offices'])
        ->orWhere('employee_id',$employee_id)
        ->orWhere('employee_number',$employee_number)
        ->first();

        $data['pmsemployeeinfo'] = $employeeinfo
        ->with('employees','gsispolicy','pagibigpolicy','philhealthpolicy','taxpolicy','banks','wages','providentfunds')
        ->orWhere('employee_id',$employee_id)
        ->orWhere('employee_number',$employee_number)
        ->first();

        if(isset($data['pmsemployeeinfo'])){

            $data['loaninfo'] = $loaninfo
            ->with('loans')
            ->orWhere('employee_id',$employee_id)
            ->orWhere('employee_number',$employee_number)
            ->get();

            $data['deductioninfo'] = $deductioninfo
            ->with('deductions')
            ->orWhere('employee_id',$employee_id)
            ->orWhere('employee_number',$employee_number)
            ->get();

            $data['benefitinfo'] = $benefitinfo
            ->with('benefits')
            ->orWhere('employee_id',$employee_id)
            ->orWhere('employee_number',$employee_number)
            ->get();
        }


        $effectivity_date = Carbon::now()->toDateString();

        $data['salaryinfo'] = $salaryinfo
        ->with('salarygrade','jobgrade')
        ->orWhere('employee_id',$employee_id)
        ->orWhere('employee_number',$employee_number)
        ->where('salary_effectivity_date','<=',$effectivity_date)
        ->first();

        $data['salarylist'] = $salaryinfo
        ->with('salarygrade','jobgrade')
        ->orWhere('employee_id',$employee_id)
        ->orWhere('employee_number',$employee_number)
        ->get();

        $data['nonplantilla'] = $nonplantilla
        ->orWhere('employee_id',$employee_id)
        ->orWhere('employee_number',$employee_number)
        ->first();


        $taxtable = $taxable
        ->get()->toArray();


        $data['cl'] = [
            'monthlyCL' => [
                            '0'.'-'.'0',
                            $taxtable[1]['salary_bracket_level2'].'-'.'.20',
                            $taxtable[1]['salary_bracket_level3'].'-'.'.25',
                            $taxtable[1]['salary_bracket_level4'].'-'.'.30',
                            $taxtable[1]['salary_bracket_level5'].'-'.'.32',
                            $taxtable[1]['salary_bracket_level6'].'-'.'.35'
                        ]
            ];

        $data['first']  = ['0','20833','33333','66667','166667','666667'];
        $data['second'] = ['20832','33332','66666','166666','666666','9999999'] ;


        return json_encode($data);
    }


    public function storeBenefitinfo(Request $request){


        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $benefitInfo = new BenefitInfo;

             $benefit_amount = (isset($request->benefit_amount)) ? str_replace(',', '', $request->benefit_amount) : 0;

            if(isset($request->benefitinfo_id)){

                $benefitInfo = $benefitInfo->find($request->benefitinfo_id);
                $benefitInfo->employee_id                = $request->employee_id;
                $benefitInfo->employeeinfo_id            = $request->employeeinfo_id;
                $benefitInfo->benefit_effectivity_date   = $request->benefit_effectivity_date;
                $benefitInfo->benefit_id                 = $request->benefit_id;
                $benefitInfo->benefit_description        = $request->benefit_description;
                $benefitInfo->date_from                  = $request->date_from;
                $benefitInfo->date_to                    = $request->date_to;
                $benefitInfo->days_present               = $request->days_present;
                $benefitInfo->hp_rate                    = $request->hp_rate;
                $benefitInfo->benefit_amount             = $benefit_amount;
                $benefitInfo->benefit_pay_period         = $request->benefit_pay_period;
                $benefitInfo->employee_number            = $request->employee_number;

                if($request->benefit_pay_period == 'Weekly'){
                    $benefitInfo->benefit_pay_sub   = $request->weekly;

                }

                if($request->benefit_pay_period == 'Semi Monthly'){
                    $benefitInfo->benefit_pay_sub   = $request->semi_monthly;
                }

                $benefitInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','benefitinfo'=> BenefitInfo::with('benefits')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('benefit_effectivity_date','asc')->get(), 'myform'=>'myForm3']);

            }else{

                $request->validate([
                    'benefit_id'                => 'required',
                    'benefit_effectivity_date'  => 'required',
                    'benefit_amount'            => 'required',
                ]);

                $request->merge(['benefit_amount' => $benefit_amount ]);

                if($request->benefit_pay_period == 'Weekly'){
                    $benefitInfo->benefit_pay_sub   = $request->weekly;

                }

                if($request->benefit_pay_period == 'Semi Monthly'){
                    $benefitInfo->benefit_pay_sub   = $request->semi_monthly;
                }

                $benefitInfo->fill($request->all())->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','benefitinfo'=> BenefitInfo::with('benefits')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('benefit_effectivity_date','asc')->get(), 'myform'=>'myForm3']);
            }

        }

        return $response;

    }

    public function storeSalaryinfo(Request $request){

        if(!isset($request->employee_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{


            $salaryInfo = new SalaryInfo;

            $basic_amount_one      = (isset($request->basic_amount_one) ? str_replace(',', '', $request->basic_amount_one) : 0.00 );
            $basic_amount_two      = (isset($request->basic_amount_two) ? str_replace(',', '', $request->basic_amount_two) : 0.00);
            $salarygrade_id        =  (isset($request->salarygrade_id) ? $request->salarygrade_id : NULL);
            $jobgrade_id           = (isset($request->jobgrade_id) ? $request->jobgrade_id : NULL);
            $position_id           =  (isset($request->position_id) ? $request->position_id : NULL);
            $positionitem_id       =  (isset($request->positionitem_id) ? $request->positionitem_id : NULL);
            $step_inc              =  (isset($request->jgstep_inc) ? $request->jgstep_inc : NULL);

            if(isset($request->sgjginfo_id)){

                $salaryInfo = SalaryInfo::find($request->sgjginfo_id);

                $salaryInfo->salarygrade_id          = $salarygrade_id;
                $salaryInfo->jobgrade_id             = $jobgrade_id;
                $salaryInfo->employee_id             = $request->employee_id;
                $salaryInfo->salary_effectivity_date = $request->salary_effectivity_date;
                $salaryInfo->salary_description      = $request->salary_description;
                $salaryInfo->employee_number         = $request->employee_number;
                $salaryInfo->basic_amount_one        = $basic_amount_one;
                $salaryInfo->basic_amount_two        = $basic_amount_two;
                $salaryInfo->positionitem_id         = $positionitem_id;
                $salaryInfo->position_id             = $position_id;
                $salaryInfo->step_inc                = $step_inc;

                $salaryInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }else{

                $request->validate([
                    'salary_effectivity_date'  => 'required',
                    'salary_description'       => 'required',
                ]);

                $salaryInfo->salarygrade_id          = $salarygrade_id;
                $salaryInfo->jobgrade_id             = $jobgrade_id;
                $salaryInfo->employee_id             = $request->employee_id;
                $salaryInfo->salary_effectivity_date = $request->salary_effectivity_date;
                $salaryInfo->salary_description      = $request->salary_description;
                $salaryInfo->basic_amount_one        = $basic_amount_one;
                $salaryInfo->employee_number         = $request->employee_number;
                $salaryInfo->basic_amount_two        = $basic_amount_two;
                $salaryInfo->positionitem_id         = $positionitem_id;
                $salaryInfo->position_id             = $position_id;
                $salaryInfo->step_inc                = $step_inc;

                $salaryInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','salaryinfo'=> SalaryInfo::with('salarygrade','jobgrade')->where('employee_id',$request->employee_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm2']);

            }
        }

        return $response;
    }

    public function storeLoaninfo(Request $request){

        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $loanInfo = new LoanInfo;

            $loanTotalAmount        = (isset($request->loan_totalamount) ? str_replace(',', '', $request->loan_totalamount) : 0.00);
            $loanTotalBalance       = (isset($request->loan_totalbalance) ? str_replace(',', '', $request->loan_totalbalance) : 0.00);
            $loanAmortization       = (isset($request->loan_amortization) ? str_replace(',', '', $request->loan_amortization) : 0.00);

            if(isset($request->loaninfo_id)){

                $loanInfo = LoanInfo::find($request->loaninfo_id);

                $loanInfo->loan_pay_period        = $request->loan_pay_period;
                $loanInfo->loan_date_started      = $request->loan_date_started;
                $loanInfo->loan_date_end          = $request->loan_date_end;
                $loanInfo->employeeinfo_id        = $request->employeeinfo_id;
                $loanInfo->employee_id            = $request->employee_id;
                $loanInfo->employee_number        = $request->employee_number;
                $loanInfo->loan_id                = $request->loan_id;
                $loanInfo->loan_date_granted      = $request->loan_date_granted;
                $loanInfo->loan_totalamount       = $loanTotalAmount;
                $loanInfo->loan_totalbalance      = $loanTotalBalance;
                $loanInfo->loan_amortization      = $loanAmortization;
               /* $loanInfo->year                   = $request->loan_year;
                $loanInfo->month                  = $request->loan_month;*/
                // $loanInfo->date_terminated    = $request->date_dateterminated;

                $loanInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','loaninfo'=> LoanInfo::with('loans')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm4']);

            }else{

                $request->validate([
                    'loan_id'           => 'required',
                    'loan_amortization' => 'required',
                    // 'loan_year'         => 'required',
                    // 'loan_month'        => 'required',
                ]);

                $request->merge([
                    'loan_totalamount'  => $loanTotalAmount,
                    'loan_totalbalance' => $loanTotalBalance,
                    'loan_amortization' => $loanAmortization,
                    'employee_number'   => $request->employee_number,
                    // 'year'              => $request->loan_year,
                    // 'month'             => $request->loan_month,
                ]);

                $loanInfo->fill($request->all())->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','loaninfo'=> LoanInfo::with('loans')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm4']);

            }
        }

        return $response;

    }


    public function storeDeductioninfo(Request $request){

        if(!isset($request->employeeinfo_id)){

            $response = json_encode(['status'=>false,'response'=>'No employee selected']);

        }else{

            $deductionInfo = new DeductionInfo;

            $deductAmount  = (isset($request->deduct_amount) ? str_replace(',', '', $request->deduct_amount) : 0.00);

            if(isset($request->deductinfo_id)){

                $deductionInfo = DeductionInfo::find($request->deductinfo_id);

                $deductionInfo->deduct_date_start      = $request->deduct_date_start;
                $deductionInfo->deduct_date_end        = $request->deduct_date_end;
                $deductionInfo->employeeinfo_id        = $request->employeeinfo_id;
                $deductionInfo->deduction_id           = $request->deduction_id;
                $deductionInfo->deduct_pay_period      = $request->deduct_pay_period;
                $deductionInfo->deduct_amount          = $deductAmount;
                // $deductionInfo->date_terminated = $request->date_deductterminated;
                $deductionInfo->save();

                $response = json_encode(['status'=>true,'response'=>'Update Successfully!','deductioninfo'=> DeductionInfo::with('deductions')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm5']);

            }else{

                $request->validate([
                    'deduction_id'  => 'required',
                    'deduct_amount' => 'required',
                ]);

                $deductAmount  = (isset($request->deduct_amount) ? str_replace(',', '', $request->deduct_amount) : 0.00);
                $request->merge(['deduct_amount' => $deductAmount]);

                $deductionInfo->fill($request->all())->save();

                $response = json_encode(['status'=>true,'response'=>'Save Successfully!','deductioninfo'=> DeductionInfo::with('deductions')->where('employeeinfo_id',$request->employeeinfo_id)->orderBy('created_at','asc')->get(), 'myform'=>'myForm5']);

            }
        }

        return $response;

    }


     public function getSgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = SalaryGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getJgstep(){

        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = JobGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }

    public function getItem(){
        $id = Input::get('id');

        $query = PositionItemSetup::where('id',$id)->first();

        return json_encode($query);
    }

    //  STORE NON PLANTILLA
    public function storeNonPlantilla(Request $request){

        $nonplantilla = new NonPlantillaEmployeeInfo;

        if(isset($request->employeeinfo_id)){

            $nonplantilla = NonPlantillaEmployeeInfo::find($request->employeeinfo_id);
            $nonplantilla->employee_id           = $request->employee_no;
            $nonplantilla->bank_id               = $request->jo_bank_id;
            $nonplantilla->taxpolicy_id          = $request->jo_taxpolicy_id;
            $nonplantilla->atm_no                = $request->jo_atm_no;
            $nonplantilla->daily_rate_amount     = str_replace(',', '', $request->job_order_daily_rate);
            $nonplantilla->tax_id_number         = $request->jo_tax_id_number;
            $nonplantilla->taxpolicy_two_id      = @$request->jo_taxpolicy_two_id;
            $nonplantilla->overtime_balance_amount   = @str_replace(',', '', $request->jo_overtime_balance_amount);

            $nonplantilla->save();

            $response = json_encode(['status'=> true, 'response'=>'Update Successfully!']);
        }else{

            $request->validate([
                'job_order_daily_rate'     => 'required',
            ]);

            $nonplantilla->employee_id           = $request->employee_no;
            $nonplantilla->bank_id               = $request->jo_bank_id;
            $nonplantilla->taxpolicy_id          = $request->jo_taxpolicy_id;
            $nonplantilla->atm_no                = $request->jo_atm_no;
            $nonplantilla->daily_rate_amount     = str_replace(',', '', $request->job_order_daily_rate);
            $nonplantilla->tax_id_number         = $request->jo_tax_id_number;
            $nonplantilla->taxpolicy_two_id      = @$request->jo_taxpolicy_two_id;
            $nonplantilla->overtime_balance_amount   = @str_replace(',', '', $request->jo_overtime_balance_amount);

            $nonplantilla->save();

            $response = json_encode(['status'=> true, 'response'=>'Save Successfully!']);
        }


        return $response;
    }

    public function deleteDeductInfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id   = $data['id'];

        $deduct_info = new DeductionInfo;

        $deduct_info->destroy($id);

        $deduct_info = $deduct_info
        ->with('deductions')
        ->where('employee_id',$employee_id)
        ->get();

        return json_encode(['status'=>'deductions','data'=>$deduct_info]);

    }

    public function deleteBenefitinfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id             = $data['id'];

        $benefit_info = new BenefitInfo;
        $benefit_info->destroy($id);

        $benefit_info = $benefit_info->with('benefits')
        ->where('employee_id',$employee_id)
        ->get();

        return json_encode(['status'=>'benefits','data'=>$benefit_info]);

    }

    public function deleteLoanInfo(){
        $data = Input::all();

        $employee_id    = $data['employee_id'];
        $id             = $data['id'];

        $loan_info = new LoanInfo;
        $loan_info->destroy($id);

        $loan_info = $loan_info->with('loans')->where('employee_id',$employee_id)->get();

        return json_encode(['status'=>'loans','data'=>$loan_info]);

    }

    public function computeEmployeeInfo(){

        $salaryinfo          = new SalaryInfo;
        $employeeinfo        = new EmployeeInfo;
        $employee            = new Employee;
        $employeeinformation = new EmployeeInformation;
        $phicpolicy          = new PhilhealthPolicy;
        $pagibigpolicy       = new PagibigPolicy;
        $philhealthpolicy    = new PhilhealthPolicy;
        // $responsibilities    = new ResponsibilityCenter;
        $employeestatus      = new EmployeeStatus;
        $gsispolicy          = new GsisPolicy;
        $taxpolicy           = new TaxPolicy;

        $employee = $employee
        ->where('active',1)
        ->select('id')
        ->get()->toArray();

        $employeestatus = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $query = $employeeinformation
        ->whereIn('employee_status_id',$employeestatus)
        ->whereIn('employee_id',$employee)
        ->get();

        $philhealthpolicy = $philhealthpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $pagibigpolicy = $pagibigpolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $gsispolicy = $gsispolicy
        ->where('policy_name','STANDARD POLICY')
        ->first();

        $taxpolicy = $taxpolicy
        ->where('policy_name','ANNUAL POLICY')
        ->first();

        foreach ($query as $key => $value) {
            $employeeinfo        = new EmployeeInfo;

            $salaryinfo = $salaryinfo
            ->where('employee_id',$value->employee_id)
            ->orderBy('salary_effectivity_date','desc')
            ->first();

            $basicOne = (isset($salaryinfo->basic_amount_one)) ? $salaryinfo->basic_amount_one : 0;
            $basicTwo = (isset($salaryinfo->basic_amount_two)) ? $salaryinfo->basic_amount_two : 0;

            $basicAmount = $basicOne + $basicTwo;

            $dailyRateAmount  = 0;
            if(isset($basicAmount)){
                $dailyRateAmount = $basicAmount / 22;
            }else{
                dd('No Salary Info Employee Id '.$value->employee_id);
            }

            $monthlyRateAmount = $basicAmount;
            $annualRateAmount = $monthlyRateAmount * 12;

            $phic = $this->computePhic($monthlyRateAmount);
            $gsis = $this->computeGsis($monthlyRateAmount);

            $phic_ee_share = $phic['ee_share'];
            $phic_er_share = $phic['er_share'];
            $gsis_ee_share = $gsis['ee_share'];
            $gsis_er_share = $gsis['er_share'];
            $pagibig_ee_share = 100;

            $totalDeduction = $phic_ee_share + $gsis_ee_share + $pagibig_ee_share;
            $grossSalary = $monthlyRateAmount - $totalDeduction;

            // $tax = $this->computeTax($grossSalary);

            $empinfo = $employeeinfo
            ->where('employee_id',$value->employee_id)
            ->first();

            if(isset($empinfo)){

                $employeeinfo = $employeeinfo->find($empinfo->id);

                $employeeinfo->employee_id                  = $value->employee_id;
                $employeeinfo->gsispolicy_id                = $gsispolicy->id;
                $employeeinfo->pagibigpolicy_id             = $pagibigpolicy->id;
                $employeeinfo->philhealthpolicy_id          = $philhealthpolicy->id;
                $employeeinfo->taxpolicy_id                 = $taxpolicy->id;
                // $employeeinfo->employee_status_id           = $value->employee_status_id;
                $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                $employeeinfo->annual_rate_amount           = $annualRateAmount;
                $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                $employeeinfo->philhealth_contribution      = $phic_ee_share;
                $employeeinfo->gsis_contribution            = $gsis_ee_share;
                // $employeeinfo->tax_contribution             = $tax;
                $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                $employeeinfo->er_philhealth_share          = $phic_er_share;
                $employeeinfo->er_gsis_share                = $gsis_er_share;
                $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                $employeeinfo->save();

            }else{

                $employeeinfo->employee_id                  = $value->employee_id;
                $employeeinfo->gsispolicy_id                   = $gsispolicy->id;
                $employeeinfo->pagibigpolicy_id                = $pagibigpolicy->id;
                $employeeinfo->philhealthpolicy_id             = $philhealthpolicy->id;
                $employeeinfo->taxpolicy_id                    = $taxpolicy->id;
                // $employeeinfo->employee_status_id           = $value->employee_status_id;
                $employeeinfo->daily_rate_amount            = $dailyRateAmount;
                $employeeinfo->monthly_rate_amount          = $monthlyRateAmount;
                $employeeinfo->annual_rate_amount           = $annualRateAmount;
                $employeeinfo->pagibig_contribution         = $pagibig_ee_share;
                $employeeinfo->philhealth_contribution      = $phic_ee_share;
                $employeeinfo->gsis_contribution            = $gsis_ee_share;
                // $employeeinfo->tax_contribution             = $tax;
                $employeeinfo->er_pagibig_share             = $pagibig_ee_share;
                $employeeinfo->er_philhealth_share          = $phic_er_share;
                $employeeinfo->er_gsis_share                = $gsis_er_share;
                $employeeinfo->overtime_balance_amount      = $annualRateAmount;

                $employeeinfo->save();

            }
        }

        return json_encode(['status' => true, 'response' => 'Update Successfully']);

    }

}
