<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\Division;
use App\EmployeeStatus;
use App\Position;
use App\Office;
use App\EmployeeInformation;
use App\Department;
class EmployeeSetupController extends Controller
{
    function __construct(){
    	$this->title 		 = 'EMPLOYEE SETUP';
    	$this->module_prefix = 'payrolls/admin';
    	$this->module 		 = 'employeesetup';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
    					'offices'			=> Office::orderBy('name','asc')->get(),
    					'divisions'			=> Division::orderBy('name','asc')->get(),
    					'positions'	         => Position::orderBy('name','asc')->get(),
    					'employee_status'	=> EmployeeStatus::orderBy('name','asc')->get(),
                        'departments'       => Department::orderBy('name','asc')->get(),
						'module'        	=> $this->module,
						'controller'    	=> $this->controller,
		                'module_prefix' 	=> $this->module_prefix,
						'title'		    	=> $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['lastname','firstname'];

        $query = EmployeeInformation::with(['positionitems','divisions','offices','employeestatus','employees'=>function($qry) use($q,$cols){
                        $qry->where(function($query) use($cols,$q){
                            $query = $query->where(function($qry) use($q, $cols){
                                foreach ($cols as $key => $value) {
                                    $qry->orWhere($value,'like','%'.$q.'%');
                                }
                            });
                        });

                    }]);

        $response = $query->get();

        return $response;

    }

    public function store(Request $request){



    	$employees = new Employee;
        $employeeinformation = new EmployeeInformation;

    	if(isset($request->employee_information_id)){

    		$employees = $employees->find($request->employee_id);

	    	$employees->firstname   = $request->firstname;
            $employees->lastname    = $request->lastname;
            $employees->middlename  = $request->middlename;
            $employees->active      = $request->status;
            $employees->created_by  = Auth::User()->id;

            $employees->save();

            $employeeinformation = $employeeinformation->find($request->employee_information_id);

            $employeeinformation->employee_id           = $request->employee_id;
            $employeeinformation->division_id           = $request->division_id;
            $employeeinformation->department_id         = $request->department_id;
            $employeeinformation->office_id             = $request->office_id;
            $employeeinformation->employee_status_id    = $request->employee_status_id;
            $employeeinformation->position_id           = $request->position_id;
            $employeeinformation->hired_date            = $request->hired_date;
            $employeeinformation->assumption_date       = $request->assumption_date;
            $employeeinformation->resigned_date         = $request->resigned_date;
            $employeeinformation->rehired_date          = $request->rehired_date;
            $employeeinformation->start_date            = $request->start_date;
            $employeeinformation->end_date              = $request->end_date;
            $employeeinformation->created_by            = Auth::User()->id;

            $employeeinformation->save();

	    	$employees->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

            $this->validate($request,[
                'firstname'             => 'required',
                'lastname'              => 'required',
                // 'middlename'            => 'required',
                // 'division_id'           => 'required',
                // 'office_id'             => 'required',
                'employee_status_id'    => 'required',
                // 'position_item_id'      => 'required',
            ]);

    		$employees->firstname   = $request->firstname;
            $employees->lastname    = $request->lastname;
            $employees->middlename  = $request->middlename;
            $employees->active      = $request->status;
            $employees->created_by  = Auth::User()->id;

            $employees->save();

            $employeeinformation->employee_id           = $employees->id;
            $employeeinformation->division_id           = $request->division_id;
            $employeeinformation->department_id         = $request->department_id;
            $employeeinformation->office_id             = $request->office_id;
            $employeeinformation->employee_status_id    = $request->employee_status_id;
            $employeeinformation->position_id           = $request->position_id;
            $employeeinformation->hired_date            = $request->hired_date;
            $employeeinformation->assumption_date       = $request->assumption_date;
            $employeeinformation->resigned_date         = $request->resigned_date;
            $employeeinformation->rehired_date          = $request->rehired_date;
            $employeeinformation->start_date            = $request->start_date;
            $employeeinformation->end_date              = $request->end_date;
            $employeeinformation->created_by            = Auth::User()->id;

            $employeeinformation->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
