<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\EmployeeStatus;
class GIPDeductionReportsController extends Controller
{
    function __construct(){
		$this->title = 'GIP SUMMARY DEDUCTED REPORT';
    	$this->module = 'gipdeductionreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;
        $transaction 		 = new NonPlantillaTransaction;

        $empstatus = $employeestatus
        ->where('code','GIP')
        ->select('RefId')
        ->first();

        $employee_id = $employeeinformation
        ->where('employee_status_id',$empstatus->RefId)
        ->select('employee_id')
        ->get()
        ->toArray();

        $query['transaction'] = $transaction
        ->with([
                'nonplantilla',
                'employeeinformation',
                'employees',
                'positionitems',
                'positions',
                'offices'
        ])
        ->whereIn('employee_id',$employee_id)
		->where('year',$year)
		->where('month',$month)
		->get();

        return json_encode($query);
    }
}
