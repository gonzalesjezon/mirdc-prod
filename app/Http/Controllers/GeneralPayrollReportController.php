<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
class GeneralPayrollReportController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL REPORT';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::all();

        $transaction = new Transaction;

        $year = $q['year'];
        $month = $q['month'];

        $query = $transaction
        ->with([
            'employees',
            'positionitems',
            'positions',
            'employeeinfo',
            'longevity',
            'offices'=>function($qry){
                $qry->orderBy('name','asc');
            },
            'employeeinformation',
            'salaryinfo',
            'gettax' =>function($qry) use($year,$month){
                $qry = $qry->where('for_year',$year)
                ->where('for_month',$month);
            },
            'deductionTransaction'=>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'benefitTransaction'=>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'loanTransaction' =>function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        // $data = [];
        // if(count($query) > 0){
        //     foreach ($query as $key => $value) {
        //         $data[$value->offices->name][$key] = $value;
        //     }

        // }else{
        //     $data = [];
        // }

        return json_encode($query);
    }
}
