<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveMonetizationTransaction;
use App\EmployeeInformation;
use App\Employee;
use App\EmployeeStatus;
use App\SalaryInfo;

use Input;
use Auth;
class LeaveMonetizationTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'LEAVE MONETIZATION';
    	$this->controller = $this;
    	$this->module = 'leavemonetizations';
        $this->module_prefix = 'payrolls/otherpayrolls';
    }

    public function index(){



    	$response = array(
           	'title' 	        => $this->title,
           	'controller'        => $this->controller,
           	'module'	       	=> $this->module,
           	'module_prefix'     => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_pei 	= Input::get('check_pei');
        $checkpei 	= Input::get('checkpei');
        $data = "";

        $data = $this->searchName($q);

        // if(isset($year) || isset($month) || isset($checkpei)){
        //     $data = $this->filter($year,$month,$checkpei);
        // }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new LeaveMonetizationTransaction;

        $cols = ['lastname','firstname'];

        $plantilla = ['Permanent','Temporary','Coterminous','Termer','Probationary'];


        $empstatus_id = $employee_status->whereIn('name',$plantilla)
                                        ->select('id')
                                        ->get()
                                        ->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)
                                        ->select('employee_id')
                                        ->get()
                                        ->toArray();

      $query = $employee->where('with_setup',1)->whereIn('id',$employee_info_id);
      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }


    public function filter($year,$month,$checkpei){


        $plantilla = ['Permanent','Temporary','Coterminous','Termer','Probationary'];

        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $transaction            = new LeaveMonetizationTransaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;



        $empstatus_id = $employee_status
                        ->whereIn('name',$plantilla)
                        ->select('id')
                        ->get()
                        ->toArray();

        $employee_id  = $employee_information
                        ->whereIn('employee_status_id',$empstatus_id)
                        ->select('employee_id')
                        ->get()
                        ->toArray();

        $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpei) {
            case 'wpei':

                $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            default:

                 $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        // ->whereIn('id',$salary_info_employee_id)
                                ->whereNotIn('id',$query)
                                ->where('active',1)
                                ->where('with_setup',1)
                                ->orderBy('lastname','asc')->get();



                break;
        }
        return $response;
    }

    public function store(Request $request){

		$transaction = new LeaveMonetizationTransaction;

		$basic_amount = str_replace(',', '', $request->basic_amount);
		$net_amount = ((float)$basic_amount*$request->factor_rate*$request->number_of_days);

		if(isset($request->monetization_id)){

			$transaction = $transaction->find($request->monetization_id);

	    	$transaction->position_id 			=  $request->position_id;
	    	$transaction->salary_grade_id 		=  $request->salary_grade_id;
            $transaction->employee_number       =  $request->employee_number;
	    	$transaction->number_of_days 		=  $request->number_of_days;
	    	$transaction->factor_rate 			=  $request->factor_rate;
	    	$transaction->net_amount 			=  $net_amount;
	    	$transaction->salary_amount 		=  $basic_amount;
	    	$transaction->updated_by 			=  Auth::User()->id;

	    	$transaction->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

		}else{

			$request->validate([
				'number_of_days' => 'required',
			]);

			$transaction->position_id 			=  $request->position_id;
			$transaction->employee_id 			=  $request->employee_id;
	    	$transaction->salary_grade_id 		=  $request->salary_grade_id;
	    	$transaction->number_of_days 		=  $request->number_of_days;
            $transaction->employee_number       =  $request->employee_number;
	    	$transaction->factor_rate 			=  $request->factor_rate;
	    	$transaction->net_amount 			=  $net_amount;
	    	$transaction->salary_amount 		=  $basic_amount;
	    	$transaction->created_by 			=  Auth::User()->id;

	    	$transaction->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);

		}
        return $response;
    }

    public function showLeaveMonetizationDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getLeaveMonetization(){
    	$data = Input::all();

    	$transaction 	= new LeaveMonetizationTransaction;
    	$salaryinfo 	= new SalaryInfo;
    	$employeeinfo 	= new EmployeeInformation;

    	$query['transaction'] = $transaction
				    			->with('positions','salaryinfo','salarygrade')
						    	->where('employee_id',@$data['employee_id'])
				                ->orderBy('created_at','desc')
				                ->get();

		$query['salaryinfo'] = $salaryinfo
        ->where('employee_id',@$data['employee_id'])
        ->orderBy('created_at','desc')
        ->first();

		$query['employeeinfo'] = $employeeinfo
        ->where('employee_id',@$data['employee_id'])
        ->first();

    	return json_encode($query);
    }

    public function deleteLeaveMonetization(){
        $data = Input::all();

        $transaction = new LeaveMonetizationTransaction;

        $transaction
        ->where('employee_id',$data['empid'])
        ->where('month',$data['month'])
        ->where('year',$data['year'])
        ->delete();

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
