<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Loan;

class LoansController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'LOANS';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'loans';
        $this->table = 'loans';
    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $loan = new Loan;

        if(isset($request->loan_id)){

            $loan = Loan::find((int)$request->loan_id);

            $loan->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $this->validate(request(),[
                'code'                  => 'required',
                'name'                  => 'required',
            ]);

            $loan->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);
        }

        return $response;
    }

    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name','loan_type'];


        $query = Loan::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = Loan::where('id',$id)->first();

        return json_encode($query);
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new Loan;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
