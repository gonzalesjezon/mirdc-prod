<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\LongevityPay;
use App\SalaryInfo;
class LongevityPayController extends Controller
{
    function __construct(){
    	$this->title = 'LONGEVITY SETUP';
    	$this->module = 'longevitysetup';
        $this->module_prefix = 'payrolls/admin';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employees           = new Employee;

        $status = $employeestatus
                ->where('category',1)
                ->select('RefId')
                ->get()->toArray();

        $employee_id = $employeeinformation
                    ->whereIn('employee_status_id',$status)
                    ->select('employee_id')
                    ->get()->toArray();

        $query = $employees
                ->whereIn('id',$employee_id)
                ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($empstatus,$category,$emp_type,$searchby){

        $employee_status     = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;


        $empstatus_id = [];
        switch($empstatus){
            case 'plantilla':
                $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = $employee_status->where('category',0)->select('RefId')->get()->toArray();
            break;
        }

        $employeesRefId = [];
        $employeesRefId = $employeeinformation->select('employee_id')
                            ->where(function($qry) use($category,$searchby){
                                switch ($searchby) {
                                    case 'company':
                                        $qry =  $qry->where('company_id',$category);
                                        break;
                                    case 'position':
                                        $qry =  $qry->where('position_id',$category);
                                        break;
                                    case 'division':
                                        $qry =  $qry->where('division_id',$category);
                                        break;
                                    case 'office':
                                        $qry =  $qry->where('office_id',$category);
                                        break;
                                    case 'department':
                                        $qry =  $qry->where('department_id',$category);
                                        break;
                                }

                            });
                if(count($empstatus_id) > 0){
                    $employeesRefId = $employeesRefId->whereIn('employee_status_id',$empstatus_id);
                }
                $employeesRefId = $employeesRefId->get()->toArray();

        $query = Employee::whereIn('id',$employeesRefId)->where('active',1)->orderBy('lastname','asc')->get();

        return $query;


    }

    public function showLongevityPay(){

        $longevitypay    = LongevityPay::with('employees')->get();

        $response       = array(
                            'longevitypay'   => $longevitypay,
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function store(Request $request){

        $basic_pay_amount = ($request->basic_salary_amount) ? str_replace(',', '', $request->basic_salary_amount) : '';
        $longevity_amount = ($request->longevity_amount) ? str_replace(',', '', $request->longevity_amount) : '';


        $longevitypay =  new LongevityPay;

        if(isset($request->longevity_id)){
            $longevitypay = $longevitypay->find($request->longevity_id);
            $longevitypay->employee_id          = $request->employee_id;
            $longevitypay->basic_pay_amount     = $basic_pay_amount;
            $longevitypay->longevity_amount     = $longevity_amount;
            $longevitypay->employee_number      = $request->employee_number;
            $longevitypay->lp                   = $request->lp;
            $longevitypay->longevity_date       = $request->longevity_date;

            $longevitypay->save();

            $response = json_encode(['status'=>true, 'response'=>'Update Successfully!']);

        }else{

            $this->validate($request,[
                'basic_salary_amount' => 'required',
                'longevity_amount' => 'required',
                'lp'               => 'required',
                'longevity_date'   => 'required|unique:pms_longevitypay'
            ]);

            // $end = date('Y-m-d', strtotime('+5 years'));

            $salaryinfo = SalaryInfo::where('employee_id',$request->employee_id)->first();

            $basic_amount_one = ($salaryinfo->basic_amount_one) ? $salaryinfo->basic_amount_one : 0;
            $basic_amount_two = ($salaryinfo->basic_amount_two) ? $salaryinfo->basic_amount_two : 0;

            $basic_amount = (float)$basic_amount_one + (float)$basic_amount_two;

            $longevitypay->employee_id          = $request->employee_id;
            $longevitypay->basic_pay_amount     = $basic_amount;
            $longevitypay->longevity_amount     = $longevity_amount;
            $longevitypay->lp                   = $request->lp;
            $longevitypay->employee_number      = $request->employee_number;
            $longevitypay->longevity_date       = $request->longevity_date;

            $longevitypay->save();

            $response = json_encode(['status'=>true, 'response'=>'Save Successfully!']);

        }

        return $response;
    }

    public function getLongevityPay(){
        $data = Input::all();

        $employee_id = $data['id'];

        $longevitypay =  new LongevityPay;
        $employee     =  new Employee;
        $salaryinfo   =  new SalaryInfo;

        $employee = $employee->where('id',$employee_id)->first();

        $query = $salaryinfo
        ->where('employee_id',@$employee_id)
        ->orderBy('salary_effectivity_date','desc')
        ->first();

        $query2 = $longevitypay->with(['salaryinfo'=>function($qry){
                                $qry->orderBy('salary_effectivity_date','desc');
                            }])->where('employee_id',@$employee_id)
                            ->orderBy('longevity_date','desc')
                            ->get();

        $date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(@$query->longevity_date)) . " + 5 year"));

        $basic_amount_one = ($query->basic_amount_one) ? $query->basic_amount_one : 0;
        $basic_amount_two = ($query->basic_amount_two) ? $query->basic_amount_two : 0;
        $basic_amount = ((float)$basic_amount_one + (float)$basic_amount_two);

        $lp = @$query->lp;

        $longevity['longevity_date'] = $date;
        $longevity['employee_number'] = $employee->employee_number;
        $longevity['basic_amount']   = $basic_amount;
        $longevity['lp']   = $lp + 5;
        $longevity['longevity_amount'] = $basic_amount * .05;


        return json_encode(['longevity'=>$longevity,'res'=>$query2]);
    }


}
