<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\EmployeeInfo;
use App\PositionItem;
class OtherBenefitsController extends Controller
{
    function __construct(){
    	$this->title = 'OTHER BENEFITS';
    	$this->controller = $this;
    	$this->module = 'otherbenefits';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_pbb 	= Input::get('check_pbb');
        $checkpbb 	= Input::get('checkpbb');
        $data = "";

        $data = $this->searchName($q,$check_pbb);

        if(isset($year) || isset($month) || isset($checkpbb)){
            $data = $this->filter($year,$month,$checkpbb);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkpbb){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkpbb) {
            case 'wpbb':
               $employee_id = $transaction->whereIn('employee_id',$employee_info_id)
               ->where('status','otherbenefits')
               ->select('employee_id')
               ->get()
               ->toArray();
               $query = $employee->whereIn('id',$employee_id);

                break;

            case 'wopbb':
               $employee_id = $transaction->whereIn('employee_id',$employee_info_id)
               ->where('status','otherbenefits')
               ->select('employee_id')
               ->get()
               ->toArray();

               $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$employee_info_id);

                break;

            default:

                $query = $employee
                ->whereIn('id',$employee_info_id);

                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$checkpbb){

        $employee_status        = new EmployeeStatus;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $employee_information   = new EmployeeInformation;
        $employeeinfo           = new EmployeeInfo;

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        // $salary_info_employee_id = $employeeinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpbb) {
            case 'wpbb':

                $query =  $transaction->select('employee_id')->where('status','otherbenefits');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'wopbb':

                 $query =  $transaction->select('employee_id')->where('status','otherbenefits');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        // ->whereIn('id',$salary_info_employee_id)
                                        ->whereNotIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processOtherBenefits(Request $request){
        $data = Input::all();

        $employeeinformation = new EmployeeInformation;

        foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $employeeinfo   = $employeeinformation->where('employee_id',$value)->first();

                $transaction = new SpecialPayrollTransaction;

                $transaction->employee_id       = $value;
                $transaction->office_id         = @$employeeinfo->office_id;
                $transaction->position_id       = @$employeeinfo->position_id;
                $transaction->division_id       = @$employeeinfo->division_id;
                $transaction->employee_number   = @$employeeinfo->employee_number;
                // $transaction->responsibility_id = @$employeeinfo->responsibility_id;
                $transaction->amount            = $data['pbb_amount'];
                $transaction->tax_amount        = $data['tax_amount'];
                $transaction->deduction_amount  = $data['deduction_amount'];
                $transaction->year              = $data['year'];
                $transaction->month             = $data['month'];
                $transaction->status            = 'otherbenefits';
                $transaction->created_by        = Auth::User()->id;
                $transaction->save();

            }
        }

        $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

        return $response;
    }

    public function showOtherBenefits(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getOtherBenefits(){
        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];
        $employee_id = @$data['employee_id'];

        $transaction =  new SpecialPayrollTransaction;

        $query = $transaction->with('positions','offices')
                    ->where('year',$year)
                    ->where('month',$month)
                    ->where('status','otherbenefits')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('created_at','desc')
                    ->first();

        return json_encode($query);
    }

    public function deleteOtherBenefits(){
        $data = Input::all();

        $transaction = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transaction->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','otherbenefits')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
