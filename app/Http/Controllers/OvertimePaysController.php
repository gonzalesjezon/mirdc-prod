<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\OvertimePay;
use App\Employee;
use App\Transaction;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\AttendanceInfo;
class OvertimePaysController extends Controller
{
    function __construct(){
    	$this->title = 'OVERTIME PAY';
    	$this->module = 'overtimepay';
        $this->module_prefix = 'payrolls';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');
        $chkovertime    = Input::get('check_overtime');
        $chk_overtime    = Input::get('chk_overtime');
        $employee_status = Input::get('status');


        $data = $this->searchName($q,$chk_overtime);

        if(isset($year) || isset($month) || isset($chkovertime) || isset($employee_status)){
            $data = $this->filter($year,$month,$chkovertime,$employee_status);
        }


        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function searchName($q,$chk_overtime){

        $cols = ['lastname','firstname'];

        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new OvertimePay;

        $employee_info_id = $employee->select('id')->get()->toArray();

         $query = [];
        switch ($chk_overtime) {
            case 'wovertime':
               $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
               $query = $employee->whereIn('id',$employee_id);
                break;

            default:
                $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereNotIn('id',$employee_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$chkovertime,$employee_status){

        $empstatus_id = [];
        switch($employee_status){
            case 'plantilla':
                $empstatus_id = EmployeeStatus::where('category',1)->select('RefId')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = EmployeeStatus::where('category',0)->select('RefId')->get()->toArray();
            break;
        }

        $employee_information = new EmployeeInformation;

        $employee_id  = $employee_information->select('employee_id')->whereIn('employee_status_id',$empstatus_id)->get()->toArray();

        $query = [];
        $response = "";
        switch ($chkovertime) {
            case 'wovertime':
                $query = OvertimePay::select('employee_id');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = Employee::whereIn('id',$query)->where('active',1)->orderBy('lastname','asc')->get();
                break;
            default:
                $query = OvertimePay::select('employee_id');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = Employee::whereIn('id',$employee_id)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')
                                        ->get();

                break;
        }
        return $response;
    }

    public function getOvertimeInfo(){
        $data = Input::all();

        $year  = $data['year'];
        $month = $data['month'];

        $employeeinfo = new EmployeeInfo;
        $nonplantilla = new NonPlantillaEmployeeInfo;
        $overtimepay  = new OvertimePay;

        $query['plantilla'] = $employeeinfo
        ->where('employee_id',$data['employee_id'])
        ->first();

        $query['nonplantilla'] = $nonplantilla
        ->where('employee_id',$data['employee_id'])
        ->first();

        $query['overtimepay'] = $overtimepay
        ->where('employee_id',@$data['employee_id'])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('created_at','desc')
        ->first();

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $workdays = $this->countDays($y,$m,array(0,6));

        $query['workdays'] = $workdays;

        return json_encode($query);
    }

    public function storeOvertimeInfo(Request $request){


        if(isset($request->overtime_id)){
            $overtimepay = new OvertimePay;

            $overtimepay->employee_id                     = $request->empid;
            $overtimepay->previous_balance                = str_replace(',', '',$request->previous_balance);
            $overtimepay->used_amount                     = str_replace(',', '',$request->used_amount);
            $overtimepay->available_balance               = str_replace(',', '',$request->available_balance);
            $overtimepay->actual_regular_overtime         = $request->actual_regular_overtime;
            $overtimepay->adjust_regular_overtime         = $request->adjust_regular_overtime;
            $overtimepay->total_regular_amount            = str_replace(',', '',$request->total_regular_amount);
            $overtimepay->actual_special_overtime         = $request->actual_special_overtime;
            $overtimepay->adjust_special_overtime         = $request->adjust_special_overtime;
            $overtimepay->total_special_amount            = str_replace(',', '',$request->total_special_amount);
            $overtimepay->actual_regular_holiday_overtime = $request->actual_regular_holiday_overtime;
            $overtimepay->adjust_regular_holiday_overtime = $request->adjust_regular_holiday_overtime;
            $overtimepay->total_regular_holiday_amount    = str_replace(',', '',$request->total_regular_holiday_amount);
            $overtimepay->total_overtime_amount           = str_replace(',', '',$request->total_overtime_amount);
            $overtimepay->pay_period                      = $request->pay_period;
            $overtimepay->sub_pay_period                  = $request->sub_pay_period;
            $overtimepay->year                            = $request->year;
            $overtimepay->month                           = $request->month;

            $overtimepay->save();

            $response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
        }else{

            foreach ($request['employee_id'] as $key => $value) {

                if(isset($value)){
                    $overtimepay      = new OvertimePay;
                    if($request->status == 'plantilla' ){
                        $employeeinfo = EmployeeInfo::where('employee_id',$value)->first();
                    }else{
                        $employeeinfo = NonPlantillaEmployeeInfo::where('employee_id',$value)->first();
                    }

                    $overtimepay->employee_id                     = $value;
                    $overtimepay->previous_balance                = $employeeinfo->overtime_balance_amount;
                    $overtimepay->employee_number                 = $employeeinfo->employee_number;
                    $overtimepay->year                            = $request->year;
                    $overtimepay->month                           = $request->month;
                    $overtimepay->pay_period                      = $request->pay_period;
                    $overtimepay->sub_pay_period                  = $request->sub_pay_period;
                    $overtimepay->save();
                }


            }

            $response = json_encode(['status'=>true,'response'=>'Save Successfully!']);

        }

        return $response;

    }

    public function deleteOvertime(){
        $data = Input::all();

        $overtimepay = new OvertimePay;

        foreach ($data['empid'] as $key => $value) {

            $overtimepay->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
