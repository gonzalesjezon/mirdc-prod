<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\AttendanceInfo;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use App\EmployeeInfo;
use App\OvertimePay;
use App\Employee;
use App\Transaction;
class OvertimeReportsController extends Controller
{
     function __construct(){
		$this->title = 'OVERTIME REPORT';
    	$this->module = 'overtime';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$employeeinfo = Employee::orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getOvertimeReport(){

        $data = Input::all();

        $transaction              = new Transaction;
        $employeeinfo             = new EmployeeInfo;
        $nonplantilla_transaction = new NonPlantillaTransaction;
        $nonplantilla_employeeinfo = new NonPlantillaEmployeeInfo;
        $attendanceinfo           = new AttendanceInfo;
        $overtime                 = new OvertimePay;


        $query['plantilla_employee'] = $employeeinfo->with('employees','taxpolicy')
                                                ->where('employee_id',$data['id'])
            									->first();

        $query['nonplantilla_employee'] = $nonplantilla_employeeinfo->with('employees','taxpolicyOne','taxpolicyTwo')
                                                ->where('employee_id',$data['id'])
                                                ->first();

        $query['overtimeinfo'] = $overtime->where('year',$data['year'])
                                            ->where('month',$data['month'])
                                            ->where('employee_id',$data['id'])
                                            ->where('pay_period',$data['pay_period'])
                                            ->where('sub_pay_period',@$data['sub_pay_period'])
                                            ->orderBy('created_at','desc')
                                            ->first();


        $query['attendanceinfo'] =  $attendanceinfo->where('employee_id',$data['id'])
                                                ->orderBy('created_at','desc')
                                                ->first();

        return json_encode($query);
    }

}
