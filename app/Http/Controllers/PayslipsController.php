<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfoTransaction;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\SalaryInfo;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\OvertimePay;
use App\EmployeeStatus;
use App\EmployeeInformation;

use Input;

class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $employee               = new Employee;

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();
        $employee_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();

        $employeeinfo = $employee->whereIn('id',$employee_id)->orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getPayslip(){
        $data = Input::all();

        $transaction         = new Transaction;
        $overtimepay         = new OvertimePay;
        $employeeinformation =  new EmployeeInformation;
        $employeeinfo        =  new EmployeeInfo;
        $deductioninfo       = new DeductionInfoTransaction;
        $loaninfo            = new LoanInfoTransaction;
        $benefitinfo         = new BenefitInfoTransaction;
        $salaryinfo          =  new SalaryInfo;

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        // $filtered = $this->filter($data['emp_status'],$data['category'],$data['emp_type'],$data['searchby']);
        if(isset($data['id'])){

            $query['transaction'] = $transaction
                                ->with([
                                    'employees',
                                    'positionitems',
                                    'positions',
                                    'offices'
                                ])
                                ->where('employee_id',$employee_id)
                                ->where('year',$year)
                                ->where('month',$month)
                                ->first();

            $query['overtime'] = $overtimepay
                                ->where('employee_id',$data['id'])
                                ->where('year',$year)
                                ->where('month',$month)
                                ->orderBy('created_at','desc')
                                ->first();

            $query['employeeinformation'] = $employeeinformation
                                        ->with(
                                            'departments',
                                            'divisions',
                                            'positions'
                                        )
                                        ->where('employee_id',$data['id'])
                                        ->first();

            $query['employeeinfo'] = $employeeinfo
                                    ->with('employees')
                                    ->where('employee_id',$data['id'])
                                    ->first();


             $query['deductioninfo'] = $deductioninfo
                                    ->with('deductions')
                                    ->where('employee_id',$data['id'])
                                    ->where('year',$year)
                                ->where('month',$month)
                                    ->get();

            $query['loaninfo'] = $loaninfo
                                ->with('loans')
                                ->where('employee_id',$data['id'])
                                ->where('year',$year)
                                ->where('month',$month)
                                ->get();

            $query['benefitinfo'] = $benefitinfo
                                    ->with('benefits')
                                    ->where('employee_id',$data['id'])
                                    ->where('year',$year)
                                    ->where('month',$month)
                                    ->get();


            $query['salaryinfo'] = $salaryinfo
                                    ->where('employee_id',$data['id'])
                                    ->orderBy('salary_effectivity_date','desc')
                                    ->first();

            return json_encode($query);

        }
    }


}
