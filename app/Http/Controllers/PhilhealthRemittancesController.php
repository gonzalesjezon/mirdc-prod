<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use Input;
class PhilhealthRemittancesController extends Controller
{
   function __construct(){
		$this->title = 'PHILHEALTH PREMIUMS';
    	$this->module = 'philhealth';
        $this->module_prefix = 'payrolls/reports/remittances';
    	$this->controller = $this;
	}

	public function index(){


		$response = array(
			'module'        => $this->module,
			'controller'    => $this->controller,
			'module_prefix' => $this->module_prefix,
			'title'		    => $this->title
		);

		return view($this->module_prefix.'.'.$this->module.'.index',$response);
	}


   public function show(){

        $q = Input::all();

        $transaction = new Transaction;

        $query = $transaction
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
             },
            'positionitems',
            'positions',
            'offices',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('year',$q['year'])
        ->where('month',$q['month'])
        ->get();
        // $data = [];
        // if(count($query) > 0){

        //     foreach ($query as $key => $value) {

        //         $data[$value->offices->name][$key] = $value;
        //     }

        // }else{

        //     $data = [];
        // }

        return json_encode($query);
    }
}
