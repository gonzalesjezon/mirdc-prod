<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
class RataReportsController extends Controller
{
    function __construct(){
    	$this->title = 'REPRESENTATION AND TRANSPORTATION ALLOWANCE';
    	$this->module = 'rata';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::all();

        $query = Rata::with(['salaryinfo','employees',
                    'positionitems'=>function($qry){
                    $qry->with('positions');
                },'offices','leave'])
                ->where('year',$q['year'])
                ->where('month',$q['month']);

        $query = $query->orderBy('office_id','desc')->get();

        return json_encode($query);

    }
}
