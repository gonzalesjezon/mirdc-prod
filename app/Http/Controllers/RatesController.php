<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Rate;
class RatesController extends Controller
{
    function __construct(){
    	$this->title 		 = 'RATE';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'rates';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        	=> $this->module,
						'controller'    	=> $this->controller,
		                'module_prefix' 	=> $this->module_prefix,
						'title'		    	=> $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['rate','status'];

        $query = Rate::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('rate','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$rate = new Rate;

    	if(isset($request->rate_id)){

    		$rate = Rate::find($request->rate_id);

	    	$rate->rate 	   	= (int)$request->rate/100;
	    	$rate->status 	   	= $request->status;
	    	$rate->updated_by 	= Auth::User()->id;

	    	$rate->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

            $this->validate($request,[
                'rate'  => 'required|unique:pms_rates',
            ]);

    		$rate->rate 	   	= (int)$request->rate/100;
    		$rate->status 	   	= $request->status;
	    	$rate->created_by 	= Auth::User()->id;

	    	$rate->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new Rate;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
