<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\SalaryInfo;
use App\Rate;
use DateTime;
class RiceTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'RICE ALLOWANCE';
    	$this->controller = $this;
    	$this->module = 'ricetransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_pei 	= Input::get('check_pei');
        $checkpei 	= Input::get('checkpei');
        $data = "";

        $data = $this->searchName($q,$check_pei);

        if(isset($year) || isset($month) || isset($checkpei)){
            $data = $this->filter($year,$month,$checkpei);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkpei){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new SpecialPayrollTransaction;
        $benefitinfo                = new BenefitInfo;
        $benefit                    = new Benefit;

        $cols = ['lastname','firstname'];

        $benefit_id =  $benefit->where('code','RA')->select('id')->first();
        $benefit_info   = $benefitinfo->where('benefit_id',$benefit_id->id)
                                        ->select('employee_id')
                                        ->get()->toArray();

        $empstatus_id = $employee_status->where('category',1)
                                        ->select('RefId')
                                        ->get()
                                        ->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)
                                        ->whereIn('employee_id',$benefit_info)
                                        ->select('employee_id')
                                        ->get()
                                        ->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkpei) {
            case 'wpei':
               $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->where('status','ra')
                                           ->whereNotNull('mid_year_amount')
                                           ->select('employee_id')
                                           ->get()->toArray();

               $query = $employee->whereIn('id',$employee_id);

                break;

            default:
                $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereNotIn('id',$employee_id)->where('with_setup',1)->whereIn('id',$employee_info_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function filter($year,$month,$checkpei){


        // $plantilla = ['Permanent','Temporary','Coterminous','Termer','Probationary'];

        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;
        $benefitinfo            = new BenefitInfo;
        $benefit                = new Benefit;


        $benefit_id =  $benefit->where('code','RA')->select('id')->first();

        $benefit_info   = $benefitinfo->where('benefit_id',$benefit_id->id)
                                        ->select('employee_id')
                                        ->get()->toArray();


        $empstatus_id = $employee_status
                        ->where('category',1)
                        ->select('RefId')
                        ->get()
                        ->toArray();

        $employee_id  = $employee_information
                        ->whereIn('employee_status_id',$empstatus_id)
                        ->whereIn('employee_id',$benefit_info)
                        ->select('employee_id')
                        ->get()
                        ->toArray();

        $query = [];
        $response = "";
        switch ($checkpei) {
            case 'wpei':

                $query =  $transaction->select('employee_id')->where('status','ra');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            default:

                 $query =  $transaction->select('employee_id')->where('status','ra');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee
                                        // ->whereIn('id',$salary_info_employee_id)
                                ->whereNotIn('id',$query)
                                ->whereIn('id',$employee_id)
                                ->where('active',1)
                                ->where('with_setup',1)
                                ->orderBy('lastname','asc')->get();


                break;
        }
        return $response;
    }


    public function processRice(Request $request){
    	$data = Input::all();

    	$benefitinfo 		 = new BenefitInfo;
    	$employeeinfo        = new EmployeeInformation;
        $positionitem 		 = new PositionItem;
        $benefit 			 = new Benefit;

        $benefit_id =  $benefit->where('code','RA')->select('id')->first();

    	foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

        		$benefit_info 	= $benefitinfo->where('employee_id',$value)
    											    		->where('benefit_id',$benefit_id->id)
    											    		->orderBy('benefit_effectivity_date','desc')
    											    		->first();
                if(isset($benefit_info)){

                    $transaction     = new SpecialPayrollTransaction;
                    $employeeinfo    = $employeeinfo->where('employee_id',$value)->first();
                    $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

                    $first_date = new DateTime($assumption_date);
                    $nov = date('Y').'-11-30 00:00:00';

                    $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
                    $interval = $first_date->diff($second_date);

                    // $x = $interval->format('%Y years %M months and %D days.');
                    $number_of_month = (int)$interval->format('%M');
                    $number_of_year = (int)$interval->format('%Y');

                    if($number_of_year !== 0){

                        $transaction->employee_id  = $value;
                        $transaction->position_id  = @$employeeinfo->position_id;
                        $transaction->employee_number  = @$employeeinfo->employee_number;
                        $transaction->amount       = ((float)$benefit_info->benefit_amount*12);
                        $transaction->benefit_info_id       = $benefit_info->id;
                        $transaction->no_of_months_entitled = 12;
                        $transaction->year         = $data['year'];
                        $transaction->month        = $data['month'];
                        $transaction->status       = 'ra';

                        $transaction->save();

                    }else{

                        $transaction->employee_id  = $value;
                        $transaction->position_id  = @$employeeinfo->position_id;
                        $transaction->amount       = ((float)$benefit_info->benefit_amount * (int)$number_of_month);
                        $transaction->benefit_info_id       = $benefit_info->id;
                        $transaction->no_of_months_entitled = (int)$number_of_month;
                        $transaction->year         = $data['year'];
                        $transaction->month        = $data['month'];
                        $transaction->status       = 'ra';

                        $transaction->save();
                    }
                }

            }

    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showRiceDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getRice(){
    	$data = Input::all();

    	$transaction =  new SpecialPayrollTransaction;

    	$query = $transaction->with('positions')
                    ->where('status','ra')
                    ->where('year',$data['year'])
			    	->where('month',$data['month'])
			    	->where('employee_id',@$data['employee_id'])->get();

    	return json_encode($query);
    }

    public function deleteRice(){
        $data = Input::all();

        $transaction = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transaction->where('employee_id',$data['empid'][$key])
            ->where('status','ra')
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
