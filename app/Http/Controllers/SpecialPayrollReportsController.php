<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class SpecialPayrollReportsController extends Controller
{
    function __construct(){
		$this->title = 'OTHER PAYROLL REPORT';
    	$this->module = 'specialpayrollreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year   	   = $data['year'];
        $month  	   = $data['month'];
        $payroll_type  = $data['payroll_type'];

        if($payroll_type == 'cashgift'){
            $payroll_type = 'yearendbonus';
        }

        $transaction =  new SpecialPayrollTransaction;

        $query = $transaction
        ->with('divisions','employees')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status',$payroll_type)
        ->get();

        $payroll = [];
        foreach ($query as $key => $value) {
        	$payroll[@$value->divisions->Code][$key] = $value;
        }

        return json_encode($payroll);

    }
}
