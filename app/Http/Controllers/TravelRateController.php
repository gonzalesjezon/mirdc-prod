<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\TravelRate;
class TravelRateController extends Controller
{
    function __construct(){
    	$this->title 		 = 'TRAVEL RATE';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'travelrates';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        	=> $this->module,
						'controller'    	=> $this->controller,
		                'module_prefix' 	=> $this->module_prefix,
						'title'		    	=> $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['rate_amount','status'];

        $query = TravelRate::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('rate_amount','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$rate = new TravelRate;

    	if(isset($request->travelrate_id)){

    		$rate = TravelRate::find($request->travelrate_id);

	    	$rate->rate_amount 	   	= $request->rate_amount;
	    	$rate->status 	   	= $request->status;
	    	$rate->updated_by 	= Auth::User()->id;

	    	$rate->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

            $this->validate($request,[
                'rate_amount' => 'required',
                'status'      => 'required'
            ]);


    		$rate->rate_amount 	   	= $request->rate_amount;
    		$rate->status 	   	= $request->status;
	    	$rate->created_by 	= Auth::User()->id;

	    	$rate->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }

    public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $benefit = new TravelRate;
        $benefit->destroy($id);

        return json_encode(['status'=>true]);
    }
}
