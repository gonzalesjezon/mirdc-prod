<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\EmployeeInformation;
use App\TravelRateTransaction;
use App\SalaryInfo;
use App\Rate;
use App\TravelRate;
use DateTime;
class TravelRateTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'TRAVEL ALLOWANCE';
    	$this->controller = $this;
    	$this->module = 'traveltransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){

    	$rate = Rate::where('status','travelrate')->get();
    	$travelrate = TravelRate::where('status','travelrate')->get();


    	$response = array(
    		'rate' 				=> $rate,
    		'travelrate' 		=> $travelrate,
           	'title' 	        => $this->title,
           	'controller'        => $this->controller,
           	'module'	       	=> $this->module,
           	'module_prefix'     => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_pei 	= Input::get('check_pei');
        $checkpei 	= Input::get('checkpei');
        $data = "";

        $data = $this->searchName($q);

        // if(isset($year) || isset($month) || isset($checkpei)){
        //     $data = $this->filter($year,$month,$checkpei);
        // }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $salaryinfo                 = new SalaryInfo;
        $transaction                = new TravelRateTransaction;
        $benefitinfo                = new BenefitInfo;
        $benefit                    = new Benefit;

        $cols = ['lastname','firstname'];


        $empstatus_id = $employee_status->where('category',1)
                                        ->select('RefId')
                                        ->get()
                                        ->toArray();

        $employee_info_id = $employee_info->whereIn('employee_status_id',$empstatus_id)
                                        ->select('employee_id')
                                        ->get()
                                        ->toArray();

      $query = $employee->where('with_setup',1)->whereIn('id',$employee_info_id);
      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }


    public function filter($year,$month,$checkpei){



        $employee_status        = new EmployeeStatus;
        $employee_information   = new EmployeeInformation;
        $transaction            = new TravelRateTransaction;
        $employee               = new Employee;
        $salaryinfo             = new SalaryInfo;



        $empstatus_id = $employee_status
                        ->where('category',1)
                        ->select('RefId')
                        ->get()
                        ->toArray();

        $employee_id  = $employee_information
                        ->whereIn('employee_status_id',$empstatus_id)
                        ->select('employee_id')
                        ->get()
                        ->toArray();

        $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpei) {
            case 'wpei':

                $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            default:

                 $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        // ->whereIn('id',$salary_info_employee_id)
                                ->whereNotIn('id',$query)
                                ->where('active',1)
                                ->where('with_setup',1)
                                ->orderBy('lastname','asc')->get();



                break;
        }
        return $response;
    }

    public function store(Request $request){

		$transaction = new TravelRateTransaction;

		$travel_rate_amount = ((float)$request->rate_amount * $request->rate);
		$net_amount = ((float)$travel_rate_amount*$request->number_of_days);

		if(isset($request->travel_allowance_id)){

			$transaction = $transaction->find($request->travel_allowance_id);

	    	$transaction->travel_rate_id 		=  $request->travel_rate_id;
            $transaction->employee_number       =  $request->employee_number;
	    	$transaction->rate_id 				=  $request->rate_id;
	    	$transaction->number_of_days 		=  $request->number_of_days;
	    	$transaction->travel_rate_amount 	=  $travel_rate_amount;
	    	$transaction->net_amount 			=  $net_amount;
	    	$transaction->updated_by 			=  Auth::User()->id;
// dd($transaction);
	    	$transaction->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

		}else{

			$request->validate([
				'travel_rate_id' => 'required',
				'rate_id'	 	 => 'required',
				'number_of_days' => 'required',
			]);

			$transaction->employee_id 			=  $request->employee_id;
	    	$transaction->travel_rate_id 		=  $request->travel_rate_id;
            $transaction->employee_number       =  $request->employee_number;
	    	$transaction->rate_id 				=  $request->rate_id;
	    	$transaction->number_of_days 		=  $request->number_of_days;
	    	$transaction->travel_rate_amount 	=  $travel_rate_amount;
	    	$transaction->net_amount 			=  $net_amount;
	    	$transaction->created_by 			=  Auth::User()->id;

	    	$transaction->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);

		}
        return $response;
    }

    public function showTravelAllowanceDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getTravelAllowance(){
    	$data = Input::all();

    	$transaction =  new TravelRateTransaction;

    	$query = $transaction
    			->with('employees','rates','travelrates')
		    	->where('employee_id',@$data['employee_id'])
                ->orderBy('created_at','desc')
                ->get();

    	return json_encode($query);
    }

    public function deleteTravelAllowance(){
        $data = Input::all();

        $transaction = new TravelRateTransaction;

        $transaction
        ->where('employee_id',$data['empid'])
        ->where('month',$data['month'])
        ->where('year',$data['year'])
        ->delete();

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
