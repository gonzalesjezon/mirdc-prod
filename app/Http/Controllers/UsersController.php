<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;
use App\EmployeeInformation;
use Input;
class UsersController extends Controller
{
    function __construct(){
    	$this->controller = $this;
    	$this->title = 'USER MANAGEMENT';
    	$this->module_prefix = 'payrolls/admin';
    	$this->module = 'users';
    }

    public function index(){

    	$employee            = new Employee;
        $employeeinformation = new EmployeeInformation;

        $employeeinfo = $employeeinformation
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions','departments');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();


        $response = array(
            'employee'      => $employee,
    		'controller'	=> $this->controller,
    		'title'			=> $this->title,
    		'module'		=> $this->module,
    		'module_prefix'	=> $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $users = new User;

        if(isset($request->id)){
            $users = $users->find($request->id);

            $users->name            = $request->name;
            $users->username        = $request->username;
            $users->password        = bcrypt($request->password);
            $users->access_type_id  = $request->access_type_id;
            $users->employee_id     = $request->employee_id;

            $users->save();

            $response = json_encode(['status'=>true,'response'=>'Update Successfully']);

        }else{

            $this->validate($request,[
                'employee_id' => 'required',
                'username' => 'required|string|max:255|unique:users',
                'password' => 'required|string|min:6',

            ]);
            $users->name            = $request->name;
            $users->username        = $request->username;
            $users->password        = bcrypt($request->password);
            $users->access_type_id  = $request->access_type_id;
            $users->employee_id     = $request->employee_id;

            $users->save();

            $response = json_encode(['status'=>true,'response'=>'Save Successfully']);
        }
        return $response;
    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['name'];


        $query = User::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function deleteUser(){

        $data = Input::all();

        $id = $data['id'];

        $users = new User;

        $users->destroy($id);

        return json_encode(['status'=>true]);

    }

}
