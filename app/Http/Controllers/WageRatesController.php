<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\WageRate;

class WageRatesController extends Controller
{
    
    function __construct(){
        $this->controller = $this;
        $this->title = 'MINIMUM WAGE RATES';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'wagerates';
        $this->table = 'wage_rates';
    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'wage_region'         => 'required',
            'wage_rate'           => 'required',
            'effectivity_date'    => 'required',
        ]);

        $wagerate = new WageRate;

        if(isset($request->for_update)){

            $wagerate = WageRate::find((int)$request->for_update);

            $wagerate->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $check = $wagerate->where('wage_rate',$request->wage_rate)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Wage rate already taken!']);

            }else{

                $wagerate->fill($request->all())->save();

                $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);

            } 
        }

        return $response;
    }

    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; } 

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['wage_region','wage_rate'];


        $query = WageRate::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = WageRate::where('id',$id)->first();

        return json_encode($query);
    }
}
