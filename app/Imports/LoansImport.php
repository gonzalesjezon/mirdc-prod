<?php

namespace App\Imports;

use App\LoanInfo;
use Maatwebsite\Excel\Concerns\ToModel;

class LoansImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new LoanInfo([
            'employee_id'           => $row[0],
            'loan_id'               => $row[1],
            'loan_totalamount'      => $row[2],
            'loan_totalbalance'     => $row[3],
            'loan_amortization'     => $row[4],
            'year'                  => $row[5],
            'month'                 => $row[6],
        ]);
    }
}
