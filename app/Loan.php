<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = "pms_loans";
    protected $fillable = [
    	'code',
    	'name',
    	'loan_type',
    	'category',
    	'gsis_excel_col',
    	'remarks'
    ];
}
