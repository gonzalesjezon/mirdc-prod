<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LongevityPay extends Model
{
    protected $table = 'pms_longevitypay';
    protected $fillable = [
    	'employee_id',
        'employee_number',
    	'basic_pay_amount',
    	'longevity_amount',
    	'lp',
    	'longevity_date',
    	'created_by',
    	'updated_by'
    ];

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function salaryinfo(){
        return $this->hasOne('App\SalaryInfo','employee_id');
    }
}
