<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherPayroll extends Model
{
    protected $table = 'pms_other_payrolls';
    protected $fillable = [
		'employee_id',
		'employee_number',
		'process_date',
		'basic_salary_amount',
		'pera_amount',
		'gross_salary_amount',
		'gsis_premium_amount',
		'philhealth_amount',
		'hdmf_amount',
		'withholding_amount',
		'total_deductions_amount',
		'gross_taxable_amount',
		'hdmf_mpl_amount',
		'hdmf_calamity_loan',
		'gsis_educ_loan',
		'gsis_emerg_loan',
		'gsis_policy_loan',
		'gsis_conso_loan',
		'lwop',
		'year',
		'month',
		'net_amount',
		'status',
		'created_by',
		'updated_by',
    ];


    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
}
