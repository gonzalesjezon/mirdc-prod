<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimePay extends Model
{
    protected $table = 'pms_overtime';
    protected $fillable = [
		'employee_id',
		'employee_number',
		'previous_balance',
		'used_amount',
		'available_balance',
		'actual_regular_overtime',
		'adjust_regular_overtime',
		'total_regular_amount',
		'actual_special_overtime',
		'adjust_special_overtime',
		'total_special_amount',
		'actual_regular_holiday_overtime',
		'adjust_regular_holiday_overtime',
		'total_regular_holiday_amount',
		'total_overtime_amount',
		'year',
		'month',
		'pay_period',
		'sub_pay_period',
		'created_by',
		'updated_by'
    ];
}
