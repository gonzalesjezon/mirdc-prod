<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryInfo extends Model
{
    protected $table = 'pms_salaryinfo';
    protected $fillable = [
    	'employee_id',
        'employee_number',
    	'salarygrade_id',
        'employee_status_id',
        'jobgrade_id',
        'positionitem_id',
        'position_id',
        'step_inc',
    	'basic_amount_one',
    	'basic_amount_two',
    	'salary_new_rate',
    	'salary_effectivity_date',
        'created_by',
        'updated_by'
    ];

    public function salarygrade(){
    	return $this->belongsTo('App\SalaryGrade','salarygrade_id');
    }

     public function jobgrade(){
        return $this->belongsTo('App\JobGrade','jobgrade_id');
    }

    public function positionitems(){
        return $this->belongsTo('App\PositionItem','positionitem_id');
    }

    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function transactions(){
        return $this->belongsTo('App\Transaction','employee_id');
    }
}
