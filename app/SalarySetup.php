<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalarySetup extends Model
{

    protected $table = 'salaries_setup';
    protected $fillable = [

    	'salary_grade_id',
    	'step_no',
    	'amount',
    	'effectivity_date'

    ];
}
