<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialAnnualTax extends Model
{
    protected $table = 'pms_special_annual_tax';
    protected $fillable = [
    	'employee_id',
        'employee_number',
    	'clothing_allowance_amount',
    	'cash_gift_amount',
    	'loyalty_amount',
    	'performance_base_amount',
    	'collective_negotiation_amount',
    	'pei_amount',
    	'mid_year_amount',
    	'year_end_amount',
        'honoraria_amount',
    	'for_year',
    	'created_by',
    	'updated_by'
    ];
}
