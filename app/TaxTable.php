<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxTable extends Model
{
    protected $table = 'pms_taxtable';
    protected $fillable = [
        'payperiod',
    	'salary_bracket_level1',
    	'salary_bracket_level2',
    	'salary_bracket_level3',
    	'salary_bracket_level4',
    	'salary_bracket_level5',
    	'salary_bracket_level6',
        'status',
        'effectivity_date',
    	'remarks'
    ];
}
