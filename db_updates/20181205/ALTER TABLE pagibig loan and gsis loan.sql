ALTER TABLE `pms_transactions`
	ADD COLUMN `pagibig_loan` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gsis_er_share`,
	ADD COLUMN `gsis_loan` DECIMAL(9,2) NULL DEFAULT NULL AFTER `pagibig_loan`;