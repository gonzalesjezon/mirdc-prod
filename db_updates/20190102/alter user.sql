ALTER TABLE `users`
	ADD COLUMN `employee_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `access_type_id`;

ALTER TABLE `pms_annual_tax_rates`
	ADD COLUMN `lwop_amount` DECIMAL(10,0) NULL DEFAULT NULL AFTER `longevity_amount`;
ALTER TABLE `pms_annual_tax_rates`
	ADD COLUMN `overtime_amount` DECIMAL(10,0) NULL DEFAULT NULL AFTER `lwop_amount`;