-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mirdc_db.pms_benefits
DROP TABLE IF EXISTS `pms_benefits`;
CREATE TABLE IF NOT EXISTS `pms_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `de_minimis_type` varchar(225) DEFAULT NULL,
  `computation_type` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table mirdc_db.pms_benefits: ~11 rows (approximately)
/*!40000 ALTER TABLE `pms_benefits` DISABLE KEYS */;
INSERT INTO `pms_benefits` (`id`, `code`, `name`, `amount`, `de_minimis_type`, `computation_type`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(4, 'PERA', 'PERA', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:00:00', '2018-06-21 04:00:00', NULL),
	(5, 'RATA1', 'RATA 1', 0.00, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:01:53', '2018-09-06 04:57:11', NULL),
	(6, 'RATA2', 'RATA 2', 0.00, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:02:43', '2018-09-06 04:57:26', NULL),
	(11, 'EE', 'EXTRAORDINARY EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:04', '2018-07-13 08:20:04', NULL),
	(12, 'ME', 'MISCELLANEOUS EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:29', '2018-07-13 08:20:29', NULL),
	(18, 'PBB', 'Performance Base Bonus', 5000.00, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:14:40', '2018-08-01 15:16:42', NULL),
	(19, 'SA', 'Subsistent Allowance', 3300.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, NULL, '2018-08-24 15:43:09', '2019-01-16 09:05:18', NULL),
	(20, 'HP', 'HAZARD PAY', 0.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, NULL, '2018-08-25 06:12:36', '2018-08-25 06:12:36', NULL),
	(21, 'RA', 'RICE ALLOWANCE', 1250.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, NULL, '2018-09-08 14:26:16', '2018-09-08 14:26:16', NULL),
	(22, 'UA', 'UNIFORM ALLOWANCE', 6000.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, NULL, '2018-09-08 14:26:54', '2018-09-08 14:26:54', NULL),
	(23, 'AB', 'ANNIVERSARY BONUS', 3000.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, NULL, '2018-09-08 14:27:59', '2018-09-08 14:27:59', NULL),
	(24, 'LA', 'Laundry Allowance', 500.00, NULL, 'Based on Attendance', NULL, 'Taxable', NULL, NULL, NULL, '2019-01-16 09:05:44', '2019-01-16 09:05:44', NULL);
/*!40000 ALTER TABLE `pms_benefits` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
