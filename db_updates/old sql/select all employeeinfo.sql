select empinfo.employee_number, employee.lastname, employee.firstname, employee.middlename, position.name as 'Position', empstatus.name as 'Employee Status', division.name as 'Division', office.name as 'Office' from pms_employee_information as empinfo
LEFT JOIN pms_positions as position ON position.id = empinfo.position_id
LEFT JOIN pms_employee_status as empstatus ON empstatus.id = empinfo.employee_status_id
LEFT JOIN pms_employees as employee ON employee.id = empinfo.employee_id
LEFT JOIN pms_divisions as division ON division.id = empinfo.division_id
LEFT JOIN pms_offices as office ON office.id = empinfo.office_id