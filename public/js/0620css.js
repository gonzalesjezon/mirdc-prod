var bgcolor       = getCookie("bgcolor");
var fontcolor     = getCookie("fontcolor");
var bordercolor   = getCookie("bordercolor");
var divcolor      = getCookie("divcolor");
var titlebarcolor = getCookie("titlebarcolor");
var font          = getCookie("font");
var hlightcolor   = getCookie("hlightcolor");
var pxpt = "px";

var titlebarcolor_1 = "#2E5C8A";
var titlebarcolor_0 = "#1A334C";
var disabledColor   = "#CDCDCD";
var MainDivcolor_1  = "#A6C3E1";
var MainDivcolor_0  = "#BDD3E9";

var objFocusColor   = "#66CCFF";
var hlightcolor     = "#C2D1E0";
fontcolor = "#002447";

var sysWidth = 100;
var sysHeight = 0;
var popScrnHeight = screen.height - 200;
var popScrnWidth = screen.width - 300;
if (popScrnHeight < 500) popScrnHeight = screen.height;
if (popScrnWidth < 1000) popScrnWidth = screen.width;
var POPleft = (screen.width/2) - (popScrnWidth/2);
var POPtop = (screen.height/2) - (popScrnHeight/2);

var css = "";
css += "<style>";
css += "table        {border-collapse:collapse;}";
css += "th           {padding:2px;text-align:center;}";
css += ".recDtlHed 	{font-family:" + font + "; font-size:11px;font-weight:600;color:" + fontcolor + ";}";
css += ".recDetail 	{font-family:" + font + "; font-size:11px;font-weight:400;color:" + fontcolor + ";}";
css += ".defNFont8   {font-family:" + font + "; font-size:8"+pxpt+";  color:" + fontcolor + "; font-weight:400}";
css += ".defBFont8   {font-family:" + font + "; font-size:8"+pxpt+";  color:" + fontcolor + "; font-weight:600}";
css += ".defNFont9   {font-family:" + font + "; font-size:9"+pxpt+";  color:" + fontcolor + "; font-weight:400}";
css += ".defBFont9   {font-family:" + font + "; font-size:9"+pxpt+";  color:" + fontcolor + "; font-weight:600}";
css += ".defNFont    {font-family:" + font + "; font-size:10"+pxpt+"; color:" + fontcolor + "; font-weight:400}";
css += ".defBFont    {font-family:" + font + "; font-size:10"+pxpt+"; color:" + fontcolor + "; font-weight:600}";
css += ".defNFont12  {font-family:" + font + "; font-size:12"+pxpt+"; color:" + fontcolor + "; font-weight:400}";
css += ".defBFont12  {font-family:" + font + "; font-size:12"+pxpt+"; color:" + fontcolor + "; font-weight:600}";
css += ".defNFont14  {font-family:" + font + "; font-size:14"+pxpt+"; color:" + fontcolor + "; font-weight:400}";
css += ".defBFont14  {font-family:" + font + "; font-size:14"+pxpt+"; color:" + fontcolor + "; font-weight:600}";

css += ".font_Footer {background-color:transparent;font-family:" + font + "; font-size:8"+pxpt+"; color:" + fontcolor + "; font-weight:600}";
css += ".MainTable {width:100%}";
css += ".MainDiv   {background-color:transparent;}";



var titlebarcolor_0 = "#478FB2";
var titlebarcolor_1 = "#52A3CC";
css += ".clsBTN_01:hover:enabled {background: "+titlebarcolor_1+";";
css += "background: -moz-linear-gradient(top, "+titlebarcolor_0+" 0%, "+titlebarcolor_1+" 100%);";
css += "background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, "+titlebarcolor_0+"), color-stop(100%, "+titlebarcolor_1+"));";
css += "background: -webkit-linear-gradient(top, "+titlebarcolor_0+" 0%, "+titlebarcolor_1+" 100%);";
css += "background: -o-linear-gradient(top, "+titlebarcolor_0+" 0%, "+titlebarcolor_1+" 100%);";
css += "background: -ms-linear-gradient(top, "+titlebarcolor_0+" 0%, "+titlebarcolor_1+" 100%);";
css += "background: linear-gradient(to bottom, "+titlebarcolor_0+" 0%, "+titlebarcolor_1+" 100%);";
css += "cursor:pointer;}";

var btnColor0 = "#2E5C8A";

css += ".clsBlack  {background-color:#111;z-index:301;position:absolute;top:0;left:0;width:100%;height:100%;opacity:0.3;display:none;}";
css += ".clsPopOut {background: "+MainDivcolor_1+";";
css += "z-index:301;position:absolute;top:0;left:"+POPleft+"px;width:"+popScrnWidth+"px;height:"+popScrnHeight+"px;display:none;overflow:auto;padding:5px;";
css += "}";

titlebarcolor_1 = "#001A33";
css += ".clsTable          {align:center;margin-top:5px;}";   
css += ".dvObjectEntry     {margin-top:5px;padding:5px;background:#eee;border:1px solid #999}";
css += ".clsDataEntry      {border-top:3px solid "+titlebarcolor+";padding-top:5px;padding-left:5px;}";    

css += ".clsTXTVIEW        {background-color:"+titlebarcolor_1+";width:150px;margin-right:5px;font-size:14"+pxpt+";font-weight:600;color:white        ;border:1px solid transparent;height:25px;padding-left:5px;}";
css += ".clsLABEL          {background-color:transparent        ;width:150px;margin-right:5px;font-size:11"+pxpt+";font-weight:600;color:"+fontcolor+";border:1px solid transparent  ;height:15px;padding-left:5px;}";
css += ".clsTXTVALUE                {background-color:white            ;width:150px;margin-right:5px;font-size:11"+pxpt+";font-weight:600;color:"+fontcolor+";border:1px solid "+fontcolor+";height:20px;padding-left:5px;}";
css += ".clsTXTVALUE:-moz-read-only {background-color:"+disabledColor+";width:150px;margin-right:5px;font-size:11"+pxpt+";font-weight:600;color:"+fontcolor+";border:1px solid "+fontcolor+";height:20px;padding-left:5px;}";
css += ".clsTXTVALUE:focus {background-color:"+objFocusColor+  ";width:150px;margin-right:5px;font-size:11"+pxpt+";font-weight:600;color:black        ;border:1px solid "+fontcolor+";height:20px;padding-left:5px;}";

css += ".clsDRPVALUE          {background-color:white            ;margin-right:5px;font-size:11"+pxpt+"; font-weight:600;color:"+fontcolor+";height:20px;border:1px solid "+fontcolor+"}";
css += ".clsDRPVALUE:disabled {background-color:"+disabledColor+";margin-right:5px;font-size:11"+pxpt+"; font-weight:600;color:"+fontcolor+";height:20px;border:1px solid "+fontcolor+"}";
css += ".clsDRPVALUE:focus {background-color:"+objFocusColor+";margin-right:5px;font-size:11"+pxpt+"; font-weight:600;color:black;        height:20px;border:1px solid "+fontcolor+"}";
css += ".clsTXTVALUEBIG    {background-color:white;width:200px; margin-right:5px;  border:2px solid " + bordercolor + ";font-size:13"+pxpt+"; font-weight:600;color:"+fontcolor+";box-shadow: 2px 2px 2px #003366;text-align:center;padding:5px;}";
css += ".TRLIST_active     {font-weight:600;background-color:#002952;color:white;font-size:11"+pxpt+";}";
css += ".TRLIST            {font-weight:600;background-color:"+bgcolor    +";color:"+fontcolor+";font-size:11"+pxpt+";}";
css += ".TRLIST:hover      {font-weight:600;background-color:"+hlightcolor+";color:"+fontcolor+";font-size:11"+pxpt+";cursor:pointer;}";

css += ".clsNoRecord       {background:#ccc;color:black;font-weight:600}";
css += ".clsNoRecord:hover {background:#aaa;color:black;font-weight:600}";

css += ".collapseornot:hover {cursor:pointer;}";
css += ".errMSG {font-family:Arial;font-size:12px;font-weight:600;border:1px solid red;color:red;background-color:#FFFFCC;padding:3px;}";
css += "</style>";
document.writeln(css);




         

