$(document).ready(function() {
   var SysUserRefId = 0;
   $("[name='EmpAccessed']").dblclick(function() {
      var idx = $(this).val();
      $("[name='EmpAccessed'] option[value='"+idx+"']").remove();
      $("[name='EmpUnaccessed']").append('<option value="'+idx+'">Item '+idx+'</option>');
   });
   $("[name='EmpUnaccessed']").dblclick(function() {
      var idx = $(this).val();
      $("[name='EmpUnaccessed'] option[value='"+idx+"']").remove();
      $("[name='EmpAccessed']").append('<option value="'+idx+'">Item '+idx+'</option>');
   });
   $("input[class*='saveFields--']").click(function() {
   });
   /*
   $("#btnADD").click(function (){
      $("#divList").hide();
      $("#divView").show();
   });
   */
   $(".clsUPDATE").click(function (){
      var Object_Value = "";
      if (confirm("Proceed ?")) {
         $("input[class*='saveFields--']").each(function(){
            if ($(this).is(":checked")) {
               Object_Value += "hidden|sint_SysUserRefId|" + $("#hSysUserRefId").val() + "!";
               Object_Value += "hidden|sint_EmployeesRefId|" + $(this).val() + "!»";
            }
         });
         $.post("SystemAjax.e2e.php",
         {
            task:"ajxSaveRecordMulti",
            tbl:"AccessEmployees",
            fnv:Object_Value,
            hUser:$("[name='hUser']").val(),
            multi:true
         },
         function(data,status) {
            code = data;
            try {
               eval(code);
            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         });
      }
   });
});

function loadUpdatedEmpAccesList(sysuserrefid) {
   $.post("ctrl_AccessEmp.e2e.php",
   {
      task:"loadAccessEmp",
      sysuserrefid:sysuserrefid
   },
   function(data,status) {
      try {
         if (status=="success") {
            $("#spEmployeesAccess").html("");
            $("#spEmployeesAccess").html(data);
         } else {
            alert(status);
         }
      } catch (e) {
          if (e instanceof SyntaxError) {
              alert(e.message);
          }
      }
   });

}

function selectSysUser(username,sysuserrefid) {
   $(".list-group-item").removeClass("active");
   $("#" + username).addClass("active");
   $("#idSysUserName").html("User Profile Selected : [" + sysuserrefid + "] " + username);
   loadUpdatedEmpAccesList(sysuserrefid);

}

function deleteRecord(refid) {
   if (refid) {
      if (confirm("Are you sure you want to delete this record")) {
         gDeleteRecord("AccessEmployees",refid);
      }
   } else {
      alert("No Ref. Id Assigned");
      return false;
   }
}

function afterDelete() {
   alert("Record Succesfully Deleted!!!");
   loadUpdatedEmpAccesList($("#hSysUserRefId").val());
}

function afterNewSave(refid) {
   if (refid) {
      alert("Record Inserted !!! " + refid);
      loadUpdatedEmpAccesList($("#hSysUserRefId").val());
   } else {
      alert("chk: " + refid);
   }
}