// JQUERY
$(document).ready(function() {
   $("#btnNewMode, #btnViewMode").hide();
   if ($("#uplClose").val() == "Y" && $("#hUserGroup").val() == "COMPEMP") {
      $("[class*='form-input']").attr("disabled",true);
      $("#btnNewMode, #btnEmpLkUp").hide();
      $("#btnViewMode, #Pic, #btnCHANGEPW").show();
      loadRecord($("#hEmpRefId").val(),
                 $("#hUserRefID").val());
   } else {
      switch ($("#hmode").val()) {
         case "VIEW":
            $("#ScreenTitle").html("USER's PROFILE");
            loadRecord($("#hEmpRefId").val(),
                       $("#hUserRefID").val());
            $(".saveFields--").attr("disabled",true);
            $("#btnViewMode").show();
            
         break;
         case "ADD":
            $("[class*='saveFields--'], #Password").attr("disabled",false);

            $("#btnNewMode").show();
         break;
      }
   }
   
   $("#btnEDIT").click(function () {
      var user_refid = $("[name='hUserRefID']").val();
      $("[class*='form-input']").attr("disabled",false);
      $("#userName").attr("disabled",true);
      $("[type='password']").attr("disabled",true);
      $("[name='hmode']").val("EDIT");
      $("[name='hRefId']").val(user_refid);
      $("#btnNewMode").show();
      $("#btnViewMode, #btnCHANGEPW").hide();

   });
   $("#btnCANCEL").click(function () {
      $("[class*='form-input']").attr("disabled",true);
      $("#btnNewMode, #btnEmpLkUp").hide();
      $("#btnViewMode, #Pic, #btnCHANGEPW").show();
      loadRecord($("#hEmpRefId").val(),
                 $("#hUserRefID").val());
   });

   $("#userName").blur(function (){
      var uname = $(this).val();
      $.get("fnCaller.e2e.php",
      {
         fn:"Avail",
         crit:"UserName = '" + uname + "'",
         table:"sysuser",
         hCompanyID:$("#hCompanyID").val(),
         hBranchID:$("#hBranchID").val(),
         hEmpRefId:$("#hEmpRefId").val(),
         hUserRefId:$("#hUserRefId").val()
      },
      function(data,status) {
         if (status=="success") {
            try {
               if (data == "true") {
                  alert("Oooops !!! username " + uname + " is already exist!!!");
                  $("#userName").focus();
                  $("#btnSAVEu").prop("disabled",true);
               } else {
                  $("#btnSAVEu").prop("disabled",false);
               }
            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         }
      });
   });

   $("#btnSAVEu").click(function () {
      var empRefid = parseInt($("#empRefId").html());
      alert(empRefid);
      if ($("#empRefId").html() == "&nbsp;") {
         alert("Oooops Error!! Employee is Required");
         $("#empRefId").focus();
         return false;
      }
      if ($("#userName").val() == "") {
         alert("Oooops Error!! User Name is Required");
         $("#firstName").focus();
         return false;
      }
      if ($("#char_SysGroupRefId").val() == "") {
         alert("Oooops Error!! System Group is Required");
         $("#char_SysGroup").focus();
         return false;
      }
      if ($("[name='hmode']").val() == "ADD") {
         t1 = calcHash($("#Password").val());
         t2 = calcHash($("#Password2").val());
         if (t1 != t2) {
            alert("Oooops Error!! Password is Mismatched");
            $("#firstName").focus();
            return false;
         } else {
            if ($("#Password2").val().length < 8) {
               alert("Oooops Error!! Password should be atleast 8 Alpha Numeric Character");
               $("#Password").focus();
               return false;
            }
         }
         $("#Password").val("");
         $("#Password2").val("");
         $("[name='hNewToken']").val(t1);
         $("[name='char_hNewReToken']").val(t2);
      }
      if ($("#hTable").val() == "") {
         alert("No Table Assigned");
         return false;
      }

      $.get("fnCaller.e2e.php",
      {
         fn:"Avail",
         crit:"UserName = '" + $("#userName").val() + "' OR EmployeesRefId = " + empRefid,
         table:"sysuser",
         hCompanyID:$("#hCompanyID").val(),
         hBranchID:$("#hBranchID").val(),
         hEmpRefId:$("#hEmpRefId").val(),
         hUserRefId:$("#hUserRefId").val()
      },
      function(data,status) {
         if (status == "success" &&
             data == "true")
         {
            try {
               alert("Oooops !!! Username or Employees is already exist!!!");
               $("#userName").focus();
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         } else {
            var objValue = getFieldEntry("EntryScrn","ADD");
            gSaveRecord(objValue,$("#hTable").val());
         }
      });
   });

   $("#btnCHANGEPW").click(function () {
      $("#txtnPW, #txtPW").prop('disabled',false);
      $("#changePW").modal();
   });
   $("#btnEXIT").click(function () {
      gotoscrn('scrnDashboard','&paramTitle=Dashboard');
   });
   $("#btnUPLOADPIC").click(function () {
      gotoscrn('scrnUpload','&paramTitle=Uploads&hCaller=scrnUsers');
   });

   

});

function afterEditSave(refid) {
   alert("Record Updated");
   refreshSCRN(refid);
}
function afterNewSave(newRefId) {
   refreshSCRN(newRefId);
   alert("New Record Inserted\nRef. ID: " + newRefId);
   return false;
}
function afterDelete() {
   alert("Record Deleted");
   return false;
}
function refreshSCRN(userrefid) {
   $("[class*='saveFields--'], #Password, #Password2").prop("disabled",true);
   $("#btnNewMode, #btnEmpLkUp").hide();
   $("#btnViewMode, #Pic").show();
   $("[name='hmode']").val("VIEW");
   loadRecord($("#hEmpRefId").val(),userrefid);
   return false;
}

function changeNow() {
   var valCurrentToken = $("#txtPW").val();
   var valNewToken = $("#txtnPW").val();
   var nt = calcHash(valNewToken);
   var ct = calcHash(valCurrentToken);
   $("#txtPW").val("");
   $("#txtnPW").val("");
   if (valCurrentToken !== "" && valNewToken !== "")
   {
      $.post("ChangePW.e2e.php",
      {
         u:$("#hUser").val(),
         nt:nt,
         ct:ct,
         hCompanyID:$("#hCompanyID").val(),
         hBranchID:$("#hBranchID").val(),
         hEmpRefId:$("#hEmpRefId").val(),
         hUserRefId:$("#hUserRefId").val()
      },
      function(data,status) {
         try {
            $("#changePW").modal("hide");
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      });

   } else {
      alert("Incomplete Entry!!!");
   }
}

function loadRecord(emprefid,userrefid) {

   $('#Password, #Password2').prop('disabled',true);
   $.get("fnCaller.e2e.php",
   {
      fn:"ViewRecord",
      refid:emprefid,
      table:"employees",
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      var data = JSON.parse(data);
      try {
         $('#empRefId').html(data.RefId + "&nbsp;");
         $('#lastName').html(data.LastName + "&nbsp;");
         $('#firstName').html(data.FirstName + "&nbsp;");
         $('#middleName').html(data.MiddleName + "&nbsp;");
         $('#extName').html(data.ExtName + "&nbsp;");
         $('#nickName').html(data.NickName + "&nbsp;");
         $('#contactNo').html(data.ContactNo + "&nbsp;");
         $('#emailAdd').html(data.EmailAdd + "&nbsp;");
         if (data.BirthDate == '1900-01-01')
            $('#birthDate').html("");
         else
            $('#birthDate').html(data.BirthDate + "&nbsp;");
         $('#Gender').html(data.Gender);

         $.get("fnCaller.e2e.php",
         {
            fn:"ViewRecord",
            refid:userrefid,
            table:"sysuser"
         },
         function(data,status) {
            var data = JSON.parse(data);
            try {
               $('#userName').val(data.UserName);
               $('#Password').val('12345678');
               $('#Password2').val('12345678');
               $("[name='sint_EmployeesRefId']").val(data.RefId);
               $('[name="sint_SysGroupRefId"]').val(data.SysGroupRefId);
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         });
      } catch (e) {
          if (e instanceof SyntaxError) {
              alert(e.message);
          }
      }
   });
}

function fillUp(data){
   try {
      $('#empRefId').html(data.RefId + "&nbsp;");
      $('#lastName').html(data.LastName + "&nbsp;");
      $('#firstName').html(data.FirstName + "&nbsp;");
      $('#middleName').html(data.MiddleName + "&nbsp;");
      $('#extName').html(data.ExtName + "&nbsp;");
      $('#nickName').html(data.NickName + "&nbsp;");
      $('#contactNo').html(data.ContactNo + "&nbsp;");
      $('#emailAdd').html(data.EmailAdd + "&nbsp;");
      if (data.BirthDate == '1900-01-01')
         $('#birthDate').html("");
      else
         $('#birthDate').html(data.BirthDate + "&nbsp;");
      $('#Gender').html(data.Gender);
      $('#userName').val("");
      $('#Password').val("");
      $('#Password2').val("");
      $('#SysGroup').val(0);
      $("[name='sint_EmployeesRefId']").val(data.RefId);
   }
   catch (e)
   {
      if (e instanceof SyntaxError) {
          alert(e.message);
      }
   }
}




