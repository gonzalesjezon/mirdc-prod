@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 text-center">
            <img src="{{ url('images/mirdc_logo.gif') }}" style="position: relative;left: -8px; margin-top: 35px; width: 400px;">
            <h3>Metals Industry Research and Development Center</h3>
        </div>
        <div class="col-md-5" style="margin-top: 150px;">

           <!--  <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body"> -->
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="text" type="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="checkbox text-right">
                                    <!-- <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label> -->
                                </div>
                            </div>
                            <div class="col-md-4 text-left" style="padding-left: 0px;">
                                <button type="submit" class="btn btn-primary">
                                    Sign In
                                </button>
                            </div>
                        </div>
<!--
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-8">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div> -->
                    </form>
               <!--  </div>
            </div>
        </div> -->
    </div>
</div>
@endsection
