
<?php
(strtolower($module) == 'gsisremittances') ? $gsisremittances = 'active' : $gsisremittances = '';
(strtolower($module) == 'ecip') ? $ecip = 'active' : $ecip = '';
(strtolower($module) == 'pagibigremittances') ? $pagibigremittances = 'active' : $pagibigremittances = '';
(strtolower($module) == 'philhealth') ? $philhealth = 'active' : $philhealth = '';
?>
<div class="side-menu">
    <div class="row">
        <div class="col-sm-8">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li class="{{ $gsisremittances }}">
                    	<a href="{{ url('payrolls/reports/remittances/gsisremittances') }}"  >
						GSIS</a>
                    </li>
                     <li class="{{ $pagibigremittances }}">
                    	<a href="{{ url('payrolls/reports/remittances/pagibigremittances') }}"  >
						PAGIBIG</a>
                    </li>
                    <li class="{{ $philhealth }}">
                    	<a href="{{ url('payrolls/reports/remittances/philhealth') }}"  >
						PHILHEALTH</a>
                    </li>
                     <li class="{{ $ecip }}">
                        <a href="{{ url('payrolls/reports/remittances/ecip') }}"  >
                        OTHER REMITTANCES</a>
                    </li>
                    <!-- <li class="nav-divider"></li> -->

                </ul>
            </nav>
        </div>
<!--         <div class="col-sm-2 col-sm-offset-8">

        </div> -->
    </div>

</div>




