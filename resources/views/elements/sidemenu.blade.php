<head>
    <style type="text/css">
    .wrapper {
        display: flex;
        width: 100%;
    }

    #sidebar {
        font-size: 13px;
        width: 200px;
        position: fixed;
        top: 0;
        left: 0;
        height: 100vh;
        z-index: 999;
        background: #7386D5;
        color: #fff;
        transition: all 0.3s;
    }
    a[data-toggle="collapse"] {
        position: relative;
    }

    .dropdown-toggle::after {
        display: block;
        position: absolute;
        top: 50%;
        right: 20px;
        transform: translateY(-50%);
    }
    @media (max-width: 768px) {
        #sidebar {
            margin-left: -250px;
        }
        #sidebar.active {
            margin-left: 0;
        }
    }
    @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";


    body {
        font-family: 'Poppins', sans-serif;
        background: #fafafa;
    }

    p {
        font-family: 'Poppins', sans-serif;
        font-size: 1.1em;
        font-weight: 300;
        line-height: 1.7em;
        color: #999;
    }

    a, a:hover, a:focus {
        color: inherit;
        text-decoration: none;
        transition: all 0.3s;
    }

    #sidebar {
        /* don't forget to add all the previously mentioned styles here too */
        background: #1c1d22;
        color: #fff;
        transition: all 0.3s;
    }

    #sidebar .sidebar-header {
        padding: 5px;
        background: #154b87;
    }

    #sidebar ul.components {
        padding: 20px 0;
        /*border-bottom: 1px solid #47748b;*/
    }

    #sidebar ul p {
        color: #fff;
        padding: 10px;
    }

    #sidebar ul li a {
        padding: 10px;
        font-size: 1.1em;
        display: block;
    }
    #sidebar ul li a:hover {
        color: #fff;
        background: #23262df0;
    }

    #sidebar ul li.active > a, a[aria-expanded="true"] {
        color: #fff;
        background: #164b88;
    }
    ul ul a {
        font-size: 0.9em !important;
        padding-left: 30px !important;
        background: #1c1d22;
    }
    ul ul ul a {
    font-size: 0.9em !important;
    padding-left: 50px !important;
    background: #1c1d22;
    }
    /* width */
    ::-webkit-scrollbar {
        width: 10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #555;
    }
</style>
</head>



<?php

    (strtolower($module) == 'transactions') ? $plantilla = 'active' : $plantilla = '';
    (strtolower($module) == 'nonplantilla') ? $nonplantilla = 'active' : $nonplantilla = '';

    (strtolower($module) == 'reports') ? $reports = 'active' : $reports = '';
    (strtolower($module) == 'overtimepay') ? $overtimepay = 'active' : $overtimepay = '';
    (strtolower($module) == 'beginningbalances') ? $beginningbalances = 'active' : $beginningbalances = '';
    (strtolower($module) == 'payrollconfigurations') ? $payrollconfigurations = 'active' : $payrollconfigurations = '';
    (strtolower($module) == 'previousemployer') ? $previousemployer = 'active' : $previousemployer = '';

    (strtolower($module) == 'employeestatus' ||
    strtolower($module) == 'divisions' ||
    strtolower($module) == 'departments' ||
    strtolower($module) == 'offices' ||
    strtolower($module) == 'position_items' ||
    strtolower($module) == 'positions' ||
    strtolower($module) == 'benefits' ||
    strtolower($module) == 'adjustments' ||
    strtolower($module) == 'deductions' ||
    strtolower($module) == 'loans' ||
    strtolower($module) == 'banks' ||
    strtolower($module) == 'philhealths' ||
    strtolower($module) == 'pagibig' ||
    strtolower($module) == 'gsis' ||
    strtolower($module) == 'providentfundpolices' ||
    strtolower($module) == 'taxes' ||
    strtolower($module) == 'wagerates' ||
    strtolower($module) == 'jobgrades')
    ? $filemanagers = 'active' : $filemanagers = '';
    (strtolower($module) == 'employees_payroll_informations') ? $employees_payroll_informations = 'active' : $employees_payroll_informations = '';
    (strtolower($module) == 'employeesetup') ? $employeesetup = 'active' : $employeesetup = '';
    (strtolower($module) == 'annualtaxsetup') ? $annualtaxsetup = 'active' : $annualtaxsetup = '';
    (strtolower($module) == 'users') ? $users = 'active' : $users = '';



     (strtolower($module) == 'beginningbalances' ||
     strtolower($module) == 'employeestatus' ||
     strtolower($module) == 'divisions' ||
     strtolower($module) == 'departments' ||
     strtolower($module) == 'offices' ||
     strtolower($module) == 'position_items' ||
     strtolower($module) == 'positions' ||
     strtolower($module) == 'benefits' ||
     strtolower($module) == 'adjustments' ||
     strtolower($module) == 'deductions' ||
     strtolower($module) == 'loans' ||
     strtolower($module) == 'banks' ||
     strtolower($module) == 'philhealths' ||
     strtolower($module) == 'pagibig' ||
     strtolower($module) == 'gsis' ||
     strtolower($module) == 'providentfundpolices' ||
     strtolower($module) == 'taxes' ||
     strtolower($module) == 'wagerates' ||
     strtolower($module) == 'jobgrades' ||
     strtolower($module) == 'employees_payroll_informations' ||
     strtolower($module) == 'previousemployer' ||
     strtolower($module) == 'payrollconfigurations' ||
     strtolower($module) == 'longevitysetup' ||
     strtolower($module) == 'loanshistory' ||
     strtolower($module) == 'annualtaxsetup' ||
     strtolower($module) == 'importmodule' ||
     strtolower($module) == 'employeesetup') ?
     $admin = 'collapse list-unstyled in' : $admin = 'collapse list-unstyled';


    (strtolower($module) == 'otherpayrolls' ||
    strtolower($module) == 'transactions' ||
    strtolower($module) == 'ratatransactions' ||
    strtolower($module) == 'communicationtransactions' ||
    strtolower($module) == 'emetransactions' ||
    strtolower($module) == 'peitransactions' ||
    strtolower($module) == 'cgyetransactions'||
    strtolower($module) == 'midyeartransactions'||
    strtolower($module) == 'uniformtransactions'||
    strtolower($module) == 'ricetransactions'||
    strtolower($module) == 'initialsalaries' ||
    strtolower($module) == 'leavemonetizations' ||
    strtolower($module) == 'lastsalaries' ||
    strtolower($module) == 'cancelsalaries' ||
    strtolower($module) == 'anniversarytransactions' ||
    strtolower($module) == 'traveltransactions' ||
    strtolower($module) == 'otherbenefits' ||
    strtolower($module) == 'honorarias' ||
    strtolower($module) == 'pbbtransactions') ?
    $payrolls = 'collapse list-unstyled in' : $payrolls = 'collapse list-unstyled';

    (strtolower($module) == 'transactions' ||
    strtolower($module) == 'nonplantilla' ||
    strtolower($module) == 'overtimepay') ?
    $transactions = 'collapse list-unstyled in' : $transactions = 'collapse list-unstyled';

    (strtolower($module) == 'ratatransactions') ? $ratatransactions = 'active' : $ratatransactions = '';
    (strtolower($module) == 'communicationtransactions') ? $communicationtransactions = 'active' : $communicationtransactions = '';
    (strtolower($module) == 'emetransactions') ? $emetransactions = 'active' : $emetransactions = '';
    (strtolower($module) == 'peitransactions') ? $peitransactions = 'active' : $peitransactions = '';
    (strtolower($module) == 'cgyetransactions') ? $cgyetransactions = 'active' : $cgyetransactions = '';
    (strtolower($module) == 'pbbtransactions') ? $pbbtransactions = 'active' : $pbbtransactions = '';
    (strtolower($module) == 'uniformtransactions') ? $uniformtransactions = 'active' : $uniformtransactions = '';
    (strtolower($module) == 'midyeartransactions') ? $midyeartransactions = 'active' : $midyeartransactions = '';
    (strtolower($module) == 'ricetransactions') ? $ricetransactions = 'active' : $ricetransactions = '';
    (strtolower($module) == 'initialsalaries') ? $initialsalaries = 'active' : $initialsalaries = '';
    (strtolower($module) == 'lastsalaries') ? $lastsalaries = 'active' : $lastsalaries = '';
    (strtolower($module) == 'cancelsalaries') ? $cancelsalaries = 'active' : $cancelsalaries = '';
    (strtolower($module) == 'longevitysetup') ? $longevitysetup = 'active' : $longevitysetup = '';
    (strtolower($module) == 'traveltransactions') ? $traveltransactions = 'active' : $traveltransactions = '';
    (strtolower($module) == 'anniversarytransactions') ? $anniversarytransactions = 'active' : $anniversarytransactions = '';
    (strtolower($module) == 'leavemonetizations') ? $leavemonetizations = 'active' : $leavemonetizations = '';
    (strtolower($module) == 'loanshistory') ? $loanshistory = 'active' : $loanshistory = '';
    (strtolower($module) == 'importmodule') ? $importmodule = 'active' : $importmodule = '';
    (strtolower($module) == 'otherbenefits') ? $otherbenefits = 'active' : $otherbenefits = '';
    (strtolower($module) == 'honorarias') ? $honorarias = 'active' : $honorarias = '';

    (strtolower($module) == 'ratatransactions' ||
    strtolower($module) == 'communicationtransactions' ||
     strtolower($module) == 'emetransactions' ||
     strtolower($module) == 'peitransactions' ||
     strtolower($module) == 'cgyetransactions' ||
     strtolower($module) == 'midyeartransactions' ||
     strtolower($module) == 'uniformtransactions' ||
     strtolower($module) == 'anniversarytransactions' ||
     strtolower($module) == 'ricetransactions' ||
     strtolower($module) == 'traveltransactions' ||
     strtolower($module) == 'otherbenefits' ||
     strtolower($module) == 'honorarias' ||
     strtolower($module) == 'pbbtransactions') ?
    $specialpayrolls = 'collapse list-unstyled in' : $specialpayrolls = 'collapse list-unstyled';

    (strtolower($module) == 'initialsalaries' ||
    strtolower($module) == 'lastsalaries' ||
    strtolower($module) == 'leavemonetizations' ||
    strtolower($module) == 'cancelsalaries') ?
    $otherpayrolls = 'collapse list-unstyled in' : $otherpayrolls = 'collapse list-unstyled';

?>


<!-- Sidebar -->
<nav id="sidebar">
    <div style="height: 100px;"></div>
    <div class="sidebar-header">
        <a href="#" onclick="closeNav();" style="vertical-align:middle;line-height:20px;">
          <i class="fa fa-backward" aria-hidden="true" id="closeNav"></i>
        </a>
        <a href="{{ url('/') }}" id="mHome" style="float:right;color:white;">
            <i class="fas fa-home" aria-hidden="true" title="Home/Exit Module"></i>
        </a>
    </div>

    <ul class="list-unstyled components" style="height: 520px;overflow-y: scroll;">
        <li class="{{ $beginningbalances }} {{$previousemployer}} {{$payrollconfigurations}} {{ $annualtaxsetup }} {{ $filemanagers }} {{ $employees_payroll_informations }} {{ $longevitysetup }} {{ $importmodule }} {{ $users }}" >
            <a href="#adminSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" id="administrator">
                <i class="fas fa-portrait"></i>&nbsp; Administration
            </a>
            <ul class="{{ $admin }}" id="adminSubmenu">
                <li class="{{ $users  }}">
                    <a href="{{ url('payrolls/admin/users')}}" id="users" class="topmenu-style subMenus" >
                        <i class="far fa-circle"></i>&nbsp; User Management
                    </a>
                </li>
                <li class="{{ $filemanagers  }}">
                    <a href="{{ url('payrolls/admin/filemanagers/benefits')}}" id="filemanagers" class="topmenu-style subMenus" >
                        <i class="far fa-circle"></i>&nbsp; File Manager
                    </a>
                </li>
                <li class="{{ $employees_payroll_informations }}">
                    <a href="{{ url('payrolls/admin/employees_payroll_informations')}}" id="payroll_info" class="topmenu-style subMenus" >
                        <i class="far fa-circle"></i>&nbsp; Payroll Employee Info
                    </a>
                </li>
                <!-- <li class="{{ $employeesetup }}" >
                    <a href="{{ url('payrolls/admin/employeesetup') }}">
                        <i class="far fa-circle"></i>&nbsp; Employee Setup
                    </a>
                </li> -->
                <li class="{{ $annualtaxsetup }}" >
                    <a href="{{ url('payrolls/admin/annualtaxsetup') }}">
                        <i class="far fa-circle"></i>&nbsp; Annual Tax Setup
                    </a>
                </li>
                <li class="{{ $longevitysetup }}" >
                    <a href="{{ url('payrolls/admin/longevitysetup') }}">
                        <i class="far fa-circle"></i>&nbsp; Longevity Setup
                    </a>
                </li>
                <!-- <li class="{{ $beginningbalances }}">
                   <a href="{{ url('payrolls/admin/beginningbalances')}}">
                        <i class="far fa-circle"></i>&nbsp; Employee Beg. Bal.
                   </a>
                </li>
                <li class="{{ $previousemployer }}">
                    <a href="{{ url('payrolls/admin/previousemployer')}}">
                        <i class="far fa-circle"></i>&nbsp; Previous Employer
                    </a>
                </li>
                <li class="{{ $payrollconfigurations }}">
                    <a   href="{{ url('payrolls/admin/payrollconfigurations')}}">
                        <i class="far fa-circle"></i>&nbsp; Payroll Configuration
                    </a>
                </li> -->
                <li class="{{ $loanshistory }}">
                    <a   href="{{ url('payrolls/admin/loanshistory')}}">
                        <i class="far fa-circle"></i>&nbsp; Loans History
                    </a>
                </li>
                <li class="{{ $importmodule }}">
                    <a   href="{{ url('payrolls/admin/importmodule')}}">
                        <i class="far fa-circle"></i>&nbsp; Import Loans
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ $plantilla }} {{ $ratatransactions }} {{ $otherbenefits }} {{ $anniversarytransactions }} {{ $honorarias }} {{ $peitransactions }} {{ $ricetransactions }} {{ $cgyetransactions }}">
            <a href="#transactionSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
            <i class="far fa-money-bill-alt"></i>&nbsp;Transactions
            </a>
            <ul class="{{ $transactions }} {{ $payrolls }}" id="transactionSubmenu">
                <li class="{{ $plantilla }}{{ $nonplantilla }} {{ $overtimepay }}" >
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="far fa-circle"></i>&nbsp; Regular Payroll
                    </a>
                    <ul class="{{ $transactions }}" id="homeSubmenu">
                        <li class="{{ $plantilla }}">
                            <a href="{{ url('payrolls/transactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Plantilla
                            </a>
                        </li>
                        <li class="{{ $nonplantilla }}">
                            <a href="{{ url('payrolls/nonplantilla') }}">
                            <i class="far fa-circle"></i>&nbsp; Non Plantilla
                            </a>
                        </li>
                        <li class="{{ $overtimepay }}">
                            <a href="{{ url('payrolls/overtimepay') }}">
                            <i class="far fa-circle"></i>&nbsp;    Overtime Pay
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ $ratatransactions }} {{ $communicationtransactions }} {{ $ratatransactions }} {{ $peitransactions }} {{ $emetransactions }} {{ $cgyetransactions }} {{ $traveltransactions }} {{ $pbbtransactions }} {{ $anniversarytransactions }} {{ $uniformtransactions }} {{ $ricetransactions }} {{ $midyeartransactions }} {{ $otherbenefits }} {{ $honorarias }}">
                    <a href="#specialPayrollSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="far fa-circle"></i>&nbsp; Special Payroll
                    </a>
                    <ul class="{{ $specialpayrolls }} {{ $ratatransactions }} {{ $communicationtransactions }} {{ $ratatransactions }} {{ $peitransactions }} {{ $emetransactions }} {{ $cgyetransactions }} {{ $pbbtransactions }} {{ $otherbenefits }}" id="specialPayrollSubmenu">
                        <!-- <li class="{{ $ratatransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/ratatransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Rata
                            </a>
                        </li> -->
                        <li class="{{ $ricetransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/ricetransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Rice Allowance
                            </a>
                        </li>
                        <li class="{{ $uniformtransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/uniformtransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Uniform Allowance
                            </a>
                        </li>
                        <li class="{{ $traveltransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/traveltransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Travel Allowance
                            </a>
                        </li>
                        <!-- <li class="{{ $communicationtransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/communicationtransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Communication
                            </a>
                        </li> -->
                        <!-- <li class="{{ $emetransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/emetransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; EME
                            </a>
                        </li> -->
                        <li class="{{ $peitransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/peitransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; PEI
                            </a>
                        </li>
                        <li class="{{ $pbbtransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/pbbtransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; PBB
                            </a>
                        </li>
                        <li class="{{ $midyeartransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/midyeartransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Mid Year Bonus
                            </a>
                        </li>
                        <li class="{{ $cgyetransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/cgyetransactions') }}" >
                                <i class="far fa-circle" ></i>&nbsp; CG And Year End
                            </a>
                        </li>
                        <li class="{{ $anniversarytransactions }}">
                            <a href="{{ url('payrolls/specialpayrolls/anniversarytransactions') }}">
                                <i class="far fa-circle"></i>&nbsp; Anniversary Bonus
                            </a>
                        </li>
                        <li class="{{ $otherbenefits }}">
                            <a href="{{ url('payrolls/specialpayrolls/otherbenefits') }}">
                                <i class="far fa-circle"></i>&nbsp; Other Benefits
                            </a>
                        </li>
                         <li class="{{ $honorarias }}">
                            <a href="{{ url('payrolls/specialpayrolls/honorarias') }}">
                                <i class="far fa-circle"></i>&nbsp; Honoraria
                            </a>
                        </li>
                    </ul>
                </li >
                <li class="{{ $initialsalaries }} {{ $lastsalaries }} {{ $cancelsalaries }} {{ $leavemonetizations }}">
                    <a href="#otherPayrollSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="far fa-circle"></i>&nbsp; Other Payroll
                    </a>
                    <ul class="{{ $initialsalaries }} {{ $otherpayrolls }}" id="otherPayrollSubmenu">
                        <li class="{{ $initialsalaries }}">
                            <a href="{{ url('payrolls/otherpayrolls/initialsalaries') }}">
                                <i class="far fa-circle"></i>&nbsp; Initial Salary
                            </a>
                        </li>
                         <li class="{{ $lastsalaries }}">
                            <a href="{{ url('payrolls/otherpayrolls/lastsalaries') }}">
                                <i class="far fa-circle"></i>&nbsp; Last Salary
                            </a>
                        </li>
                         <li class="{{ $cancelsalaries }}">
                            <a href="{{ url('payrolls/otherpayrolls/cancelsalaries') }}">
                                <i class="far fa-circle"></i>&nbsp; Cancel Salaries
                            </a>
                        </li>
                        <li class="{{ $leavemonetizations }}">
                            <a href="{{ url('payrolls/otherpayrolls/leavemonetizations') }}">
                                <i class="far fa-circle"></i>&nbsp; Leave Monetization
                            </a>
                        </li>
                    </ul>
                 </li >
            </ul>
        </li>
        <li class="{{ $reports }}">
            <a href="{{ url('payrolls/reports') }}">
            <i class="fas fa-briefcase"></i>&nbsp; Reports
            </a>
        </li>

    </ul>
</nav>


