<div class="col-md-12">
	<table class="table datatable" id="tbl_salaryinfo">
		<thead>
			<tr>
				<th>Effectivity Date</th>
				<th>Description</th>
				<th>SG</th>
				<th>Basic 1</th>
				<th>Basic 2</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_salaryinfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_salaryinfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	    	clear_form_elements("myForm2");
	        $(this).removeClass('selected');

	        _id 				= $(this).data('id');
			_employeeId			= $(this).data('employeeid');
			_salaryGradeId		= $(this).data('salarygradeid');
			_jobGradeId			= $(this).data('jobgradeid');
			_description		= $(this).data('description');
			_position_id		= $(this).data('position_id');
			_positionitem_id	= $(this).data('positionitem_id');
			_basic_amount_one	= $(this).data('basic_amount_one');
			_basic_amount_two	= $(this).data('basic_amount_two');
			_effectiveDate		= $(this).data('effectivitydate');
			_step_inc			= $(this).data('step_inc');

			$('#effective_salarydate').val(_effectiveDate);
			$('#salary_description').val(_description);
			$('#basic_amount_one').val(_basic_amount_one);
			$('#basic_amount_two').val(_basic_amount_two);
			$('#salary_employee_id').val(_employeeId);
			$('#salarygrade_id').val(_salaryGradeId);
			$('#jobgrade_id').val(_jobGradeId);
			$('#sgjginfo_id').val(_id);
			$('#positionitem_id').val(_positionitem_id);
			$('#position_id').val(_position_id);
			$('#jgstep_inc').val(_step_inc);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

	$(document).on('change','#jgstep_inc',function(){
		var id = "";
		var step_no = $(this).val();
		id = $('#jobgrade_id').find(':selected').val();

		$.ajax({
			url:base_url+module_prefix+module+'/getJgstep',
			data:{'step_no':step_no,'id':id},
			type:'get',
			dataType:'JSON',
			success:function(data){
				var amount = 0;
				$.each(data,function(k,v){
					amount = v;
				})
				$('#new_rate').val(amount);
			}
		})

	});


	$('input[type=radio][name=optSalary]').change(function() {
	 	$('#jobgrade_id').find(':selected').val('');
	 	$('#jobgrade_id').find(':selected').text('');
	 	$('#salarygrade_id').find(':selected').text('');
	 	$('#amount').val('');
        $('#step_inc').val('');
        $('#jgamount').val('');
        $('#jgstep_inc').val('');
        if (this.value == 'optSG') {
            $('.divsg').removeClass('hidden');
            $('.divjg').addClass('hidden');
        }
        else  {
            $('.divjg').removeClass('hidden');
            $('.divsg').addClass('hidden');
        }
    });

 //    $(document).on('change','#step_inc',function(){
	// 	var id = "";
	// 	var step_no = $(this).val();
	// 	id = $('#salarygrade_id').find(':selected').val();

	// 	$.ajax({
	// 		url:base_url+module_prefix+module+'/getSgstep',
	// 		data:{'step_no':step_no,'id':id},
	// 		type:'get',
	// 		dataType:'JSON',
	// 		success:function(data){
	// 			var amount = 0;
	// 			$.each(data,function(k,v){
	// 				amount = v;
	// 			})
	// 			$('#new_rate').val(amount);
	// 		}
	// 	})

	// });


})
</script>
