<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_adjustments">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Type</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->tax_type }}</td>
					<td>{{ $value->amount }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
	
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_adjustments').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_adjustments tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

	$('#tbl_adjustments tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#tax_type').val(data.tax_type);
				$('#amount').val(data.amount);
				$('#payroll_group').val(data.payroll_group);
				$('#itr_classification').val(data.itr_classification);
				$('#alphalist_classification').val(data.alphalist_classification);
				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
