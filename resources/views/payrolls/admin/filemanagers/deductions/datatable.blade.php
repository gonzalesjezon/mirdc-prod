<div class="col-md-12">
	<table class="table table-responsive datatable nowrap" id="tbl_deductions">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Type</th>
				<th>Amount</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody class="text-center">
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->tax_type }}</td>
					<td>{{ $value->amount }}</td>
					<td><a class="btn btn-xs btn-danger delete_item" data-function_name="delete" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a></td>
				</tr>
				@endforeach

		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deductions').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_deductions tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );


	$('#tbl_deductions tr').on('click',function(){
		var id = 0;
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#tax_type').val(data.tax_type);
				$('#amount').val(data.amount);
				$('#payroll_group').val(data.payroll_group);
				$('#itr_classification').val(data.itr_classification);
				$('#alphalist_classification').val(data.alphalist_classification);
				$('#for_update').val(data.id);
			}
		})
	});

	// ======================================================= //
   // ============ DELETE  FUNCTION ==================== //
  // ===================================================== //

$(document).on('click','.delete_item',function(){
	id 	= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							if(isConfirm.value == true){
								window.location.href = base_url+module_prefix+module;
							}else{
								return false;
							}
						})

					}

				})
			}else{
				return false;
			}
		});
	}
})

})
</script>
