<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_employee_status">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Category</th>
			</tr>
		</thead>
		<tbody class="text-center">
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-category="{{ $value->category }}" data-name="{{ $value->name }}" data-code="{{ $value->code }}" data-btnnew="newEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus" data-btncancel="cancelEmployeeStatus">
				<td>{{ $value->code }}</td>
				<td>{{ $value->name }}</td>
				<td>{{ ($value->category == 1 ) ? 'Plantilla' : 'Non Plantilla'  }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_employee_status').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_employee_status tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	employeestatus_id 	= $(this).data('id');
			code 			= $(this).data('code');
			name 			= $(this).data('name');
			category 			= $(this).data('category');

			$('#employeestatus_id').val(employeestatus_id);
			$('#code').val(code);
			$('#name').val(name);
			$('#category').val(category);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
