@extends('app-filemanager')

@section('filemanager-content')
<div id="loans" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>LOANS</label>
		</div>

	</div>

	<div class="sub-panel">
		{!! $controller->show() !!}
	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="loan_id" id="loan_id">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper" >
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_saveloans"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-5">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Code</label></td>
								<td>
									@if ($errors->has('code'))
									    <span class="text-danger">{{ $errors->first('code') }}</span>
									@endif
									<input type="text" name="code" id="code" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Name</label></td>
								<td>
									@if ($errors->has('name'))
									    <span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
									<input type="text" name="name" id="name" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Type</label></td>
								<td>
									@if ($errors->has('loan_type'))
									    <span class="text-danger">{{ $errors->first('loan_type') }}</span>
									@endif
									<select name="loan_type" id="loan_type" class="form-control font-style2">
										<option value=""></option>
										<option value="Pagibig Loan">Pagibig</option>
										<option value="SSS Loan">SSS</option>
										<option value="GENF LEXI">GENF</option>
										<option value="GSIS">GSIS</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-7">
<!-- 						<table class="table borderless text-right" style="border: none;">
							<tr>
								<td><label>Category</label></td>
								<td>
									@if ($errors->has('category'))
									    <span class="text-danger">{{ $errors->first('category') }}</span>
									@endif
									<select name="category" id="category" class="form-control font-style2">
										<option value=""></option>
										<option value="MPL">MPL</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>GSIS Ex. Column</label></td>
								<td>
									@if ($errors->has('gsis_excel_col'))
									    <span class="text-danger">{{ $errors->first('gsis_excel_col') }}</span>
									@endif
									<select name="gsis_excel_col" id="gsis_excel_col" class="form-control font-style2">
										<option value=""></option>
									</select>
								</td>
							</tr>
						</table> -->
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#loan_id').val('');
		});
	})
</script>
@endsection