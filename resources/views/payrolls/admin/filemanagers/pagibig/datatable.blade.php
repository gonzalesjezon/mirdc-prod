<div class="col-md-12">
	<table class="table" id="pagibigtable">
		<thead>
			<tr>
				<th>Pagibig Below</th>
				<th>Pagibig Above</th>
				<th>EE Share</th>
				<th>ER Share</th>
				<th>Max EE Share</th>
				<th>Max ER Share</th>
				<th>Effectivity Date</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr>
					<td>{{ $value->below }}</td>
					<td>{{ $value->above }}</td>
					<td>{{ $value->ee_share }}</td>
					<td>{{ $value->er_share }}</td>
					<td>{{ $value->max_ee_share }}</td>
					<td>{{ $value->max_er_share }}</td>
					<td>{{ $value->effectivity_date }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
	<span class="paginate-style"> {!! $data->render() !!}</span>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#pagibigtable').DataTable();

	$('#pagibigtable tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

})
</script>
