@extends('app-filemanager')

@section('filemanager-content')
<div id="tax" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>
		<div class="col-md-6">

		</div>
	</div>

	<div class="tab-container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#table_tax">Tax Table</a></li>
			<li><a href="#table_taxannual">Annual Tax Table</a></li>
			<li><a href="#policy_tax">Policy</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div id="table_tax" class="tab-pane fade in active">

			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="update_taxtable" id="update_taxtable">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper" style="margin-top: 15px;margin-bottom: 15px;">
							<a class="btn btn-xs btn-info btn-savebg btn_new" data-btnnew="newTaxTable" data-btnsave="saveTaxTable" data-btnedit="editTaxTable" data-btndelete="deleteTaxTable" data-btncancel="cancelTaxTable" id="newTaxTable"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-btnnew="newTaxTable" data-btnsave="saveTaxTable" data-btnedit="editTaxTable" data-btndelete="deleteTaxTable"  data-btncancel="cancelTaxTable" id="saveTaxTable"><i class="fa fa-save"></i> Save</a>
							<!-- <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a> -->
							<a class="btn btn-xs btn-danger btn_cancel" data-btnnew="newTaxTable" data-btnsave="saveTaxTable" data-btnedit="editTaxTable" data-btndelete="deleteTaxTable"  id="cancelTaxTable"> Cancel</a>
						</div>
					</div>
					<div class="sub-panel" style="margin-top: 50px;" >

						{!! $controller->show() !!}

						<div class="col-md-5 newTaxTable">
							<table class="table borderless disableinput" style="border: none;">
								<tr>
									<td><label>Effectivity Date</label></td>
									<td>
										@if ($errors->has('effectivity_date'))
										    <span class="text-danger">{{ $errors->first('effectivity_date') }}</span>
										@endif
										<input type="text" name="effectivity_date" id="effectivity_date" class="form-control font-style2">
									</td>
								</tr>
							</table>

						</div>

					</div>

				</form>

			</div>
		</div>
		<div id="table_taxannual" class="tab-pane fade in">
			<div class="sub-panel" style="margin-top: 50px;" >

				{!! $controller->showAnnualTaxPolicy() !!}

			</div>
			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeAnnualTaxPolicy')}}" onsubmit="return false" id="form3" class="annualTaxPolicyForm">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="tax_annual_id" id="tax_annual_id">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper">
							<a class="btn btn-xs btn-info btn-savebg btn_new" data-btnnew="newAnnual" data-btnsave="saveAnnual" data-btnedit="editAnnual" data-btndelete="deleteAnnual" id="newAnnual" ><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" id="saveAnnual" data-form="form3" id="btn_savebenefits" btn_new" data-btnnew="newAnnual" data-btnsave="saveAnnual" data-btnedit="editAnnual" data-btndelete="deleteAnnual"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" id="editAnnual"><i class="fa fa-edit" btn_new" data-btnnew="newAnnual" data-btnsave="saveAnnual" data-btnedit="editAnnual" data-btndelete="deleteAnnual"></i> Edit</a>
							<a class="btn btn-xs btn-danger btn_cancel" id="cancelAnnual" btn_new" data-btnnew="newAnnual" data-btnsave="saveAnnual" data-btnedit="editAnnual" data-btndelete="deleteAnnual"> Cancel</a>
						</div>
					</div>
					<div class="benefits-content style-box2 newAnnual">
						<div class="col-md-8">
							<input type="hidden" name="from_year" id="from_year">
							<input type="hidden" name="to_year" id="to_year">
							<table class="table borderless" style="border: none;">
								<tr class="text-center">
									<td></td>
									<td>From</td>
									<td>To</td>
								</tr>
								<tr>
									<td><label>For Year</label></td>
									<td>
										@if ($errors->has('first_year'))
										    <span class="text-danger">{{ $errors->first('first_year') }}</span>
										@endif
										<select name="first_year" id="first_year" class="form-control font-style2 year">
											<option></option>
										</select>
									</td>
									<td>
										@if ($errors->has('second_year'))
										    <span class="text-danger">{{ $errors->first('second_year') }}</span>
										@endif
										<select name="second_year" id="second_year" class="form-control font-style2 year">
											<option></option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label>Range</label></td>
									<td>
										@if ($errors->has('below_amount'))
										    <span class="text-danger">{{ $errors->first('below_amount') }}</span>
										@endif
										<input type="text" name="below_amount" id="below_amount" class="form-control font-style2 onlyNumber" placeholder="Below">
									</td>
									<td>
										@if ($errors->has('above_amount'))
										    <span class="text-danger">{{ $errors->first('above_amount') }}</span>
										@endif
										<input type="text" name="above_amount" id="above_amount" class="form-control font-style2 onlyNumber" placeholder="Above">
									</td>
								</tr>
								<tr>
									<td><label>Rate</label></td>
									<td>
										@if ($errors->has('rate_percentage'))
										    <span class="text-danger">{{ $errors->first('rate_percentage') }}</span>
										@endif
										<input type="text" name="rate_percentage" id="rate_percentage" maxlength="3" class="form-control font-style2" placeholder="Percentage">
									</td>
									<td>
										@if ($errors->has('rate_amount'))
										    <span class="text-danger">{{ $errors->first('rate_amount') }}</span>
										@endif
										<input type="text" name="rate_amount" id="rate_amount" class="form-control onlyNumber font-style2" placeholder="Amount">
									</td>
								</tr>
								<tr>
									<td><label>Excess Over</label></td>
									<td>
										@if ($errors->has('excess_amount'))
										    <span class="text-danger">{{ $errors->first('excess_amount') }}</span>
										@endif
										<input type="text" name="excess_amount" id="excess_amount" class="form-control font-style2 onlyNumber">
									</td>
								</tr>
								<tr>
									<td><label>Effectivity Date</label></td>
									<td>
										@if ($errors->has('annual_effectivity_date'))
										    <span class="text-danger">{{ $errors->first('annual_effectivity_date') }}</span>
										@endif
										<input type="text" name="annual_effectivity_date" id="annual_effectivity_date" class="form-control font-style2 datepicker">
									</td>
								</tr>
							</table>
						</div>
					</div>
				</form>

			</div>
		</div>
		<div id="policy_tax" class="tab-pane fade in">
			<div class="sub-panel" style="margin-top: 50px;" >

				{!! $controller->showTaxpolicy() !!}

			</div>

			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeTaxpolicy')}}" onsubmit="return false" id="form4" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="update_taxpolicy" id="update_taxpolicy">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper">
							<a class="btn btn-xs btn-info btn-savebg btn_new" data-btnnew="newPolicy" data-btnsave="savePolicy" data-btnedit="editPolicy" data-btndelete="deletePolicy" data-btncancel="cancelPolicy" id="newPolicy"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form4" data-btnnew="newPolicy" data-btnsave="savePolicy" data-btnedit="editPolicy" data-btndelete="deletePolicy" data-btncancel="cancelPolicy" id="savePolicy"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newPolicy" data-btnsave="savePolicy" data-btnedit="editPolicy" data-btndelete="deletePolicy" data-btncancel="cancelPolicy" id="editPolicy"><i class="fa fa-edit"></i> Edit</a>
							<a class="btn btn-xs btn-danger btn_cancel" data-btnnew="newPolicy" data-btnsave="savePolicy" data-btnedit="editPolicy" data-btndelete="deletePolicy" id="cancelPolicy" data-form="myform"> Cancel</a>
						</div>
					</div>
					<div class="benefits-content style-box2 newPolicy">
						<div class="col-md-12">
							<div class="col-md-6">
								<table class="table borderless" style="border: none;">
									<tr>
										<td><label>Policy Name</label></td>
										<td>
											@if ($errors->has('policy_name'))
											    <span class="text-danger">{{ $errors->first('policy_name') }}</span>
											@endif
											<input type="text" name="policy_name" id="policy_name" class="form-control font-style2">
										</td>
									</tr>
									<tr>
										<td><label>Pay Period</label></td>
										<td>
											@if ($errors->has('pay_period'))
											    <span class="text-danger">{{ $errors->first('pay_period') }}</span>
											@endif
											<select name="pay_period" id="pay_period" class="form-control font-style2">
												<option value=""></option>
												<option value="Semi-Monthly">Semi-Monthly</option>
												<option value="Monthly">Monthly</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Deduction Period</label></td>
										<td>
											@if ($errors->has('deduction_period'))
											    <span class="text-danger">{{ $errors->first('deduction_period') }}</span>
											@endif
											<select name="deduction_period" id="deduction_period" class="form-control font-style2">
												<option value=""></option>
												<option value="Both">Both</option>
												<option value="First Half">First Half</option>
												<option value="Second Half">Second Half</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Policy Type</label></td>
										<td>
											@if ($errors->has('policy_type'))
											    <span class="text-danger">{{ $errors->first('policy_type') }}</span>
											@endif
											<select name="policy_type" id="policy_type" class="form-control font-style2">
												<option value=""></option>
												<option value="System Generated">System Generated</option>
												<option value="Inputted">Inputted</option>

											</select>
										</td>
									</tr>
									<tr>
										<td><label>Job Grade Rate</label></td>
										<td>
											@if ($errors->has('job_grade_rate'))
											    <span class="text-danger">{{ $errors->first('job_grade_rate') }}</span>
											@endif
											<select name="job_grade_rate" id="job_grade_rate" class="form-control font-style2">
												<option value=""></option>
												<option value=".02">2 %</option>
												<option value=".03">3 %</option>
												<option value=".05">5 %</option>
												<option value=".10">10 %</option>
											</select>
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="table borderless" style="border: none;">
									<tr>
										<td><label>Base On</label></td>
										<td>
											@if ($errors->has('based_on'))
											    <span class="text-danger">{{ $errors->first('based_on') }}</span>
											@endif
											<select name="based_on" id="based_on" class="form-control font-style2">
												<option value=""></option>
												<option value="Gross Salary">Gross Salary</option>
												<option value="Gross Taxable">Gross Taxable</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Computation</label></td>
										<td>
											@if ($errors->has('computation'))
											    <span class="text-danger">{{ $errors->first('computation') }}</span>
											@endif
											<select name="computation" id="computation" class="form-control font-style2">
												<option value=""></option>
												<option value="Gross Taxable">Gross Taxable</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="is_withholding" id="chk_monthly" value="Monthly" >
											<label>Monthly</label>

										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="is_withholding" id="chk_annual" value="Annual">
											<label>Annualized</label>

										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</form>

			</div>


		</div>


	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		var year = [];
		year += '<option ></option>';
		for(y = 2018; y <= 2100; y++) {
		    year += '<option value='+y+'>'+y+'</option>';
		}
		$('.year').html(year);

		$('.select2').select2();

		$('.benefits-content :input').attr("disabled",true);

		$('#first_year').on('change',function(){
			from_year = $(this).find(':selected').val();
			$('#from_year').val(from_year);
		})
		$('#second_year').on('change',function(){
			to_year = $(this).find(':selected').val();
			$('#to_year').val(to_year);
		})


	$('.newAnnual :input').prop('disabled',true);
	$('.newTaxTable :input').prop('disabled',true);
	$('.btn_new').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').prop("disabled",false);
		$('.'+btnnew).prop('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').prop("disabled",false);
		$('.'+btnnew).prop('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').prop("disabled",true);
		$('.'+btnnew).prop('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');

		form = $(this).data('form');
		clear_form_elements(form);
		$('.error-msg').remove();

	});

		$('#btnCancelTax').on('click',function(){
			$('.disableinput :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
		})

		$('.nav-tabs a').click(function(){
			$(this).tab('show');
		})

		$('.datepicker').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$('#effectivity_date').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#update_taxpolicy').val('');
			$('#update_taxannual').val('');
			$('update_taxtable').val('');
		});

		$('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})


		/*serialize All form ON SUBMIT*/
$(document).off('click',".submitme").on('click',".submitme",function(){
	form = $(this).data('form');
	btn = $(this);

	$("#"+form).ajaxForm({
		beforeSend:function(){

		},
		success:function(data){
			par  =  JSON.parse(data);
			if(par.status){

				swal({  title: par.response,
						text: '',
						type: "success",
						icon: 'success',

					}).then(function(){
						// window.location.href = base_url+module_prefix+module;
						// clear_form_elements('myform')
						annualTaxPolicyTable(par.data);
						$('.btn_cancel').trigger('click');
					});

			}else{

				swal({  title: par.response,
						text: '',
						type: "error",
						icon: 'error',

					});

			}

			btn.button('reset');
		},
		error:function(data){
			$error = data.responseJSON;
			/*reset popover*/
			$('input[type="text"], select').popover('destroy');

			/*add popover*/
			block = 0;
			$(".error-msg").remove();
			$.each($error,function(k,v){
				var messages = v.join(', ');
				msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
				$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
				if(block == 0){
					$('html, body').animate({
				        scrollTop: $('.err-'+k).offset().top - 250
				    }, 500);
				    block++;
				}
			})
			$('.saving').replaceWith(btn);
		},
		always:function(){
			setTimeout(function(){
					$('.saving').replaceWith(btn);
				},300)
		}
	}).submit();

});

function annualTaxPolicyTable(data){
	var tAnnual = $('#taxannual').DataTable();

	tAnnual.clear().draw();

	$.each(data,function(k,v){

		above_amount 	= (v.above_amount) ? v.above_amount : 0;
		below_amount 	= (v.below_amount) ? v.below_amount : 0;
		excess_amount 	= (v.excess_amount) ? v.excess_amount : 0;
		rate_amount 	= (v.rate_amount) ? v.rate_amount : 0;
		rate_percentage = (v.rate_percentage) ? v.rate_percentage : 0;
		from_year 		= (v.from_year) ? v.from_year : 0;
		to_year 		= (v.to_year) ? v.to_year : 0;

		aboveAmount = (above_amount !== 0) ? commaSeparateNumber(parseFloat(above_amount).toFixed(2)) : '';
		belowAmount = (below_amount !== 0) ? commaSeparateNumber(parseFloat(below_amount).toFixed(2)) : '';
		excessAmount = (excess_amount !== 0) ? commaSeparateNumber(parseFloat(excess_amount).toFixed(2)) : '';
		rateAmount = (rate_amount !== 0) ? commaSeparateNumber(parseFloat(rate_amount).toFixed(2)) : '';
		ratePercentage = (rate_percentage !== 0) ? rate_percentage+' %' : '';
		for_year = from_year+' - '+to_year;

		tAnnual.row.add( [
			for_year,
			belowAmount,
			aboveAmount,
			ratePercentage,
			rateAmount,
			excessAmount

        ]).draw( false );

        tAnnual.rows(k).nodes().to$().attr("data-id", v.id);
        tAnnual.rows(k).nodes().to$().attr("data-above_amount", above_amount);
        tAnnual.rows(k).nodes().to$().attr("data-below_amount", v.below_amount);
        tAnnual.rows(k).nodes().to$().attr("data-excess_amount", v.excess_amount);
        tAnnual.rows(k).nodes().to$().attr("data-rate_amount", v.rate_amount);
        tAnnual.rows(k).nodes().to$().attr("data-rate_percentage", v.rate_percentage);
        tAnnual.rows(k).nodes().to$().attr("data-from_year", v.from_year);
        tAnnual.rows(k).nodes().to$().attr("data-to_year", v.to_year);
        tAnnual.rows(k).nodes().to$().attr("data-btnnew", "newAnnual");
        tAnnual.rows(k).nodes().to$().attr("data-btnsave", "saveAnnual");
        tAnnual.rows(k).nodes().to$().attr("data-btnedit", "editAnnual");
        tAnnual.rows(k).nodes().to$().attr("data-btncancel", "cancelAnnual");
        tAnnual.rows(k).nodes().to$().attr("data-btndelete", "deleteAnnual");


	});
}

})
</script>
@endsection