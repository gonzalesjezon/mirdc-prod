@extends('app-filemanager')

@section('filemanager-content')
<div id="wagerate" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>
		<div class="col-md-6">
			
		</div>
	</div>
		
	<div class="sub-panel" >
				
		{!! $controller->show() !!}

	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="for_update" id="for_update">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savewagerate"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-6">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Wage Region</label></td>
								<td>
									@if ($errors->has('wage_region'))
									    <span class="text-danger">{{ $errors->first('wage_region') }}</span>
									@endif
									<select name="wage_region" id="wage_region" class="form-control font-style2">
										<option value=""></option>
										<option value="National Capital Region">Nation Capital Region</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Wage Rate</label></td>
								<td>
									@if ($errors->has('wage_rate'))
									    <span class="text-danger">{{ $errors->first('wage_rate') }}</span>
									@endif
									<input type="text" name="wage_rate" id="wage_rate" class="form-control font-style2"/>
										
								</td>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Effectivity Date</label></td>
								<td>
									@if ($errors->has('effectivity_date'))
									    <span class="text-danger">{{ $errors->first('effectivity_date') }}</span>
									@endif
									<input type="text" name="effectivity_date" id="effectivity_date" class="form-control font-style2"/>
										
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
		</form>
		
	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){
		
		
		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$('.nav-tabs a').click(function(){
			$(this).tab('show');
		})

		$('#effectivity_date').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#for_update').val('');
		});
	})
</script>
@endsection