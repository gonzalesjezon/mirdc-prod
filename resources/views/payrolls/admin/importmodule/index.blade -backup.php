@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div class="col-md-12">
	        <!-- page content -->
    <div class="right_col" role="main" style="margin-top: 50px;">

        <div class="col-md-6">
            <div class="panel" style="padding: 15px;">
                <div class="panel-header">
                    <h4><i class="fa fa-cogs"></i> Loan Type</h4>
                </div>
                <div class="panel-body">
                    <table class="table noborder borderless">
                        <tbody class="noborder-top" style="font-weight: bold;">
                            <tr>
                                <td>
                                    <h5>CONSOLOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>ECARD PLUS</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>SALARY LOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>CASH ADVANCE</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>EMERGENCT LOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>EDUC ASSISTANCE LOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>PL REG</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>MULTI PURPOSE LOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>HOUUSING LOAN</h5>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-round btn-block import"><i class="fa fa-upload"></i>Import</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
        </div>
        <div class="col-md-6">
            <div class="col-md-10 col-sm-12 col-xs-12 account-import" style="display: none">
                <div class="page-title">
                    <div class="title_left">
                        <h4>
                            <span class="import-message">Importing Loans</span>
                        </h4>
                    </div>
                    <div class="title_right">
                    </div>
                </div>
                <div class='x_panel'>
                    <div class="col-md-12" id="progress-width">
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            0%
                          </div>
                        </div>
                        <span id="progress-caption">Please Wait...</span>
                        <!-- <hr> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- /page content -->
        <form action="{{ url($module_prefix.'/'.$module.'/fileUpload')}} " id="preciousUpload" method="POST" files="true" style="display:none" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="file" name="file" id="template">
        </form>
    </div>

<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

  // =========================== //
 // ====== IMPORT EXCEL ======= //
// =========================== //

$(".import").click(function(){
    $("#template").trigger('click');
});

var settings;
$("#template").change(function(){
    $('#preciousUpload').ajaxForm({
        beforeSend:function(){
            $(".progress-bar").css('width','0%')
            $(".progress-bar").attr('aria-valuenow','0')
            $(".progress").show();
            $('#panel-error').hide();
            $('#success-import-wrapper').hide();
            $('.selection-map').show();
            $(".account-wrapper").hide();
            $(".import-success").hide();
            $(".account-import").show();
            $('.account-mapping').html('<tr><td colspan="2">please wait...</td></tr>');
            // $(".stone-spinner").show();
            // $("."+button).attr("disabled",true).html('<i class="fa fa-upload"></i> Please Wait while Uploading...');
        },
        uploadProgress:function(event, position, total, percentComplete){
            // console.log(percentComplete)
            $(".progress-bar").css({'width':percentComplete+'%'}).html(percentComplete+'%')
            $(".progress-bar").attr('aria-valuenow',percentComplete)
        },
        success:function(data){
            $("#progress-width").delay(1000).fadeOut(300);
            swal({
            title: "Import Successfully",
            type: "success",
            showCancelButton: false,
            confirmButtonClass: "btn-success",
            confirmButtonText: "OK",
            closeOnConfirm: false
            }).then(function(isConfirm){
                if(isConfirm.value == true){
                     window.location.href = base_url+module_prefix+module;
                }else{
                     window.location.href = base_url+module_prefix+module;
                }
            });
        },
        error:function(data){
        }
    }).submit();

    $("#template").val('');
});


})
</script>
@endsection
