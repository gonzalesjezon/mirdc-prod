
<table class="table table-responsive datatable" id="tbl_initial_salary">
	<thead>
		<tr>
			<th>Date</th>
			<th>Basic Pay</th>
			<th>LP</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_initial_salary').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_initial_salary tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
	        clear_form_elements('myForm4');

	        id 					= $(this).data('id');
			employee_id 		= $(this).data('employee_id');
			basic_pay_amount 	= $(this).data('basic_pay_amount');
			longevity_date 		= $(this).data('longevity_date');
			lp 					= $(this).data('lp');
			longevity_amount 	= $(this).data('longevity_amount');

			$('#longevity_id').val(id);
			$('#employee_id').val(employee_id);
			$('#basic_pay_amount').val(basic_pay_amount);
			$('#longevity_date').val(longevity_date);
			$('#lp').val(lp);
			$('#longevity_amount').val(longevity_amount);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
