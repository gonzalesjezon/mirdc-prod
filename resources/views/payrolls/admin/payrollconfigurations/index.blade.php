@extends('app-front')
@section('content')
<div style="height: 50px;"></div>
<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-heading">{{ $title }}</div>
		<div class="panel-body">
			<form method="POST" action="{{ url($module_prefix.'/'.$module) }}" onsubmit="return false" id="form" class="myForm" style="padding: 15px;">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col-md-6">
						<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="myForm" data-btnnew="myForm" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

							<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="myForm" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>

							<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="myForm" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<span><b>&nbsp;</b></span>
						<div class="form-group">
							<span>No. of Days in a Year</span>
							<input type="text" name="number_of_days_in_a_year" id="number_of_days_in_a_year" class="form-control onlyNumber" maxlength="3">
						</div>
						<div class="form-group">
							<span>No. of Days in a Month</span>
							<input type="text" name="number_of_days_in_a_month" id="number_of_days_in_a_month" class="form-control onlyNumber" maxlength="2">
						</div>
						<div class="form-group">
							<span>Bank Name</span>
							<select class="form-control" name="bank_id" id="bank_id">
								<option value=""></option>
								@foreach($banks as $key => $value)
								<option data-branch="{{ $value->branch_name }}"  value="{{ $value->id }}">{{ $value->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<span>Bank Branch</span>
							<input type="text" name="branch_name" id="branch_name" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<span><b>POLICY</b></span>
						<div class="form-group">
							<span>GSIS Policy</span>
							<select class="form-control" name="gsis_policy_id" id="gsis_policy_id">
								<option value=""></option>
								@foreach($gsis_policy as $key => $value)
								<option data-er_percentage="{{ $value->er_percentage }}" data-ee_percentage="{{ $value->ee_percentage }}"  value="{{ $value->id }}">{{ $value->policy_name }}</option>
								@endforeach
							</select>
							<input type="hidden" name="ee_percentage" id="ee_percentage">
							<input type="hidden" name="er_percentage" id="er_percentage">
						</div>
						<div class="form-group">
							<span>Philhealth Policy</span>
							<select class="form-control" name="philhealth_policy_id" id="philhealth_policy_id">
								<option value=""></option>
								@foreach($philhealth_policy as $key => $value)
								<option data-above="{{ $value->above }}" data-below="{{ $value->below }}" value="{{ $value->id }}">{{ $value->policy_name }}</option>
								@endforeach
							</select>
							<input type="hidden" name="above" id="above">
							<input type="hidden" name="below" id="below">
						</div>
						<div class="form-group">
							<span>Pagibig Policy</span>
							<select class="form-control" name="pagibig_policy_id" id="pagibig_policy_id">
								<option value=""></option>
								@foreach($pagibig_policy as $key => $value)
								<option value="{{ $value->id }}">{{ $value->policy_name }}</option>
								@endforeach
							</select>
						</div>
						<!-- <div class="form-group">
							<span>Tax Policy</span>
							<select class="form-control" name="tax_policy_id" id="tax_policy_id">
								<option value=""></option>
								<option value="Inputted">Inputted</option>
								<option value="System Generated">System Generated</option>
							</select>
						</div> -->
					</div>
					<div class="col-md-6">
						<span><b>BONUS</b></span>
						@foreach($benefits as $value)
						<div class="form-group">
							<span>{{ $value->name }}</span>
							<input type="text" name="benefit_id[{{ $value->id }}]"  class="form-control onlyNumber" placeholder="Amount">
						</div>
						@endforeach
						<!-- <div class="form-group">
							<span>PBB</span>
							<input type="text" name="pbb_amount" id="pbb_amount" class="form-control onlyNumber" placeholder="Amount">
							<input type="hidden" name="pbb_code" value="PBB">
						</div>
						<div class="form-group">
							<span>Cash Gift</span>
							<input type="text" name="cash_gift" id="cash_gift" class="form-control onlyNumber" placeholder="Amount">
							<input type="hidden" name="cash_gift_code" value="CG">
						</div> -->
						<div class="form-group">
							<input type="checkbox" name="chk_year_end" id="chk_year_end" class="form-check-input">
							<span class="form-check-label">Year End</span>
						</div>
						<div class="form-group">
							<input type="checkbox" name="chk_mid_year" id="chk_mid_year" class="form-check-input">
							<span class="form-check-label">Mid Year</span>
						</div>
					</div>
					<!-- <div class="col-md-3">
						<span><b>Pay Period</b></span>
						<div class="form-group">
							<span>&nbsp;</span>
							<select class="form-control" id="pei_based_on" name="pei_based_on">
								<option value=""></option>
								<option value="Monthly">Monthly</option>
								<option value="Semi Monthly">Semi Monthly</option>
								<option value="Weekly">Weekly</option>
							</select>
						</div>
						<div class="form-group">
							<span>&nbsp;</span>
							<select class="form-control" id="pbb_based_on" name="pbb_based_on">
								<option value=""></option>
								<option value="Monthly">Monthly</option>
								<option value="Semi Monthly">Semi Monthly</option>
								<option value="Weekly">Weekly</option>
							</select>
						</div>
						<div class="form-group">
							<span>&nbsp;</span>
							<select class="form-control" id="cash_gift_based_on" name="cash_gift_based_on">
								<option value=""></option>
								<option value="Monthly">Monthly</option>
								<option value="Semi Monthly">Semi Monthly</option>
								<option value="Weekly">Weekly</option>
							</select>
						</div>
					</div>
 -->
				</div>
			</form>
		</div>
	</div>
	<br><br>
</div>

@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	$('.myForm :input').attr('disabled',true)
	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		form = $(this).data('form');
		clear_form_elements(form)
		$('.error-msg').remove();

	});

	$(document).on('change','#bank_id',function(){
		bank = $(this).find(':selected').data('branch');
		$('#branch_name').val(bank).prop('readonly',true);
	});

	$(document).on('change','#gsis_policy_id',function(){
		ee_percentage = $(this).find(':selected').data('ee_percentage');
		er_percentage = $(this).find(':selected').data('er_percentage');
		console.log(er_percentage);
		$('#ee_percentage').val(ee_percentage);
		$('#er_percentage').val(er_percentage);
	});
	$(document).on('change','#philhealth_policy_id',function(){
		above = $(this).find(':selected').data('above');
		below = $(this).find(':selected').data('below');
		$('#above').val(above);
		$('#below').val(below);
	});


})
</script>
@endsection