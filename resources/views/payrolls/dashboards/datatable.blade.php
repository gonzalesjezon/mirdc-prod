
<div class="tableFixHead">
	<table class="table table-striped"  id="namelist">
		<thead>
			<tr class="text-center">
				<th  >#</th>
				<th  >Employee Name</th>
				<th  >Assumption of Duty</th>
				<th  >Status</th>
				<th  >Salary Grade</th>
				<th  >Monthly Salary</th>
				<th  >Action</th>
			</tr>
		</thead>
		<tbody >
			@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}"  >
				<td ><input type="checkbox" name="checked_emp_id[]" value="{{ $value->id  }}" class="emp_select" data-key="{{ $key }}" ></td>
				<td >{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
				<td class="text-center">{{ @$value->employeeinformation->assumption_date }}</td>
				<td class="text-center">{{ @$value->employeeinformation->employeestatus->Name }}</td>
				<td class="text-center">{{ @$value->salaryinfo->salargrade->salary_grade }}</td>
				<td class="text-right">{{ number_format(@$value->salaryinfo->basic_amount_one+@$value->salaryinfo->basic_amount_two,2) }}</td>
				<td ></td>
			</tr>
			@endforeach

		</tbody>
	</table>
	<br>
</div>
