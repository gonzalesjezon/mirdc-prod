<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >
		@foreach($data as $key => $value)

			<tr data-empid="{{ $value->id }}" data-fullname="{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary">
				<td>{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
			</tr>

		@endforeach
	</tbody>
</table>
<!-- <div class="ajax-loader">
  	<img src="{{ asset('images/30.gif') }}" class="img-responsive" />
</div> -->
<script type="text/javascript">
	$(document).ready(function(){
});

</script>