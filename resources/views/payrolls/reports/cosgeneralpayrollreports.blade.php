@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<style type="text/css">
	p{
		color: #101010;
	}
	.borderstyle2{
		border-left: none !important;
		border-right: none !important;
		border-bottom: none !important;
		border-top: 2px solid #5a5a5a !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<!-- <div class="row">
	       			<div class="col-md-5 text-right">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<i></i>
   					</div>
   					<div class="col-md-12 text-center" style="font-weight: bold;margin-left: 20px;padding-top: 15px;">
   						Metals Industry Research and Development Center <br>
   						<span style="padding-left: 25px;">General Santos Ave., Bicutan, Taguig City</span>
   					</div>
	       		</div> -->
	       		<br>
   				<table class="table" style="margin-top: 20px;border:none;">
   					<thead>
   						<tr>
   							<td colspan="9" class="text-center" style="font-weight: bold;border: none;">
   								<span>
			       					METALS INDUSTRY RESEARCH AND DEVELOPMENT <br>
			       					<b>GENERAL PAYROLL</b> <br>
			       					We hereby acknowledge to have received from MIRDC the sum herein specified our respective names representing our salaries for the perio <span class="covered_year"></span>
			       				</span>
   							</td>
   						</tr>
   						<tr class="text-center borderless" style="border-top: 2px solid #5a5a5a;font-weight: bold;">
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">ID #</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">Name</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">Basic</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">Gross Pay</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">OP/UP</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">LWOP</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">OTHER DED</td>
   							<td colspan="2">W TAX</td>
   							<td rowspan="2" style="border-bottom: 2px solid #5a5a5a;">NET PAY</td>
   						</tr>
   						<tr class="text-center" style="border-bottom: 2px solid #5a5a5a;font-weight: bold;">
   							<td style="border: none;">8%|5%</td>
   							<td style="border: none;">0%|3%</td>
   						</tr>
   					</thead>
   					<tbody id="tbl_content"></tbody>
   				</table>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	});

	var payrollType;
	var payrollText;
	$('#select_payroll').on('change',function(){
		payrollText = $(this).find(':selected').text();
		payrollType = $(this).find(':selected').val();

		$('#payroll_type').text(payrollText);
	});

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'year':year,
				'month':month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.length !== 0){
					arr = [];
					netBasicAmount 	= 0;
					netgrossAmount 	= 0;
					netLWOPAmount 	= 0;
					netTaxAmountOne = 0;
					netTaxAmountTwo = 0;
					netNetAmount 	= 0;
					$.each(data,function(k,v){
						subBasicAmount 	= 0;
						subgrossAmount 	= 0;
						subLWOPAmount 	= 0;
						subTaxAmountOne = 0;
						subTaxAmountTwo = 0;
						subNetAmount 	= 0;

						arr += '<br><tr class="text-left" style="border-bottom: 2px solid #5a5a5a;">';
						arr += '<td style="font-weight:bold;border-left:none;border-right:none;" colspan="4">'+k+'</td>';
						arr += '</tr>';

						$.each(v,function(key,val){

							employee_number = (val.employees.employee_number) ? val.employees.employee_number : '';
							firstname = val.employees.firstname;
							lastname = val.employees.lastname;
							middlename = (val.employees.middlename) ? val.employees.middlename : '';
							basicAmount = (val.employees.middlename) ? val.employees.middlename : '';

							fullname = lastname+' '+firstname+' '+middlename;

							basicAmount = val.monthly_rate_amount;
							grossAmount = val.gross_pay;
							lwopAmount = (val.total_absences_amount) ? val.total_absences_amount : 0;
							taxAmountOne = val.tax_rate_amount_one;
							taxAmountTwo = val.tax_rate_amount_two;

							deductionAmount = parseFloat(lwopAmount) + parseFloat(taxAmountOne) + parseFloat(taxAmountTwo);

							netAmount = parseFloat(grossAmount) - parseFloat(deductionAmount);

							subBasicAmount += parseFloat(basicAmount);
							subgrossAmount += parseFloat(grossAmount);
							subLWOPAmount += parseFloat(lwopAmount);
							subTaxAmountOne += parseFloat(taxAmountOne);
							subTaxAmountTwo += parseFloat(taxAmountTwo);
							subNetAmount	+= parseFloat(netAmount);

							basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
							gross_amount = (grossAmount) ? commaSeparateNumber(parseFloat(grossAmount).toFixed(2)) : '';
							lwop_amount = (lwopAmount) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '';
							tax_amount_one = (taxAmountOne) ? commaSeparateNumber(parseFloat(taxAmountOne).toFixed(2)) : '';
							tax_amount_two = (taxAmountTwo) ? commaSeparateNumber(parseFloat(taxAmountTwo).toFixed(2)) : '';
							net_amount = (netAmount) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td class="text-center" style="border:none;">'+employee_number+'</td>';
							arr += '<td class="text-left" style="border:none;">'+fullname+'</td>';
							arr += '<td class="text-right" style="border:none;">'+basic_amount+'</td>';
							arr += '<td class="text-right" style="border:none;">'+gross_amount+'</td>';
							arr += '<td class="text-right" style="border:none;"></td>';
							arr += '<td class="text-right" style="border:none;">'+lwop_amount+'</td>';
							arr += '<td class="text-right" style="border:none;"></td>';
							arr += '<td class="text-right" style="border:none;">'+tax_amount_one+'</td>';
							arr += '<td class="text-right" style="border:none;">'+tax_amount_two+'</td>';
							arr += '<td class="text-right" style="border:none;">'+net_amount+'</td>';
							arr += '</tr>';
						});

						netBasicAmount += parseFloat(subBasicAmount);
						netgrossAmount += parseFloat(subgrossAmount);
						netLWOPAmount += parseFloat(subLWOPAmount);
						netTaxAmountOne += parseFloat(subTaxAmountOne);
						netTaxAmountTwo += parseFloat(subTaxAmountTwo);
						netNetAmount += parseFloat(subNetAmount);

						sub_basic_amount = (subBasicAmount) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '';
						sub_gross_amount = (subgrossAmount) ? commaSeparateNumber(parseFloat(subgrossAmount).toFixed(2)) : '';
						sub_lwop_amount = (subLWOPAmount) ? commaSeparateNumber(parseFloat(subLWOPAmount).toFixed(2)) : '';
						sub_tax_amount_one = (subTaxAmountOne) ? commaSeparateNumber(parseFloat(subTaxAmountOne).toFixed(2)) : '';
						sub_tax_amount_two = (subTaxAmountTwo) ? commaSeparateNumber(parseFloat(subTaxAmountTwo).toFixed(2)) : '';
						sub_net_amount = (subNetAmount) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td class="text-right " style="border:none;"></td>';
						arr += '<td class="text-right " style="border:none;">TOTAL</td>';
						arr += '<td class="text-right borderstyle2">'+sub_basic_amount+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_gross_amount+'</td>';
						arr += '<td class="text-right borderstyle2"></td>';
						arr += '<td class="text-right borderstyle2">'+sub_lwop_amount+'</td>';
						arr += '<td class="text-right borderstyle2"></td>';
						arr += '<td class="text-right borderstyle2">'+sub_tax_amount_one+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_tax_amount_two+'</td>';
						arr += '<td class="text-right borderstyle2">'+sub_net_amount+'</td>';
						arr += '</tr>';

					});

					net_basic_amount = (netBasicAmount) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '';
					net_gross_amount = (netgrossAmount) ? commaSeparateNumber(parseFloat(netgrossAmount).toFixed(2)) : '';
					net_lwop_amount = (netLWOPAmount) ? commaSeparateNumber(parseFloat(netLWOPAmount).toFixed(2)) : '';
					net_tax_amount_one = (netTaxAmountOne) ? commaSeparateNumber(parseFloat(netTaxAmountOne).toFixed(2)) : '';
					net_tax_amount_two = (netTaxAmountTwo) ? commaSeparateNumber(parseFloat(netTaxAmountTwo).toFixed(2)) : '';
					net_net_amount = (netNetAmount) ? commaSeparateNumber(parseFloat(netNetAmount).toFixed(2)) : '';

					arr += '<tr style="font-weight:bold;">';
					arr += '<td class="text-center" style="border:none;"></td>';
					arr += '<td class="text-right" style="border:none;">GRAND TOTAL</td>';
					arr += '<td class="text-right borderstyle2">'+net_basic_amount+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_gross_amount+'</td>';
					arr += '<td class="text-right borderstyle2"></td>';
					arr += '<td class="text-right borderstyle2">'+net_lwop_amount+'</td>';
					arr += '<td class="text-right borderstyle2"></td>';
					arr += '<td class="text-right borderstyle2">'+net_tax_amount_one+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_tax_amount_two+'</td>';
					arr += '<td class="text-right borderstyle2">'+net_net_amount+'</td>';
					arr += '</tr>';
					arr += '<div style="height:50px;"></div>'
					arr += '<tr >';
					arr += '<td style="border:none;" colspan="5" >C E R T I F I E D : Services duly rendered as stated</td>';
					arr += '<td style="border:none;" colspan="7"> APPROVED FOR PAYMENT _______________________________</td>';
					arr += '</tr>';

					arr += '<tr class="text-center">';
					arr += '<td style="border:none;padding-top: 30px;"colspan="4"> <p style="font-weight:bold;">JELLY N. ORTIZ</p> <p style="line-height:0.5em;">Supvng. Administrative Officer </p> </td>';
					arr += '<td style="border:none;padding-top: 30px;"colspan="5"> <p style="font-weight:bold;">AGUSTIN M. FUDOLIG </p> <p style="line-height:0.5em;">  Supvng. Deputy Executive Director </p></td>';
					arr += '</tr>';

					arr += '<tr >';
					arr += '<td style="border:none;padding-top: 30px;" colspan="5" >C E R T I F I E D :  Supporting documents complete & proper <br>  and Cash available in the amount of PHP'+net_amount+'</td>';
					arr += '</tr>';

					arr += '<tr class="text-center" >';
					arr += '<td style="border:none;padding-top: 30px;" colspan="5"> <p style="font-weight:bold;">JOHNNY G. QUINGCO</p> <p style="line-height:0.5em;">Accountant IV</p> </td>';
					arr += '</tr>';

					days = daysInMonth(_monthNumber,_Year)

					if(_payPeriod == 'monthly'){
						_coveredPeriod = _Month+' 1-'+days+', '+_Year;
					}else{
						switch(_semiPayPeriod){
							case 'firsthalf':
								_coveredPeriod = _Month+' 1-15, '+_Year;
							break;
							default:
								_coveredPeriod =_Month+' 16-'+days+', '+_Year;
							break;
						}
					}

					$('.covered_year').text(_coveredPeriod);

					$('#tbl_content').html(arr);

					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}

});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

})
</script>
@endsection