@extends('app-reports')


@section('reports-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printportrait.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " style="width: 960px;" id="reports">
	       		<div class="row">
	       		<!-- 	<div class="col-md-5 text-right">
   						<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
   						<i></i>
   					</div> -->
   					<div class="col-md-12 text-center" style="font-weight: bold;margin: auto;padding-top: 15px;">
   						Metals Industry Research and Development Center <br>
   						ADMINISTRATIVE AND GENERAL SERVICES SECTION
   					</div>
	       		</div>
	       		<br>
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<span>
	       					YEAR-END BONUS AND CASH GIFT FOR THE YEAR
	       					<span class="covered_date"></span>
	       				</span>
	       			</div>
	       		</div>
   				<table class="table" style="border: 2px solid #5a5a5a">
   					<thead style="border: 2px solid #5a5a5a">
   						<tr class="text-center" style="font-weight: bold;">
   							<td style="width: 10px;">#</td>
   							<td>Name</td>
   							<td>Amount</td>
   						</tr>
   					</thead>
   					</tfoot>
   					<tbody id="tbl_content"></tbody>
   				</table>
   				<div class="row" style="padding: 10px;">
   					<div class="col-md-12">
   						Item 5.0 of DBM Budget Circular No. 2017 - 2 dated May 8, 2017 provides that:
The Mid-Year Bonus equivalent to one(1) month basic pay as of May 15 shall be given to entitled personnel not earlier than May 15 of the current year, provided that personnel has rendered at least a total or an aggregate of four (4) months of service from July 1 of the immediately preceeding year to May 15 of the current year; remains to be in the government service as of May 15 of the current year; and has received at least a satisfactory performance rating in the immediately preceeding rating period.
   					</div>
   				</div>
   				<div class="row" style="padding: 10px;">
   					<div class="col-md-6">
   						Prepared by: <br><br><br>
   						<p style="margin-left: 30px;color:#333;">
							<b>LAILA R. PORLUCAS</b> <br>
							Administrative Officer IV
   						</p>
   					</div>
   				</div>
   				<div class="row" style="padding: 10px;">
   					<div class="col-md-6">
   						Certified Correct: <br><br><br>
   						<p style="margin-left: 30px;color:#333;">
							<b>JELLY N. ORTIZ, DPA</b> <br>
							Chief, FAD - AGSS
   						</p>
   					</div>
   				</div>
   				<div class="row" style="padding: 10px;">
   					<div class="col-md-6">
   						Reviewed by: <br><br><br>
						<p style="margin-left: 30px;color:#333;">
							<b>AUREA T. MOTAS</b> <br>
							Chief AO, FAD
						</p>
   					</div>
   				</div>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})
	var _payPeriod;
	var _semiPayPeriod;
	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

$(document).on('click','#print',function(){
	$('#reports').printThis();
});
$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/show',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.length !== 0){
					arr = [];

					ctr = 1;
					sub_total = 0;
					year_end_amount = 0;
					$.each(data,function(key,value){

						arr += '<tr>';
						arr += '<td></td>';
						arr += '<td style="font-weight:bold;" colspan="2">'+key+'</td>';


						$.each(value,function(k,v){

							firstname = (v.employees.firstname) ? v.employees.firstname : '';
							lastname = (v.employees.lastname) ? v.employees.lastname : '';
							middlename = (v.employees.middlename) ? v.employees.middlename : '';
							year_end_amount = (v.amount) ? v.amount : 0;

							fullname = lastname+' '+firstname+' '+middlename;

							sub_total += parseFloat(year_end_amount);

							year_end_amount = (year_end_amount !== 0) ? commaSeparateNumber(parseFloat(year_end_amount).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+ctr+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-right">'+year_end_amount+'</td>';
							arr += '</tr>';

							ctr++;

						});

						arr += '</tr>';

					});

					sub_total = (sub_total !== 0) ? commaSeparateNumber(parseFloat(sub_total).toFixed(2)) : '';
					arr += '<tr>';
					arr += '<td></td>';
					arr += '<td style="font-weight:bold;">GRAND TOTAL</td>';
					arr += '<td style="font-weight:bold;" class="text-right">'+sub_total+'</td>';
					arr += '</tr>';

					$('#tbl_content').html(arr);


					// days = daysInMonth(_monthNumber,_Year)

					// if(_payPeriod == 'monthly'){

					// }else{
					// 	switch(_semiPayPeriod){
					// 		case 'firsthalf':
					// 			_coveredPeriod = _Month+' 1-15, '+_Year;
					// 		break;
					// 		default:
					// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
					// 		break;
					// 	}
					// }

					_coveredPeriod = _Year;

					$('.covered_date').text(_coveredPeriod);



					$('#btnModal').trigger('click');

				}else{
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}
			}
		})
	}



});
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
})
</script>
@endsection