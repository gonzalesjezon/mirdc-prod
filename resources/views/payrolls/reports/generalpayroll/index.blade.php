@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<img src="{{ url('images/mirdc_logo.gif') }}" style="height: 80px;">
	       				<h5><b>METAL INDUSTRY AND RESEARCH AND DEVELOPMENT</b></h5>
	       				<h6>We hereby akcnowledge to have received from MIRDC the sum herein specified our respectives names representing SALARY for the pay period  <b><span id="month_year"></span></b></h6>

	       			</div>
					<table class="table" style="border: 2px solid #585555;">
					 <thead class="text-center" style="font-weight: bold;">
						 <tr >

						 	<td rowspan="3" style="width: 240px;border: 2px solid #585555;">Name</td>
						 	<td colspan="3" style="border: 2px solid #585555;">Compensation</td>
						 	<td colspan="3" style="border: 2px solid #585555;">Deduction</td>
						 	<td colspan="2" style="border: 2px solid #585555;">NET PAY</td>
						 </tr>
						 <tr>
						 	<td style="border: 2px solid #585555;" >BASIC</td>
						 	<td style="border: 2px solid #585555;" >RATA</td>
						 	<td  style="border: 2px solid #585555;">HP</td>
						 	<td style="border: 2px solid #585555;">GSIS PREM</td>
						 	<td style="border: 2px solid #585555;">PHILHEALTH</td>
						 	<td style="border: 2px solid #585555;" rowspan="2"  style="width:200px;">Other</td>
						 	<td style="border: 2px solid #585555;" rowspan="2">MONTHLY</td>
						 	<td style="border: 2px solid #585555;" rowspan="2">(1-15 / 16-31)</td>
						 </tr>
						 <tr>
						 	<td style="border: 2px solid #585555;" >PERA</td>
						 	<td style="border: 2px solid #585555;">SALA</td>
						 	<td style="border: 2px solid #585555;">LP</td>
						 	<td style="border: 2px solid #585555;">PIB PREM</td>
						 	<td style="border: 2px solid #585555;">W/TAX</td>

						 </tr>
					 </thead>
					 <tfoot>
					 	<tr>

					 		<td style="border:none" colspan="4">
					 			<span style="margin-left: 120px;">CERTIFIED: Services duly rendered as stated:</span>
					 		</td>
					 		<td style="border:none" colspan="7">
					 			<span>
						 			APPROVED FOR PAYMENT : <br>
						 			(amount in words) <br>
						 			_____________________________
					 			</span>
					 		</td>
					 	</tr>
					 	<tr >
					 		<td style="border:none" colspan="4" class="text-center">
					 			<b>JELLY N. ORTIZ</b> <br>
					 			Supervising Administrative Officer
					 		</td>
					 		<td style="border:none" colspan="7">
					 			<span style="padding-left: 70px;">
					 				<b>ENGR. ROBERT O. DIZON</b>
					 			</span>
					 			<br>
					 			<span style="padding-left: 80px;">
					 				Executive Director
					 			</span>
					 		</td>
					 	</tr>
					 	<tr>
					 		<td style="border:none;padding-top: 30px;" colspan="4"  >
					 			<span style="margin-left: 120px;">
					 				CERTIFIED: Supporting documents complete & proper, and cash
					 			</span>
					 			<br>
					 			<span style="margin-left: 120px;">available in the amount of  ______________________</span>
					 		</td>
					 		<td style="border:none;padding-top: 30px;" colspan="7" >
					 			<span >
					 				CERTIFIED: Each employee whose name appears above has <br>
					 				been paid the amount indicated opposite his/her name

					 			</span>
					 		</td>
					 	</tr>
					 	<tr  >
					 		<td style="border:none;padding-top: 25px;" colspan="4" class="text-center">
					 			<span>
					 				<b>JOHNNY G. QUINCO</b> <br>
					 				Accountant IV
					 			</span>
					 		</td>
					 		<td style="border:none;padding-top: 25px;" colspan="7">
					 			<span  style="padding-left: 70px;">
					 				<b>ZENAIDA L. JUMILLA</b>
					 			</span>
					 			<br>
					 			<span style="padding-left: 80px;">
					 				Disbursing Officer
					 			</span>
					 		</td>
					 	</tr>
					 </tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});
	$('#select_pay_period').select2({
		allowClear:true,
	    placeholder: "Pay Period",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	$(document).on('change','#select_pay_period',function(){
		_payPeriod = $(this).find(':selected').val()
		$('#pay_period').text(_payPeriod);
	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#print',function(){
		$('#reports').printThis();
	});

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					if(data.length !== 0){
						arr = [];
						ctr = 0;


						subBasicAmount 		= 0;
						subPeraAmount 		= 0;
						subTaAmount  		= 0;
						subRaAmount  		= 0;
						subSalaAmount  		= 0;
						subRataAmount  		= 0;
						subHpAmount  		= 0;
						subGsisAmount 		= 0;
						subPagibigAmount 	= 0;
						subPhilhealthAmount = 0;
						subTaxAmount 		= 0;
						subOtherAmount 		= 0;
						subNetAmount 		= 0;
						subSemiNetAmount 	= 0;
						subLongevityAmount 	= 0;


						$.each(data,function(k,v){

							lastname = (v.employees.lastname !== null) ? v.employees.lastname  : '';
							firstname = (v.employees.firstname !== null) ? v.employees.firstname  : '';
							middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
							employee_number = (v.employees.employee_number !== null) ? v.employees.employee_number : '';
							position = (v.positions !== null) ? v.positions.Name : '';

							basicOneAmount = (v.salaryinfo.basic_amount_one) ? v.salaryinfo.basic_amount_one : 0;
							basicTwoAmount = (v.salaryinfo.basic_amount_two) ? v.salaryinfo.basic_amount_two : 0;

							basicAmount = (parseFloat(basicOneAmount) + parseFloat(basicTwoAmount));


							// ===============
							// BENEFIT INFO
							// ===============

							taAmount 				= 0;
							raAmount 				= 0;
							salaAmount 				= 0;
							saAmount 			 	= 0;
							laAmount 				= 0;
							hpAmount 				= 0;
							peraAmount 				= 0;
							salaAbsentAmount 		= 0;
							salaUndertimeAmount 	= 0;

							if(v.benefit_transaction.length !== 0){
								$.each(v.benefit_transaction,function(k2,v2){
									switch(v2.benefits.code){
										case 'RATA1':
											raAmount = (v2.amount !== null) ? v2.amount : 0;
											break;
										case 'RATA2':
											taAmount = (v2.amount !== null) ? v2.amount : 0;
											break;
										case 'SA':
											saAmount = (v2.amount !== null) ? v2.amount : 0;
											salaAbsentAmount = (v2.sala_absent_amount) ? v2.sala_absent_amount : 0;
											salaUndertimeAmount = (v2.sala_undertime_amount) ? v2.sala_undertime_amount : 0;
											break;
										case 'LA':
											laAmount = (v2.amount !== null) ? v2.amount : 0;
											break;
										case 'HP':
											hpAmount = (v2.amount !== null) ? v2.amount : 0;
											break;
										case 'PERA':
											peraAmount = (v2.amount !== null) ? v2.amount : 0;
											break;
									}
								})
							}

							salaAmount = parseFloat(saAmount) + parseFloat(laAmount);

							rataAmount = (parseFloat(taAmount) + parseFloat(raAmount));

							// ===============
							// DEDUCTIONS
							// ===============

							gsisAmount = (v.gsis_ee_share !== null) ? v.gsis_ee_share : 0;
							pagibigAmount = (v.employeeinfo.pagibig_contribution !== null) ? v.employeeinfo.pagibig_contribution : 0;
							philhealthAmount = (v.employeeinfo.philhealth_contribution !== null) ? v.employeeinfo.philhealth_contribution : 0;
							taxAmount = (v.gettax) ? v.gettax.tax_amount : 0;
							totalLoanAmount = (v.total_loan !== null) ? v.total_loan : 0;
							totalOtherDeduct = (v.total_otherdeduct !== null) ? v.total_otherdeduct : 0;
							longevityAmount = (v.longevity !== null) ? v.longevity.longevity_amount : 0;

							totalCompensationAmount = (parseFloat(basicAmount) + parseFloat(rataAmount) + parseFloat(peraAmount) + parseFloat(salaAmount)  + parseFloat(hpAmount) + parseFloat(longevityAmount));

							mandatoryAmount = parseFloat(gsisAmount) + parseFloat(pagibigAmount) + parseFloat(philhealthAmount) + parseFloat(taxAmount) + parseFloat(salaAbsentAmount) + parseFloat(salaUndertimeAmount);

							totalOtherAmount = (parseFloat(totalLoanAmount) + parseFloat(totalOtherDeduct) + parseFloat(mandatoryAmount));

							totalNetAmount = (parseFloat(totalCompensationAmount) - parseFloat(totalOtherAmount));

							semiNetAmount = parseFloat(totalNetAmount)/2;

							subBasicAmount += parseFloat(basicAmount)
							subPeraAmount += parseFloat(peraAmount)
							subTaAmount  += parseFloat(taAmount);
							subRaAmount  += parseFloat(raAmount);
							subSalaAmount  += parseFloat(salaAmount);
							subRataAmount  += parseFloat(rataAmount);
							subHpAmount  += parseFloat(hpAmount);
							subGsisAmount += parseFloat(gsisAmount);
							subPagibigAmount += parseFloat(pagibigAmount);
							subPhilhealthAmount += parseFloat(philhealthAmount);
							subTaxAmount += parseFloat(taxAmount);
							subOtherAmount += parseFloat(totalOtherAmount);
							subNetAmount += parseFloat(totalNetAmount);
							subSemiNetAmount += parseFloat(semiNetAmount);
							subLongevityAmount += parseFloat(longevityAmount);


							basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
							pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
							rata_amount = (rataAmount !== 0) ? commaSeparateNumber(parseFloat(rataAmount).toFixed(2)) : '';
							sala_amount = (salaAmount !== 0) ? commaSeparateNumber(parseFloat(salaAmount).toFixed(2)) : '';
							gsis_amount = (gsisAmount !== 0) ? commaSeparateNumber(parseFloat(gsisAmount).toFixed(2)) : '';
							pagibig_amount = (pagibigAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigAmount).toFixed(2)) : '';
							philhealth_amount = (philhealthAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthAmount).toFixed(2)) : '';
							tax_amount = (taxAmount !== 0) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
							hp_amount = (hpAmount !== 0) ? commaSeparateNumber(parseFloat(hpAmount).toFixed(2)) : '';
							total_other_amount = (totalOtherAmount !== 0) ? commaSeparateNumber(parseFloat(totalOtherAmount).toFixed(2)) : '';
							total_net_amount = (totalNetAmount !== 0) ? commaSeparateNumber(parseFloat(totalNetAmount).toFixed(2)) : '';
							semi_net_amount = (semiNetAmount !== 0) ? commaSeparateNumber(parseFloat(semiNetAmount).toFixed(2)) : '';
							longevity_amount = (longevityAmount !== 0) ? commaSeparateNumber(parseFloat(longevityAmount).toFixed(2)) : '';
							sala_absent_amount = (salaAbsentAmount !== 0) ? commaSeparateNumber(parseFloat(salaAbsentAmount).toFixed(2)) : '';
							sala_undertime_amount = (salaUndertimeAmount !== 0) ? commaSeparateNumber(parseFloat(salaUndertimeAmount).toFixed(2)) : '';

							arr += '<tr style="border-top: 1px solid #b2b2b2;">';
							arr += '<td rowspan="2" class="text-left">['+employee_number+'] <b>'+lastname+' '+firstname+' '+middlename+'</b><br><span>'+position+'</span></td>';
							arr += '<td colspan="1" class="text-right">'+basic_amount+'</td>';
							arr += '<td colspan="1" class="text-right">'+rata_amount+'</td>';
							arr += '<td colspan="1" class="text-right">'+hp_amount+'</td>'; // HP
							arr += '<td colspan="1" class="text-right">'+gsis_amount+'</td>';
							arr += '<td colspan="1" class="text-right">'+philhealth_amount+'</td>';
							arr += '<td rowspan="2" class="text-right">'+total_other_amount+'</td>';
							arr += '<td rowspan="2" class="text-right">'+total_net_amount+'</td>';
							arr += '<td rowspan="2" class="text-right" colspan="3">'+semi_net_amount+'</td>';
							arr += '</tr>';

							arr += '<tr >';
							arr += '<td class="text-right">'+pera_amount+'</td>';
							arr += '<td class="text-right">'+sala_amount+'</td>';
							arr += '<td class="text-right">'+longevity_amount+'</td>'; // LP
							arr += '<td class="text-right">'+pagibig_amount+'</td>';
							arr += '<td class="text-right" >'+tax_amount+'</td>';
							arr += '</tr>';

							// ===============
							// LOANS
							// ===============

							if(salaAbsentAmount){
								arr += '<tr>';
								arr += '<td class="text-right" colspan="7" style="border:none;">Absent (SALA)<span style="margin-left:30px;">'+sala_absent_amount+'</span></td>';
								arr += '<td colspan="4" style="border:none;border-right:1px solid #b2b2b2;"></td>';
								arr += '</tr>';
							}

							if(salaUndertimeAmount){
								arr += '<tr>';
								arr += '<td class="text-right" colspan="7" style="border:none;">Undertime (SALA)<span style="margin-left:30px;">'+sala_undertime_amount+'</span></td>';
								arr += '<td colspan="4" style="border:none;border-right:1px solid #b2b2b2;"></td>';
								arr += '</tr>';
							}


							if(v.loan_transaction.length !== 0){
								$.each(v.loan_transaction,function(k2,v2){
									arr += '<tr>';
									arr += '<td class="text-right" colspan="7" style="border:none;">'+v2.loans.name+' <span style="margin-left:30px;">'+commaSeparateNumber(parseFloat(v2.amount).toFixed(2))+'</span></td>';
									arr += '<td colspan="4" style="border:none;border-right:1px solid #b2b2b2;"></td>';
									arr += '</tr>';
								})
							}

							if(v.deduction_transaction.length !== 0){
								$.each(v.deduction_transaction,function(k2,v2){
									arr += '<tr>';
									arr += '<td class="text-right" colspan="7" style="border:none;">'+v2.deductions.name+' <span style="margin-left:30px;">'+commaSeparateNumber(parseFloat(v2.amount).toFixed(2))+'</span></td>';
									arr += '<td colspan="4" style="border:none;border-right:1px solid #b2b2b2;"></td>';
									arr += '</tr>';
								})
							}
						})

						sub_total_basic_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '';
						sub_total_rata_amount = (subRataAmount !== 0) ? commaSeparateNumber(parseFloat(subRataAmount).toFixed(2)) : '';
						sub_total_hp_amount = (subHpAmount !== 0) ? commaSeparateNumber(parseFloat(subHpAmount).toFixed(2)) : '';
						sub_total_gsis_amount = (subGsisAmount !== 0) ? commaSeparateNumber(parseFloat(subGsisAmount).toFixed(2)) : '';
						sub_total_philhealth_amount = (subPhilhealthAmount !== 0) ? commaSeparateNumber(parseFloat(subPhilhealthAmount).toFixed(2)) : '';
						sub_other_amount = (subOtherAmount !== 0) ? commaSeparateNumber(parseFloat(subOtherAmount).toFixed(2)) : '';
						sub_total_pera_amount = (subPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subPeraAmount).toFixed(2)) : '';
						sub_total_sala_amount = (subSalaAmount !== 0) ? commaSeparateNumber(parseFloat(subSalaAmount).toFixed(2)) : '';
						sub_total_pagibig_amount = (subPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(subPagibigAmount).toFixed(2)) : '';
						sub_total_tax_amount = (subTaxAmount !== 0) ? commaSeparateNumber(parseFloat(subTaxAmount).toFixed(2)) : '';
						sub_total_net_amount = (subNetAmount !== 0) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '';
						sub_total_semi_net_amount = (subSemiNetAmount !== 0) ? commaSeparateNumber(parseFloat(subSemiNetAmount).toFixed(2)) : '';
						sub_total_longevity_amount = (subLongevityAmount !== 0) ? commaSeparateNumber(parseFloat(subLongevityAmount).toFixed(2)) : '';

						arr += '<tr style="border-top: 2px solid #585555; font-weight:bold">';
						arr += '<td rowspan="2" class="text-left" style="border-bottom: 2px solid #585555;"><b>GRAND TOTAL</b></td>';
						arr += '<td colspan="1" class="text-right">'+sub_total_basic_amount+'</td>';
						arr += '<td colspan="1" class="text-right">'+sub_total_rata_amount+'</td>';
						arr += '<td colspan="1" class="text-right">'+sub_total_hp_amount+'</td>'; // HP
						arr += '<td colspan="1" class="text-right">'+sub_total_gsis_amount+'</td>';
						arr += '<td colspan="1" class="text-right">'+sub_total_philhealth_amount+'</td>';
						arr += '<td rowspan="2" class="text-right" style="border-bottom: 2px solid #585555;">'+sub_other_amount+'</td>';
						arr += '<td rowspan="2" class="text-right" style="border-bottom: 2px solid #585555;">'+sub_total_net_amount+'</td>';
						arr += '<td rowspan="2" colspan="3" class="text-right" style="border-bottom: 2px solid #585555;">'+sub_total_semi_net_amount+'</td>';
						arr += '</tr>';

						arr += '<tr style="font-weight:bold">';
						arr += '<td class="text-right"  style="border-bottom: 2px solid #585555;">'+sub_total_pera_amount+'</td>';
						arr += '<td class="text-right" style="border-bottom: 2px solid #585555;">'+sub_total_sala_amount+'</td>';
						arr += '<td class="text-right" style="border-bottom: 2px solid #585555;">'+sub_total_longevity_amount+'</td>'; // LP
						arr += '<td class="text-right" style="border-bottom: 2px solid #585555;">'+sub_total_pagibig_amount+'</td>';
						arr += '<td class="text-right"  style="border-bottom: 2px solid #585555;">'+sub_total_tax_amount+'</td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);
						$('#month_year').text(_Month+' '+_Year);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});
})
</script>
@endsection