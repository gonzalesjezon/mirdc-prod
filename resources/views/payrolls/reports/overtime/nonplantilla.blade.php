<div class="col-md-12">
	<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	<h5><b>JOB ORDER OVERTIME</b></h5>
	<table class="table borderless">
		<tr>
			<td colspan="2"><b>Name of Employee</b></td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="employee_name"></span></td>
		</tr>
		<tr>
			<td colspan="2"><b>Daily Rate</b></td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="daily_rate"></span></td>
		</tr>
		<tr>
			<td colspan="6"><b>Overtime Hours</b></td>
		</tr>
		<tr>
			<td colspan="2">Regular</td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="regular_hours"></span></td>
		</tr>
		<tr>
			<td colspan="2">Weekend</td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="weekend_hours"></span></td>
		</tr>
		<tr>
			<td colspan="2"><b>LBP S/A #</b></td>
			<td>:</td>
			<td class="text-left" colspan="3"><span id="bank_account_number"></span></td>
		</tr>
		<tr>
			<td colspan="2"><b>EW/Tax</b></td>
			<td>:</td>
			<td class="text-left" colspan="3"><span class="ewtax"></span></td>
		</tr>
		<tr>
			<td colspan="2"><bTIN</b></td>
			<td>:</td>
			<td class="text-left" colspan="3"><span id="tax_id_number"></span></td>
		</tr>
		<tr>
			<td colspan="6">
				<b>SALARY FOR THE PERIOD COVERING</b>
			</td>
		</tr>
		<tr>
			<td colspan="3" >Overtime</td>
			<td colspan="3" class="text-left"><span class="ewtax"></span><b>(Less)</b></td>
		</tr>
		<tr>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td ><span id="regular_ot_amount" class="text-left"></span></td>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td class="text-right"><span id="regular_ot_less_percent_amount"></span></td>
		</tr>
		<tr>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td style="border-bottom: 1px solid #e2e2e2;" ><span id="weekend_ot_amount" class="text-left"></span></td>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td class="text-right" style="border-bottom: 1px solid #e2e2e2;"><span id="weekend_ot_less_percent_amount"></span></td>
		</tr>
		<tr>
			<td colspan="2">Gross Pay</td>
			<td class="text-left" colspan="1"><span id="sub_total_regular_and_weekend_amount"></span></td>
			<td class="text-right" colspan="3"><span id="sub_total_regular_and_weekend_less_percent_amount"></span></td>
		</tr>
		<tr>
			<td><b>NET PAY</b></td>
			<td>:</td>
			<td class="text-left"><b><span id="net_pay"></span></b></td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="6">
				<b>Attachments:</b>
			</td>
		</tr>
		<tr>
			<td colspan="6">Certified True Copies of </td>
		</tr>
		<tr>
			<td colspan="6">1. Certification of Completion of Service</td>
		</tr>
		<tr>
			<td colspan="6">2. Accomplishment Report</td>
		</tr>
		<tr>
			<td colspan="6" style="line-height: 4em;">Certified Correct</td>
		</tr>
		<tr>
			<td colspan="6" >
				<b>ANTONIA LYNNELY L. BAUTISTA</b>
			</td>
		</tr>
		<tr>
			<td colspan="6">Chief Admin Officer, HRDD</td>
		</tr>
	</table>
</div>