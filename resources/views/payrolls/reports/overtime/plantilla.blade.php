<div class="col-md-12">
	<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	<h5><b>PLANTILLA OVERTIME</b></h5>
	<table class="table borderless">
		<tr>
			<td colspan="2"><b>Name of Employee</b></td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="fullname"></span></td>
		</tr>
		<tr>
			<td colspan="2"><b>Daily Rate</b></td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="dailyrate"></span></td>
		</tr>
		<tr>
			<td colspan="6"><b>Overtime Hours</b></td>
		</tr>
		<tr>
			<td colspan="2">Regular</td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="regularhours"></span></td>
		</tr>
		<tr>
			<td colspan="2">Weekend</td>
			<td>:</td>
			<td colspan="3" class="text-left"><span id="weekendhours"></span></td>
		</tr>
		<tr>
			<td colspan="2"><b>LBP S/A #</b></td>
			<td>:</td>
			<td class="text-left" colspan="3"><span id="bankaccountnumber"></span></td>
		</tr>
		<tr>
			<td colspan="2"><bTIN</b></td>
			<td>:</td>
			<td class="text-left" colspan="3"><span id="taxidnumber"></span></td>
		</tr>
		<tr>
			<td colspan="6">
				<b>SALARY FOR THE PERIOD COVERING</b>
			</td>
		</tr>
		<tr>
			<td colspan="3" >Overtime</td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td ><span id="regularotamount" class="text-left"></span></td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td><b>Weekend OT</b></td>
			<td>:</td>
			<td style="border-bottom: 1px solid #e2e2e2;" ><span id="weekendotamount" class="text-left"></span></td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="2">Gross Pay</td>
			<td class="text-left" colspan="1"><span id="gross_pay"></span></td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td><b>NET PAY</b></td>
			<td>:</td>
			<td class="text-left"><b><span id="netpay"></span></b></td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td colspan="6">
				<b>Attachments:</b>
			</td>
		</tr>
		<tr>
			<td colspan="6">Certified True Copies of </td>
		</tr>
		<tr>
			<td colspan="6">1. Certification of Completion of Service</td>
		</tr>
		<tr>
			<td colspan="6">2. Accomplishment Report</td>
		</tr>
		<tr>
			<td colspan="6" style="line-height: 4em;">Certified Correct</td>
		</tr>
		<tr>
			<td colspan="6" >
				<b>ANTONIA LYNNELY L. BAUTISTA</b>
			</td>
		</tr>
		<tr>
			<td colspan="6">Chief Admin Officer, HRDD</td>
		</tr>
	</table>
</div>