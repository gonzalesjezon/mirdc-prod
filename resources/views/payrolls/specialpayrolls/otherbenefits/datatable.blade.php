<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_pbb">
		<thead>
			<tr >
				<th  >Other Benefits Amount</th>
				<th  >Deduction</th>
				<th>Tax Withheld</th>
				<th  >Total</th>
			</tr>
		</thead>
		<tbody class="text-right">
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pbb').DataTable({
	 	'dom':'<lf<t>pi>',
	 	'responsive': true,
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pbb tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        id 					= $(this).data('id');
	        employee_id 		= $(this).data('employeeid');
	        position_id 		= $(this).data('position_id');
	        responsibility_id 	= $(this).data('responsibility_id');
	        division_id 		= $(this).data('division_id');
	        office_id 			= $(this).data('office_id');
	        unit_id 			= $(this).data('unit_id');
	        year 				= $(this).data('year');
	        month 				= $(this).data('month');
	        pbb_amount 			= $(this).data('amount');

	        $('#employee_id').val(employee_id);
	        $('#new_pbb_amount').val(pbb_amount);
	        $('#year').val(year);
	        $('#month').val(month);
	        $('#position_id').val(position_id);
	        $('#responsibility_id').val(responsibility_id);
	        $('#division_id').val(division_id);
	        $('#office_id').val(office_id);
	        $('#unit_id').val(unit_id);


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
