<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_pei" style="width: 809px;">
		<thead>
			<tr >
				<th>Position</th>
				<th>Uniform Allowance</th>
				<th>Cost of Uniform</th>
				<th>Tax Amount</th>
				<th >Net Amount</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody class="text-right">
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pei').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pei tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');
	        id 					= $(this).data('id');
	        employee_id 		= $(this).data('employee_id');
	        uniform_amount 		= $(this).data('uniform_amount');
	        cost_uniform_amount = $(this).data('cost_uniform_amount');
	        position_id 		= $(this).data('position_id');
	        benefit_info_id 	= $(this).data('benefit_info_id');

	        $('#uniform_id').val(id);
	        $('#employee_id').val(employee_id);
	        $('#uniform_amount').val(uniform_amount);
	        $('#cost_uniform_amount').val(cost_uniform_amount);
	        $('#position_id').val(position_id);
	        console.log(benefit_info_id);
	        $('#benefitinfo_id').val(benefit_info_id);




	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
