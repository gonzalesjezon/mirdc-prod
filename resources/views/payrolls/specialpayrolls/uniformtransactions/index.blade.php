@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_pei" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_pei" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_pei" id="wpei" value="wpei">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_pei" id="wopei" value="wopei">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="pei">
			<div class="row">
				<div class="col-md-12">
					<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;{{ $title }}</label>
					<div class="sub-panel">
						{!! $controller->showUniformDatatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
					<div class="style-box2">
						<!-- <div class="button-wrapper" style="position: relative;bottom:15px;left: 5px;" >
							<a class="btn btn-xs btn-info btn-editbg btn_edit " id="editAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> Edit</a>

							<a class="btn btn-xs btn-info btn-savebg btn_save update_uniform hidden" data-form="form" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance" id="saveAttendance"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-form="myform" data-btnedit="editAttendance" data-btnsave="saveAttendance"id="cancelAttendance"> Cancel</a>
						</div> -->

						<form method="POST" action="{{ url($module_prefix.'/'.$module.'/updateUniform')}}" onsubmit="return false" id="form" class="myform">
							<input type="hidden" name="token" value="{{ csrf_token() }}">
							<input type="hidden" name="uniform_id" id="uniform_id">
							<input type="hidden" name="employee_id" id="employee_id">
							<input type="hidden" name="position_id" id="position_id">
							<input type="hidden" name="benefit_info_id" id="benefitinfo_id">
							<div class="col-md-3">
								<div class="form-group editAttendance" >
									<label> Uniform Amount</label>
									<input type="text" name="uniform_amount" id="uniform_amount" class="form-control">
								</div>
								<div class="form-group editAttendance">
									<label>Cost Uniform Amount</label>
									<input type="text" name="cost_uniform_amount" id="cost_uniform_amount" class="form-control" >
								</div>
							</div>
							<div class="col-md-9">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblUniform = $('#tbl_pei').DataTable();
	var uniform_id;
	number_of_actual_work = 0;
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
		year += '<option value='+y+'>'+y+'</option>';
	}
	$('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#wopei').prop('checked',false);

 // $('.editAttendance :input').attr("disabled",true);
 $('.btn_new').on('click',function(){
	// $('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){

	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	// $('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnedit+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');

	editClicked = true;
});

$('.btn_cancel').on('click',function(){
	$('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	// $('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnedit+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	$('#benefitForm').addClass('hidden');
	clear_form_elements('nonplantilla');
	$('.error-msg').remove();

	editClicked = false;

});

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    var uniformAmount = 0;
    $('#uniform_amount').on('keyup',function(){
    	uniformAmount = $(this).val();
    });

    var costUniformAmount = 0;
    $('#cost_uniform_amount').on('keyup',function(){
    	costUniformAmount = $(this).val();
    });

    _checkpei = ""
    $('input[type=radio][name=chk_pei]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wpei') {
    			$('#btn_process_pei').prop('disabled',true);
    			_checkpei = 'wpei';

    		}
    		else if (this.value == 'wopei') {
    			$('#btn_process_pei').prop('disabled',false);
    			_checkpei = 'wopei';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}

    });

    var pei_id;
    var employee_id = '';
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getUniform',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				tblUniform.clear().draw();
    				if(data.length !== 0){
	    				clear_form_elements('benefits-content');
	    				pei_id = '';

	    				uniform_amount = 0;
	    				cost_uniform_amount = 0

	    				$.each(data,function(k,v){

	    					positions = (v.positions) ? v.positions.name : '';
	    					uniform_amount = (v.amount) ? v.amount : 0;
	    					cost_uniform_amount = (v.cost_uniform_amount) ? v.cost_uniform_amount : 0;

		    				net_amount = (uniform_amount - cost_uniform_amount);

		    				created_at =  new Date(v.created_at);

		    				uniform_amount = (uniform_amount) ? commaSeparateNumber(parseFloat(uniform_amount).toFixed(2)) : '';
		    				cost_uniform_amount = (cost_uniform_amount) ? commaSeparateNumber(parseFloat(cost_uniform_amount).toFixed(2)) : '';
		    				net_amount = (net_amount) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

							tblUniform.row.add( [
								positions,
								uniform_amount,
								cost_uniform_amount,
								'',
								net_amount,
								created_at.getFullYear() +'-'+ (created_at.getMonth()+1) +'-'+created_at.getDate()

							]).draw( false );

					        tblUniform.rows(k).nodes().to$().attr("data-id", v.id);
					        tblUniform.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
					        tblUniform.rows(k).nodes().to$().attr("data-cost_uniform_amount", v.cost_uniform_amount);
					        tblUniform.rows(k).nodes().to$().attr("data-uniform_amount", v.amount);
					        tblUniform.rows(k).nodes().to$().attr("data-position_id", v.position_id);
					        tblUniform.rows(k).nodes().to$().attr("data-benefit_info_id", v.benefit_info_id);
					        tblUniform.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
					        tblUniform.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
					        tblUniform.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
					        tblUniform.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
					        tblUniform.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");
	    				});


    				}

				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_pei':_checkpei,
					'_year':_Year,
					'_month':_Month
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();

	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_pei',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Uniform Allowance?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processUniform();
			}else{
				return false;
			}
		});
	}

});

$.processUniform= function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processUniform',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'uniform_amount':uniformAmount,
			'cost_uniform_amount':costUniformAmount,
			'pei_id':pei_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_pei').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
				// window.location.href = base_url+module_prefix+module;
				_listId = [];
				$('#btn_process_pei').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}else{

				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
				$('#btn_process_pei').html('<i class="fa fa-save"></i> Process').prop('disabled',false);

			}
		}
	});
}


$(document).on('click','#delete_pei',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Uniform Allowance?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteUniform();
			}else{
				return false;
			}
		});
	}
})

$.deleteUniform = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteUniform',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_pei').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_pei').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkpei':_checkpei },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});

$(document).on('click','.update_uniform',function(){

	if(employee_id == ""){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Uniform Allowance?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.updateUniform();
			}else{
				return false;
			}
		});
	}

});

$.updateUniform= function(){
	$.ajax({
		url:base_url+module_prefix+module+'/updateUniform',
		data:{
			'_token':'{{ csrf_token() }}',
			'year':_Year,
			'month':_Month,
			'uniform_amount':$('#uniform_amount').val(),
			'cost_uniform_amount': $('#cost_uniform_amount').val(),
			'uniform_id':$('#uniform_id').val(),
			'employee_id':$('#employee_id').val(),
			'position_id':$('#position_id').val(),
			'benefit_info_id':$('#benefitinfo_id').val()
		},
		type:'POST',
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					_listId = [];
					$('.btnfilter').trigger('click');
				}else{
					swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					closeOnConfirm: false
				});
				}
			}
	});
}



});


</script>
@endsection