<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_benefitTransact">
		<thead>
			<tr>
				<th>Effectivity Date</th>
				<th>Benefits</th>
				<th>Description</th>
				<th>Amount</th>
				<th>Pay Period</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody >

		</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	 table = $('#tbl_benefitTransact').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_benefitTransact tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        $('#daysPresent').addClass('hidden');


	        benefitinfo_id 			= $(this).data('id');
			benefit_employee_id 	= $(this).data('employeeid');
			_effectiveDate 			= $(this).data('date');
			_payperiod 				= $(this).data('payperiod');
			_description 			= $(this).data('description');
			_amount 				= $(this).data('amount');
			code					= $(this).data('code');
			_paySub 				= $(this).data('paysub');
			_benefit_id 			= $(this).data('benefitid');
			_dateFrom 	 			= $(this).data('datefrom');
			_dateTo 	 			= $(this).data('dateto');
			_daysPresent 			= $(this).data('dayspresent');
			_hpRate 	 			= $(this).data('hprate');
			sala_absent 	 		= $(this).data('sala_absent');
			sala_absent_amount 	 	= $(this).data('sala_absent_amount');
			sala_undertime 	 		= $(this).data('sala_undertime');
			sala_undertime_amount 	= $(this).data('sala_undertime_amount');

			$('#benefit_effectivedate').val(_effectiveDate);
			$('#benefits_payperiod').val(_payperiod);
			$('#benefits_description').val(_description);

			$('#benefit_id').val(_benefit_id);
			$('#benefits_payperiod').val(_payperiod);
			$('#benefitinfo_id').val(benefitinfo_id);
			$('#date_from').val(_dateFrom);
			$('#date_to').val(_dateTo);

			$('#hp_rate').val(_hpRate);

			if(code === 'SA' || code === 'LA'){
				$('.daysPresent').removeClass('hidden');
				$('.hpRate').addClass('hidden');
				$('#benefitForm').removeClass('hidden');
				$('#days_present_sala').val(_daysPresent)
				$('#benefits_amount_sala').val(_amount);
				$('#benefit_status').val(code);
				$('#sala_absent').val(sala_absent);
				$('#sala_absent_amount').val(sala_absent_amount);
				$('#sala_undertime').val(sala_undertime);
				$('#sala_undertime_amount').val(sala_undertime_amount);
			}else if(code === 'HP'){
				$('.hpRate').removeClass('hidden');
				$('.daysPresent').addClass('hidden');
				$('#benefitForm').removeClass('hidden');
				$('#days_present_hp').val(_daysPresent);
				$('#benefits_amount_hp').val(_amount);
				$('#benefit_status').val(code);
			}else{
				$('#benefitForm').addClass('hidden');
			}


			switch(_payperiod){
				case 'Weekly':
					$('.weekly').addClass('show').removeClass('hidden');
					$('.semi-monthly').removeClass('show').addClass('hidden');
				break;

				case 'Semi Monthly':
					$('.weekly').removeClass('show').addClass('hidden');
					$('.semi-monthly').addClass('show').removeClass('hidden');
				break;

				default:
					$('.weekly').removeClass('show').addClass('hidden');
					$('.semi-monthly').removeClass('show').addClass('hidden');
				break;
			}

			switch(_paySub){
				case 'Week 1':
					$('#week1').prop('checked','checked');
				break;
				case 'Week 2':
					$('#week2').prop('checked','checked');
				break;
				case 'Week 3':
					$('#week3').prop('checked','checked');
				break;
				case 'Week 4':
					$('#week4').prop('checked','checked');
				break;
				case 'First Half':
					$('#firsthalf').prop('checked','checked');
				break;
				case 'Second Half':
					$('#secondhalf').prop('checked','checked');
				break;
			}

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});


	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
