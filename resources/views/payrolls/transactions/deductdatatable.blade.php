<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_deductTransact">
		<thead>
				<tr>
					<th>Deduction Name</th>
					<th>Amount</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Pay Period</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody></tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deductTransact').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_deductTransact tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        _id 			= $(this).data('id');
			_deductionId 	= $(this).data('deductionid');
			_amount 		= $(this).data('amount');
			_dateStart		= $(this).data('datestart');
			_dateEnd		= $(this).data('dateend');
			_payPeriod		= $(this).data('payperiod');
			_dateTerminated	= $(this).data('dateterminated');
			_employeeId		= $(this).data('employeeid');


			$('#deduction_id').val(_deductionId);
			$('#input_deductamount').val(_amount);
			$('#select_deductperiod').val(_payPeriod);
			$('#date_deductstart').val(_dateStart);
			$('#date_deductterminated').val(_dateTerminated);
			$('#date_deductend').val(_dateEnd);
			$('#deduct_employee_id').val(_employeeId);
			$('#deductinfo_id').val(_id);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
